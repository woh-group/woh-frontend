# WohFrontend

## Crear componente

ng generate component NOMBRE_COMPONENTE

## Colors

primary #489CA2 -RGB(72, 156, 162)
secondary #68D8D8 -RGB(104, 216, 216)
tertiary #5D5E5E -RGB(93, 94, 94)
quaternary #F35051 -RGB(243, 80, 81)
quinary #FA9448 -RGB(250, 148, 72)

## Cypress init

npx cypress open

## Deploy AWS

ng build --prod

aws s3 sync . s3://woh-frontend/ --delete --acl public-read

aws cloudfront create-invalidation --distribution-id EOHS3G59I6FFS --paths "/*"



-------------------------
Ejemplo categorias
https://stackoverflow.com/questions/50859425/using-graphlookup-with-a-condition

db.prueba1.aggregate([
  { "$match": { "_id": 3 } },
  { "$graphLookup": {
    "from": "prueba1",
    "startWith": "$parent",
    "connectFromField": "parent",
    "connectToField": "_id",
    "as": "people",
    "depthField": "depth"
  }}
]).pretty();


Categorias 
---------------------
db.categoriasPregunta.aggregate([
  { "$match": { "id": "2.2" } },
  { "$graphLookup": {
    "from": "categoriasPregunta",
    "startWith": "$categoriaPadreId",
    "connectFromField": "categoriaPadreId",
    "connectToField": "id",
    "as": "people",
    "depthField": "depth"
  }}
]).pretty();


Random
---------------------
db.tiposDocumento.aggregate(
   [ { $sample: { size: 3 } } ]
).pretty();



http://embed.plnkr.co/jAVScZ/
https://stackoverflow.com/questions/33806943/two-sidenav-in-a-page-in-angular-material