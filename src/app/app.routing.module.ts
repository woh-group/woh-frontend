import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './_components/home/home.component';
import { LoginComponent } from './_components/login/login.component';
import { PerfilComponent } from './_components/usuarios/perfil/perfil/perfil.component';
import { ActivarUsuarioComponent } from './_components/usuarios/activar-usuario/activar-usuario.component';

// Preguntas
import { PreguntaResultadosComponent } from './_components/preguntas/pregunta-resultados/pregunta-resultados.component';
import { PreguntaCrearComponent } from './_components/preguntas/pregunta-crear/pregunta-crear.component';
import { PreguntaDetalleComponent } from './_components/preguntas/pregunta-detalle/pregunta-detalle.component';
import { NuestrosServiciosComponent } from './_components/nuestros-servicios/nuestros-servicios.component';
import { ProfesionalesComponent } from './_components/profesionales/profesionales.component';
import { ProfesionalPerfilPublicoComponent } from './_components/profesional-perfil-publico/profesional-perfil-publico.component';

// Admin
import { UsuariosComponent } from './_components/admin/usuarios/usuarios.component';
import { ReportsComponent } from './_components/admin/reports/reports.component';
import { LogsComponent } from './_components/admin/logs/logs.component';

// Guard
import { AuthRolAdminGuard } from './_guards/auth-rol-admin.guard';
import { AuthGuard } from './_guards/auth.guard';
import { OlvidoContrasenaComponent } from './_components/usuarios/olvido-contrasena/olvido-contrasena.component';
import { RestablecerContrasenaComponent } from './_components/usuarios/restablecer-contrasena/restablecer-contrasena.component';
import { ResultadoPagoComponent } from './_components/resultado-pago/resultado-pago.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'last-questions',
    component: NuestrosServiciosComponent
  },
  {
    path: 'registro/:type',
    component: PerfilComponent,
    data: { scope: 'ADD' }
  },
  {
    path: 'usuario/perfil',
    component: PerfilComponent,
    canActivate: [AuthGuard],
    data: { scope: 'UPDATE' }
  },
  {
    path: 'usuario/activacion/:id/:activationCode',
    component: ActivarUsuarioComponent
  },
  {
    path: 'usuario/olvidoContrasena',
    component: OlvidoContrasenaComponent
  },
  {
    path: 'usuario/restablecerContrasena/:id',
    component: RestablecerContrasenaComponent
  },
  // {
  //   path: 'preguntas', component: HomeComponent,
  //   children: [
  //     // {path: '', redirectTo: 'home', pathMatch: 'full'},
  //     // {path: 'home', component: PersonaHomeComponent},
  //     {path: 'search/:pregunta', component: PreguntaResultadosComponent},
  //     {path: 'add/:pregunta', component: PreguntaCrearComponent},
  //     {path: 'detail/:id', component: PreguntaDetalleComponent}
  //   ]
  // },
  {
    path: 'preguntas/search',
    component: PreguntaResultadosComponent,
    data: { scope: 'TYPE-1' }
  },
  {
    path: 'preguntas/search/:pregunta',
    component: PreguntaResultadosComponent,
    data: { scope: 'TYPE-2' }
  },
  {
    path: 'preguntas/search/category/:id',
    component: PreguntaResultadosComponent,
    data: { scope: 'TYPE-3' }
  },
  {
    path: 'preguntas/mis-preguntas',
    component: PreguntaResultadosComponent,
    canActivate: [AuthGuard],
    data: { scope: 'TYPE-4' }
  },
  {
    path: 'preguntas/add/:pregunta',
    component: PreguntaCrearComponent
  },
  {
    path: 'preguntas/detail/:id',
    component: PreguntaDetalleComponent
  },
  {
    path: 'profesionales',
    component: ProfesionalesComponent
  },
  {
    path: 'profesionales/:letter',
    component: ProfesionalesComponent
  },
  {
    path: 'profesionales/perfil/:id',
    component: ProfesionalPerfilPublicoComponent
  },
  {
    path: 'admin/usuarios',
    component: UsuariosComponent,
    canActivate: [AuthRolAdminGuard]
  },
  {
    path: 'admin/reports',
    component: ReportsComponent,
    canActivate: [AuthRolAdminGuard]
  },
  {
    path: 'admin/logs',
    component: LogsComponent,
    canActivate: [AuthRolAdminGuard]
  },
  {
    path: 'pay/result',
    component: ResultadoPagoComponent
  },
  {
    path: 'pay/confirmation',
    component: ResultadoPagoComponent
  },
  { path: '**', component: HomeComponent }
];

@NgModule({
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
})
export class AppRoutingModule { }
