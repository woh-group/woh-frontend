export class Config {

    // Headers
    static headerRequestId = 'Request-Id';

    // Tipos de estudios
    static versionTerminosCondiciones = 1.0;

    // Tipos de estudios
    static tipoEstudio = 1;
    static tipoEstudioComplementario = 2;

    // Tipos de usuarios
    static userPaciente = 'PACIENTE';
    static userProfesional = 'PROFESIONAL';

    // Valores por defecto
    static paisIdDefault = 'COL'; // Colombia
    static departamentoIdDefault = '08'; // Atlantico
    static ciudadIdDefault = '08-001'; // Barranquilla
    static mayoriaEdad = 18;

    static anoActual = new Date().getFullYear();
    static maxDateParaRegistro = new Date(Config.anoActual - Config.mayoriaEdad, 11, 31);
    static maxDateParaPregunta = new Date();

    // WhatsApp
    static numeroWhatsapp = '+573133753782';
    static textMensajeWhatsapp = '&text=Hola,%20deseo%20reservar%20un%20espacio%20con%20ustedes.%20Gracias.';
    static textMensajeWhatsappProfesional = '&text=Hola,%20deseo%20reservar%20un%20espacio%20con%20el%20psicólogo%20###NOMBRE###.%20Gracias.';

    static respuestaDefault = 'Si deseas conocer más o agendar una cita para consejería o consultoría psicológica por medio de chat, llamada o videollamada, escríbenos a <a href="https://api.whatsapp.com/send?phone=' + Config.numeroWhatsapp + '&text=' + Config.textMensajeWhatsapp + '" target="_blank">nuestro WhatsApp</a> y con gusto te acompañaremos y apoyaremos.';
    static agendarCitaProfesional = '<a href="https://api.whatsapp.com/send?phone=' + Config.numeroWhatsapp + '&text=' + Config.textMensajeWhatsappProfesional + '" target="_blank">Agendar con este psicólogo</a>';

    // Roles
    static rolSuperAdministrador = 1;
    static rolAdministrador = 2;
    static rolProfesional = 3;
    static rolPaciente = 4;

    // Profesiones
    static profesionIdPsicologo = 1;

    // estadoUsuarios usuario
    static estadoUsuarioInactivo = 1;
    static estadoUsuarioPendiente = 2;
    static estadoUsuarioEnValidacion = 3;
    static estadoUsuarioActivo = 4;
    static estadoUsuarioBloqueado = 5;
    static estadoUsuarioEliminado = 6;

    // Scopes
    static scopeREAD = 'READ';
    static scopeADD = 'ADD';
    static scopeUPDATE = 'UPDATE';
    static scopeDELETE = 'DELETE';

    static permisos = {
        'ADD': true,
        'DELETE': true,
        'READ': true,
        'UPDATE': true
    };

    // Api Comunes
    static apiComunesPaises = 'comunes - paises';
    static apiComunesDepartamentos = 'comunes - departamentos';
    static apiComunesCiudades = 'comunes - ciudades';
    static apiComunesTiposDocumentos = 'comunes - tiposDocumentos';
    static apiComunesNivelesAcademicos = 'comunes - nivelesAcademicos';
    static apiComunesProfesiones = 'comunes - profesiones';
    static apiComunesCorreos = 'comunes - correos';

    // Api Seguridad
    static apiUsuariosRoles = 'usuarios - roles';
    static apiUsuariosAutenticacion = 'usuarios - autenticacion';
    static apiUsuariosUsuarios = 'usuarios - usuarios';
    static apiUsuariosPersonas = 'usuarios - personas';

    // Api Preguntas
    static apiPreguntasPreguntas = 'preguntas - preguntas';
    static apiPreguntasRespuestas = 'preguntas - respuestas';
    static apiPreguntasCategorias = 'preguntas - categorias';

    // Api Profesionales
    static apiProfesionalesProfesionales = 'profesionales - profesionales';
    static apiProfesionalesPerfiles = 'profesionales - perfiles';
    static apiProfesionalesEstudios = 'profesionales - estudios';
    static apiProfesionalesExperiencias = 'profesionales - experiencias';
    static apiProfesionalesEspecialidades = 'profesionales - especialidades';

    // Api Trace
    static apiTrace = 'trace - trace';

    // Api Otras
    static apiGeolocalizacion = 'Otra - geolocalizacion';

    // Patrones - Expresiones Regulares
    static emailValido = '[a-zA-Z\-0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}';
    static preguntaValida = '^(\¿)?[A-Za-z0-9áéíóúÁÉÍÓÍ\s;?.,!¡]+';
    static soloLetras = '[a-zA-Z ]*$';

    // Correos
    static correoContacto = 'contigo@woh.com.co';
    static correoContactoGmail = 'wordsofhopeco@gmail.com';

    // Correos Plantillas
    static plantillaContacto = 'correo-contacto';

    // APIS Existentes
    static apis = [
        // Api Comunes
        'paises',
        'departamentos',
        'ciudades',
        'tiposDocumento',
        'nivelesAcademico',
        'profesiones',
        'correos',

        // Api Seguridad
        'roles',
        'autenticacion',
        'usuarios',

        // Api Preguntas
        'preguntas',
        'categoriasPregunta',

        // Api Profesionales
        'profesionales',
        'profesionalesEstudios',
        'profesionalesExperiencias',
        'especialidades'
    ];

    // Valores del negocio
    static valorConsulta = 70000;
    static nombreServicio = 'Consulta individual WOH';
}
