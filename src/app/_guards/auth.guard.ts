import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AutenticacionService } from '../_services/autenticacion.service';
@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {

    constructor(private autenticacionService: AutenticacionService, private router: Router) { }

    canActivate(): boolean {
        const token = this.autenticacionService.getCurrentUser();
        if (token && token.accessToken) {
            return true;
        } else {
            this.router.navigate(['/home']);
            return false;
        }
    }
}
