import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AutenticacionService } from '../_services/autenticacion.service';
import { JwtUtil } from '../_helpers/_util/jwt-util';
import { Config } from '../_configs/config';
@Injectable({
    providedIn: 'root'
})
export class AuthRolAdminGuard implements CanActivate {

    constructor(private autenticacionService: AutenticacionService, private router: Router) { }

    canActivate(): boolean {
        const token = this.autenticacionService.getCurrentUser();
        if (token && token.accessToken) {
            // Obtenemos los datos del token
            const tokenInfo = JwtUtil.decodeToken(token.accessToken);
            if (tokenInfo.rol === Config.rolSuperAdministrador) {
                return true;
            } else {
                this.router.navigate(['/home']);
                return false;
            }
        } else {
            this.router.navigate(['/home']);
            return false;
        }
    }
}
