export class DateUtil {

    // Calcula la edad basado una fecha
    static calculateAge(fechaNacimiento: Date) {
        if (fechaNacimiento) {
            const ageDifMs = Date.now() - new Date(fechaNacimiento).getTime();
            const ageDate = new Date(ageDifMs);
            const edad = Math.abs(ageDate.getUTCFullYear() - 1970);
            return edad === 0 ? 1 : edad;
        }
        return 0;
    }
}
