import * as CryptoJS from 'crypto-js';

export class CryptoUtil {

    static key = 'lpP;UQ0mi*98Si:u';

    static encrypt(text: string): string {
        return CryptoJS.AES.encrypt(text, this.key).toString();
    }

    static decrypt(cipherText: string): string {
        const bytes = CryptoJS.AES.decrypt(cipherText, this.key);
        return bytes.toString(CryptoJS.enc.Utf8);
    }

}
