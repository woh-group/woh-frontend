import { Config } from '../../_configs/config';

export class TitleUtil {

    // Obtienes un titulo para un form
    static getTitle(scope: string, text: string): string {
        switch (scope) {
            case Config.scopeREAD: {
                return `VER ${text}`;
            }
            case Config.scopeADD: {
                return `REGISTRO ${text}`;
            }
            case Config.scopeUPDATE: {
                return `EDITAR ${text}`;
            }
            case Config.scopeDELETE: {
                return `ELIMINAR ${text}`;
            }
            default: {
                return '';
            }
        }
    }

}
