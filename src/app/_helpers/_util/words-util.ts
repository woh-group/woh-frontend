export class WordsUtil {

    static ARTICULOS = 'el|la|los|las|un|una|unos|unas';
    static PREPOSICIONES = 'a|ante|bajo|con|contra|de|desde|en|entre|hacia|hasta|para|por|pro|segun|sin|sobre|tras';
    static CONJUNCIONES = 'y|e|ni|o|u|bien|ya|no|otro|tanto|como|pero|mas|aunque|sin embargo|no obstante|antes|por lo demas|sino|excepto|es|decir|esto|porque|pues|puesto|si|tal|que|siempre|luego|modo|conque|mientras|cuando';
    static OTROS = 'se|le|poco|han|estan|muy|mi|yo|tu|ella|nosotros|ustedes|ellos|';
    static SIGNOS = '¡|!|¿|?|.|,|:|;|+|-|*|/|(|)|{|}|[|]|"';

    static getPalabrasPrincipales(text: string): string {
        let resp = text;
        // console.log('NFD:', text.normalize('NFD'));
        resp = resp.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
        // console.log('sin acentos:', resp);
        resp = resp.replace(/[^a-z0-9\s]/gi, '');
        // resp = resp.replace(/[^a-z0-9\s]/gi, '');
        // console.log('sin signos:', resp);
        resp = resp.replace(new RegExp('\\b(' + this.ARTICULOS + ')\\b', 'gi'), ' ').replace(/\s{2,}/g, ' ');
        resp = resp.replace(new RegExp('\\b(' + this.PREPOSICIONES + ')\\b', 'gi'), ' ').replace(/\s{2,}/g, ' ');
        resp = resp.replace(new RegExp('\\b(' + this.CONJUNCIONES + ')\\b', 'gi'), ' ').replace(/\s{2,}/g, ' ');
        resp = resp.replace(new RegExp('\\b(' + this.OTROS + ')\\b', 'gi'), ' ').replace(/\s{2,}/g, ' ');
        // return resp.trim().replace(/\s/g, ',');

        resp = resp.trim().replace(/\s/g, ',');
        const array = resp.split(',');

        let words = '';
        array.forEach(element => {
            if (element.trim().length > 1) {
                words += element + ',';
            }
        });
        words = words.slice(0, -1);
        return words;
    }

}
