export class PersonaUtil {

    // Obtienes el genero de una persona basado en el sexo y la edad
    static getGeneroFromSexoAndAge(sexo: string, edad: number): string {
        switch (sexo) {
            case 'F': {
                return 'mujer';
                // if (edad > 18) {
                //     return 'mujer';
                // } else {
                //     return 'niña';
                // }
                break;
            }
            case 'M': {
                return 'hombre';
                // if (edad > 18) {
                //     return 'hombre';
                // } else {
                //     return 'niño';
                // }
                break;
            }
            default: {
                return 'persona';
                break;
            }
        }
    }

}
