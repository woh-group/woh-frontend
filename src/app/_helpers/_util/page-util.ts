export class PageUtil {

    // Mueve la vista a la seccion indicada
    static goToSecction(location: string): void {
        window.location.hash = location;
    }
}
