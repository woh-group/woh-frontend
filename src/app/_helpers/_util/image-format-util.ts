
export class ImageFormatUtil {

    // Devuelve true si el campo tiene errores
    static formatImage(formato: string): string {
        let resp = '';
        switch (formato) {
            case 'image/jpeg': {
                resp = '.jpg';
               break;
            }
            case 'image/png': {
                resp = '.png';
               break;
            }
            default: {
                resp = '.jpg';
               break;
            }
         }
        return resp;
    }
}
