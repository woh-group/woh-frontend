import swal from 'sweetalert2';

export class ModalUtil {

    // Types modal
    static TYPE_MODAL_SUCCESS = 'success';
    static TYPE_MODAL_ERROR = 'error';
    static TYPE_MODAL_WARNING = 'warning';
    static TYPE_MODAL_INFO = 'info';
    static TYPE_MODAL_QUESTION = 'question';

    // Buttons name
    static MSJ_CONFIRM_ELIMINAR = '¿Está seguro que desea eliminar el registro?';

    // Buttons name
    static BTN_NAME_ACEPTAR = 'Aceptar';
    static BTN_NAME_CANCELAR = 'Cancelar';
    static BTN_NAME_ELIMINAR = 'Eliminar';

    static TITLE_NAME_ERROR = 'Error';
    static TITLE_NAME_SUCCESS = 'Hecho';

    static showModal(message: string, allowOutsideClick: boolean, typeModal: any, title?: string, requestId?: string): Promise<any> {
        let titleMessage = '';
        let footerMessage = '';

        if (title) {
            titleMessage = title;
        } else {
            titleMessage = this.getDefaultTitle(typeModal);
        }

        if (requestId && requestId.length > 0) {
            footerMessage += `<small class="text-muted">PETICIÓN: ${requestId}</small>`;
        }

        return swal({
            title: titleMessage,
            html: message,
            footer: footerMessage,
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            confirmButtonText: this.BTN_NAME_ACEPTAR,
            type: typeModal ? typeModal : null,
            allowOutsideClick: allowOutsideClick
        });
    }

    static showModalConfirm(message: string, allowOutsideClick: boolean, typeModal: any, confirmButtonText: string, title?: string): Promise<any> {
        let titleMsg = '';

        if (title) {
            titleMsg = title;
        } else {
            titleMsg = this.getDefaultTitle(typeModal);
        }

        let confirmButtonClass = '';
        if (confirmButtonText === this.BTN_NAME_ELIMINAR) {
            confirmButtonClass = 'btn btn-quaternary';
        } else {
            confirmButtonClass = 'btn btn-primary';
        }

        return swal({
            title: titleMsg,
            html: message,
            type: typeModal,
            showCancelButton: true,
            showCloseButton: true,
            confirmButtonClass: confirmButtonClass,
            cancelButtonClass: 'btn btn-tertiary',
            confirmButtonText: confirmButtonText,
            cancelButtonText: this.BTN_NAME_CANCELAR,
            buttonsStyling: false,
            reverseButtons: true,
            allowOutsideClick: allowOutsideClick
        });
    }

    static showModalConfirmInputText(message: string, allowOutsideClick: boolean, typeModal: any, confirmButtonText: string, title?: string): Promise<any> {
        let titleMsg = '';

        if (title) {
            titleMsg = title;
        } else {
            titleMsg = this.getDefaultTitle(typeModal);
        }

        let confirmButtonClass = '';
        if (confirmButtonText === this.BTN_NAME_ELIMINAR) {
            confirmButtonClass = 'btn btn-quaternary';
        } else {
            confirmButtonClass = 'btn btn-primary';
        }

        return swal({
            title: titleMsg,
            input: 'text',
            // preConfirm: (value) => {
            //           throw new Error(value);
            //   },


            html: message,
            type: typeModal,
            showCancelButton: true,
            confirmButtonClass: confirmButtonClass,
            cancelButtonClass: 'btn btn-tertiary',
            confirmButtonText: confirmButtonText,
            cancelButtonText: this.BTN_NAME_CANCELAR,
            buttonsStyling: false,
            reverseButtons: true,
            allowOutsideClick: allowOutsideClick
        });
    }

    static getDefaultTitle(typeModal: string): string {
        switch (typeModal) {
            case 'success': {
                return 'ÉXITO';
            }
            case 'error': {
                return 'ERROR';
            }
            case 'warning': {
                return 'PRECAUCIÓN';
            }
            case 'info': {
                return 'INFORMACIÓN';
            }
            case 'question': {
                return 'PREGUNTA';
            }
            default: {
                return 'INFORMACIÓN';
            }
        }
    }
}
