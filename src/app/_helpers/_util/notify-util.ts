declare const $: any;

// @Injectable({ providedIn: 'root' })
export class NotifylUtil {

    // Froms
    static FROM_TOP = 'top';
    static FROM_BOTTOM = 'bottom';

    // Aligns
    static ALIGN_LEFT = 'left';
    static ALIGN_CENTER = 'center';
    static ALIGN_RIGHT = 'right';

    // Types notify
    static TYPE_NOTIFY_INFO = 'info';
    static TYPE_NOTIFY_SUCCESS = 'success';
    static TYPE_NOTIFY_WARNING = 'warning';
    static TYPE_NOTIFY_ERROR = 'danger';

    // Messages
    static MESSAGE_ERROR_GENERICO = 'Parece que ocurrió un error al procesar la solicitud, inténtalo de nuevo más tarde';

    static showNotification(from: string, align: string, typeNotify: string, message: string, title?: string, requestId?: string) {
        let titleMsg = '';
        let msg = '';

        if (title && title.length > 0) {
            titleMsg = title;
        } else {
            switch (typeNotify) {
                case 'info': {
                    titleMsg = 'Información';
                    break;
                }
                case 'success': {
                    titleMsg = 'Éxito';
                    break;
                }
                case 'warning': {
                    titleMsg = 'Precaución';
                    break;
                }
                case 'danger': {
                    titleMsg = 'Error';
                    break;
                }
                default: {
                    break;
                }
            }
        }

        msg = `<center><b>${titleMsg}</b><br>${message}</center>`;

        if (requestId && requestId.length > 0) {
            msg += `<br><center><b>ID: ${requestId}</b></center>`;
        }

        $.notify({
            icon: 'notifications',
            message: msg
        }, {
                type: typeNotify,
                timer: 10,
                placement: {
                    from: from,
                    align: align
                },
                template:
                    `
                <div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0} alert-with-icon" role="alert">
                    <button mat-raised-button type="button" aria-hidden="true" class="close" data-notify="dismiss">
                    <i class="material-icons">close</i>
                    </button>
                    <i class="material-icons" data-notify="icon">notifications</i>
                    <span data-notify="title">{1}</span>
                    <span data-notify="message">{2}</span>
                    <div class="progress" data-notify="progressbar">
                    <div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"
                        style="width: 0%;"></div>
                    </div>
                    <a href="{3}" target="{4}" data-notify="url"></a>
                </div>
              `
            });
    }

}
