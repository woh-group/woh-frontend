import { Config } from '../../_configs/config';

export class RequestUtil {

    static generateRequestId(apiName?: string): string {
        return this.getInitialsApi(apiName) + new Date().getTime() + this.getRandomInt(1, 100);
    }

    static getLocation(): string {
        return localStorage.getItem('location') ? localStorage.getItem('location') : null;
    }

    static getInitialsApi(apiName: string): string {
        switch (apiName) {
            // Comunes
            case Config.apiComunesPaises: {
                return 'ACOPAI';
            }
            case Config.apiComunesDepartamentos: {
                return 'ACODEP';
            }
            case Config.apiComunesCiudades: {
                return 'ACOCIU';
            }
            case Config.apiComunesTiposDocumentos: {
                return 'ACOTDO';
            }
            case Config.apiComunesNivelesAcademicos: {
                return 'ACONAC';
            }
            case Config.apiComunesProfesiones: {
                return 'ACOPRO';
            }
            // Usuarios
            case Config.apiUsuariosAutenticacion: {
                return 'AUSAUT';
            }
            case Config.apiUsuariosUsuarios: {
                return 'AUSUSU';
            }
            case Config.apiUsuariosRoles: {
                return 'AUSROL';
            }
            case Config.apiUsuariosPersonas: {
                return 'AUSPER';
            }
            // Preguntas
            case Config.apiPreguntasPreguntas: {
                return 'APRPRE';
            }
            case Config.apiPreguntasRespuestas: {
                return 'APRRES';
            }
            case Config.apiPreguntasCategorias: {
                return 'APRCAT';
            }
            default: {
                return '';
            }
        }
    }

    static getRandomInt(min: number, max: number): number {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
}
