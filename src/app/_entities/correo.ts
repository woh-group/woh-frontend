export class Correo {
    destinatario: string;
    remitente: string;
    remitenteNombre: string;
    asunto: string;
    mensaje: string;
    palabras: Map<string, string>[];
    nombrePlantilla: string;
}
