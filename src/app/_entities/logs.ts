export class Logs {
    _id: string;
    fecha: string;
    tipo: string;
    level: string;
    requestId: string;
    appId: number;
    username: string;
    ipCliente: string;
    ipServidor: string;
    api: string;
    metodo: string;
    uri: string;
    body: string;
}
