export class Profesion {
    _id: string;
    id: number;
    nombre: string;
    estado: boolean;
    fechaCreacion: Date;
    usuarioCreacion: string;
    fechaModificacion: Date;
    usuarioModificacion: string;
}
