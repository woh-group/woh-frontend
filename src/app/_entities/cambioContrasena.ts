export class CambioContrasena {
    username: string;
    oldPassword: string;
    newPassword: string;
}
