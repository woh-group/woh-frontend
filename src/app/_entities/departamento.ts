export class Departamento {
    _id: string;
    id: string;
    nombre: string;
    paisId: string;
    estado: boolean;
    fechaCreacion: Date;
    usuarioCreacion: string;
    fechaModificacion: Date;
    usuarioModificacion: string;
}
