export class Persona {
    _id: string;
    tipoDocumentoId: number;
    numeroDocumento: string;
    nombres: string;
    apellidos: string;
    sexo: string;
    telefono: string;
    celular: string;
    fechaNacimiento: Date;
    estadoCivil: string;
    correo: string;
    direccion: string;
    codigoPostal: string;
    paisId: string;
    departamentoId: string;
    ciudadId: string;
    fechaCreacion: Date;
    usuarioCreacion: string;
    fechaModificacion: Date;
    usuarioModificacion: string;
}
