export class Pais {
    _id: string;
    id: string;
    codigo: string;
    nombre: string;
    continente: string;
    indicativo: string;
    estado: boolean;
    fechaCreacion: Date;
    usuarioCreacion: string;
    fechaModificacion: Date;
    usuarioModificacion: string;
}
