import { PerfilProfesional } from './perfilProfesional';
import { Persona } from './persona';

export class Profesional {
    _id: string;
    usuarioId: string;
    especialidadPrincipal: string;
    perfiles: PerfilProfesional[];
    idiomas: string[];
    persona: Persona;
    reputacion: number;
    estado: boolean;
    fechaCreacion: Date;
    usuarioCreacion: string;
    fechaModificacion: Date;
    usuarioModificacion: string;
}
