export class CategoriaPregunta {
    _id: string;
    id: string;
    descripcion: string;
    categoriaPadreId: string;
    padres: CategoriaPregunta[];
    hijos: CategoriaPregunta[];
    estado: boolean;
    fechaCreacion: Date;
    usuarioCreacion: string;
    fechaModificacion: Date;
    usuarioModificacion: string;
}
