import {Respuesta} from './respuesta';

export class Pregunta {
    _id: string;
    pregunta: string;
    informacionAdicional: string;
    informacionAdicionalArray: string[];
    categoriaId: string;
    categoriaNombre: string;
    usuarioId: string;
    genero: string;
    edad: number;
    fecha: Date;
    fechaUltimaRespuesta: Date;
    visitas: number;
    respuestas: Respuesta[];
    fechaCreacion: Date;
    usuarioCreacion: string;
    fechaModificacion: Date;
    usuarioModificacion: string;
}
