import {Persona} from '../persona';

export class Respuesta {
    _id: string;
    respuesta: string;
    respuestaArray: string[];
    usuarioId: string;
    likes: number;
    dislikes: number;
    checkUtil: boolean;
    persona: Persona;
    fechaCreacion: Date;
    usuarioCreacion: string;
    fechaModificacion: Date;
    usuarioModificacion: string;
}
