export class Ciudad {
    _id: string;
    id: string;
    nombre: string;
    departamentoId: string;
    estado: boolean;
    fechaCreacion: Date;
    usuarioCreacion: string;
    fechaModificacion: Date;
    usuarioModificacion: string;
}
