export class TokenInfo {
    sub: string;
    rol: number;
    permisos: {
        ADD: boolean
        DELETE: boolean
        READ: boolean
        UPDATE: boolean
    };
}
