export class Experiencia {
    _id: string;
    perfilProfesionalId: string;
    empresa: string;
    cargo: string;
    fechaInicio: Date;
    fechaFin: Date;
    funciones: string;
    actualmente: boolean;
    fechaCreacion: Date;
    usuarioCreacion: string;
    fechaModificacion: Date;
    usuarioModificacion: string;
}
