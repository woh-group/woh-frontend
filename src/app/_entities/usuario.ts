import { Persona } from './persona';

export class Usuario {
    _id: string;
    imagenPerfil: File;
    username: string;
    password: string;
    cambiarContrasena: boolean;
    estadoId: number;
    rol: number;
    permisos: any;
    aceptoRecibirPublicidad: boolean;
    versionTerminosCondiciones: number;
    persona: Persona;
    fechaCreacion: Date;
    usuarioCreacion: string;
    fechaModificacion: Date;
    usuarioModificacion: string;
}
