export class NivelAcademico {
    _id: string;
    id: number;
    descripcion: string;
    estado: boolean;
    fechaCreacion: Date;
    usuarioCreacion: string;
    fechaModificacion: Date;
    usuarioModificacion: string;
}
