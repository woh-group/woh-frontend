export class TipoDocumento {
    _id: string;
    descripcion: string;
    abreviatura: string;
    estado: boolean;
    fechaCreacion: Date;
    usuarioCreacion: string;
    fechaModificacion: Date;
    usuarioModificacion: string;
}
