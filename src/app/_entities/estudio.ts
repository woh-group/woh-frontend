export class Estudio {
    _id: string;
    perfilProfesionalId: string;
    tipoEstudioId: number;
    institucion: string;
    nivelAcademicoId: number;
    titulo: string;
    estado: string;
    fechaInicio: Date;
    fechaFin: Date;
    tarjetaProfesional: string;
    fechaCreacion: Date;
    usuarioCreacion: string;
    fechaModificacion: Date;
    usuarioModificacion: string;
}
