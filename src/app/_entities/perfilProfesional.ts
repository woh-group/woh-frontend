import { Estudio } from './estudio';
import { Experiencia } from './experiencia';

export class PerfilProfesional {
    _id: string;
    profesionId: number;
    perfil: string;
    estudios: Estudio[];
    estudiosComplementarios: Estudio[];
    experiencias: Experiencia[];
    especialidades: string[];
    indicativo: string;
    estado: boolean;
    fechaCreacion: Date;
    usuarioCreacion: string;
    fechaModificacion: Date;
    usuarioModificacion: string;
}
