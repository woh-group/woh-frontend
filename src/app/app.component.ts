
import { Component, OnInit, ViewChild, AfterViewChecked, ChangeDetectorRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';

import { JwtUtil } from './_helpers/_util/jwt-util';
import { StringUtil } from './_helpers/_util/string-util';

import { environment } from '../environments/environment';

import { AutenticacionService } from './_services/autenticacion.service';
import { UsuarioService } from './_services/usuario.service';
import { GeolocationService } from './_services/geolocation.service';

import { Persona } from './_entities/persona';
import { DateAdapter } from '@angular/material';
import { LoaderService } from './_services/_service-util/loader.service';
import { Subscription } from 'rxjs';

// Google analytics
declare var gtag;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewChecked {

  // VARIABLES PARA SRC DE IMAGEN DE PERFIL
  srcImgPerfil: any;
  subscription: Subscription;

  showLoader = false;

  menuLeft2 = new FormControl();
  @ViewChild('menuLeft') sidenav: any;
  showMenuIcon = true;
  showSubmenu = false;

  body = document.getElementsByTagName('body')[0];
  isHome = false;
  loggedIn = false;
  persona: Persona;
  usuarioId: string;
  rol = 0;
  routeFiles = environment.endpointBucketFiles;
  currentLat: any;
  currentLong: any;

  constructor(
    private router: Router,
    private autenticacionService: AutenticacionService,
    private usuarioService: UsuarioService,
    private loaderService: LoaderService,
    private geolocationService: GeolocationService,
    private adapter: DateAdapter<any>,
    private cdRef: ChangeDetectorRef
  ) {
    // Google analytics
    if (environment.production) {
      const navEndEvents$ = this.router.events
        .pipe(
          filter((event) => event instanceof NavigationEnd)
        );

      navEndEvents$.subscribe((event: NavigationEnd) => {
        gtag('config', 'UA-145704275-1', {
          page_path: event.urlAfterRedirects
        });
      });
    }

    router.events.subscribe((val) => {
      if (this.router.url.includes('/home')) {
        this.isHome = true;
        // console.log('path', this.router.url);
        this.body.classList.remove('body-background-other');
        this.body.classList.add('body-background-home');
      } else {
        this.isHome = false;
        this.body.classList.remove('body-background-home');
        this.body.classList.add('body-background-other');
      }
    });

    // Validamos si esta logueado
    this.autenticacionService.currentUser.subscribe(response => {
      if (response && response.accessToken) {
        this.loggedIn = true;
        const tokenInfo = JwtUtil.decodeToken(response.accessToken);
        this.rol = tokenInfo.rol;

        // Obtenemos los datos personales del usuario
        if (!this.autenticacionService.getCurrentPersona()) {
          this.usuarioId = tokenInfo.sub;
          this.getImgPerfil();
          // Obtenemos los datos de la persona
          this.usuarioService.getPersonaByUsuarioId(this.usuarioId).subscribe((responsePersona: any) => {
            // console.log('responsePersona:', JSON.stringify(responsePersona));
            if (responsePersona && responsePersona.status === 200) {
              this.persona = responsePersona.body;
              this.autenticacionService.savePersonalocalStorage(this.persona);
            }
          });
        } else {
          this.persona = this.autenticacionService.getCurrentPersona();
          this.usuarioId = tokenInfo.sub;
        }
        this.getImgPerfil();
      } else {
        this.loggedIn = false;
      }
    }, error => {
      this.loggedIn = false;
    });
  }

  ngOnInit() {
    const currentDate = new Date();
    this.usuarioService.setSrcImgPerfil(this.usuarioId, currentDate.getMinutes(), currentDate.getSeconds());
    this.adapter.setLocale('es');
    this.loader();
    this.findMe();
  }

  getImgPerfil() {
    this.subscription = this.usuarioService.getSrcImgPerfil().subscribe(src => {
      this.srcImgPerfil = src.text;
    });
  }

  findMe() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition((position) => {
          const longitude = position.coords.longitude;
          const latitude = position.coords.latitude;

          this.geolocationService.getReverseGeocoding(latitude, longitude).subscribe((response: any) => {
            // console.log(response);
            if (response) {
              const location = response.address;
              location.type = response.type;
              localStorage.setItem('location', StringUtil.eliminarTildes(JSON.stringify(location)));
            }
          });
        });
    } else {
       console.log('Navegador no sorporta localizacion');
    }
  }

  openCloseMenuLeft() {
    if (this.sidenav.opened) {
      this.sidenav.toggle();
      // this.showMenuIcon = true;
    } else {
      this.sidenav.open();
      // this.showMenuIcon = false;
    }
  }

  logout() {
    this.autenticacionService.logout();
    this.loggedIn = false;
    this.router.navigate(['/home']);
  }

  subirScroll() {
    window.scrollTo(0, 0);
  }

  goTo(location: string): void {
    window.location.hash = location;
  }

  loader() {
    this.loaderService.status.subscribe((val: boolean) => {
      this.showLoader = val;
    });
  }

  ngAfterViewChecked(): void {
    this.cdRef.detectChanges();
  }

}
