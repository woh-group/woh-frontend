import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app.routing.module';

import { Ng2ImgMaxModule } from 'ng2-img-max';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDialogModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatStepperModule,
} from '@angular/material';
import { MatDatepickerModule } from '@angular/material/datepicker';

// Pipes
import { TitleCaseFirstLetterPipe } from './_helpers/_pipes/title-case-first-letter.pipe';

// Services
import { AutenticacionService } from './_services/autenticacion.service';
import { PaisService } from './_services/comunes/pais.service';
import { DepartamentoService } from './_services/comunes/departamento.service';
import { ProfesionalPerfilService } from './_services/profesionales/perfil.service';
import { PreguntaService } from './_services/preguntas/pregunta.service';
import { RespuestaService } from './_services/preguntas/respuesta.service';
import { CategoriaPreguntaService } from './_services/preguntas/categoria.service';
import { TraceService } from './_services/trace/trace.service';
import { CorreoService } from './_services/comunes/correo.service';
import { LoaderService } from './_services/_service-util/loader.service';

// Components
import { AppComponent } from './app.component';
import { HomeComponent } from './_components/home/home.component';
import { DatosUsuarioComponent } from './_components/usuarios/perfil/datos-usuario/datos-usuario.component';
import { DatosPersonalesComponent } from './_components/usuarios/perfil/datos-personales/datos-personales.component';
import { FooterComponent } from './_components/footer/footer.component';
import { LoginComponent } from './_components/login/login.component';
import { FieldErrorDisplayComponent } from './_components/_utils/field-error-display/field-error-display.component';
import { PaginationComponent } from './_components/_utils/pagination/pagination.component';

import { PerfilComponent } from './_components/usuarios/perfil/perfil/perfil.component';
import { PerfilProfesionalComponent } from './_components/usuarios/perfil/perfil-profesional/perfil-profesional.component';
import { PerfilDatosPersonalesComponent } from './_components/usuarios/perfil/perfil-datos-personales/perfil-datos-personales.component';
import { IdiomasComponent } from './_components/usuarios/perfil/idiomas/idiomas.component';
import { EstudiosComponent } from './_components/usuarios/perfil/estudios/estudios.component';
import { ExperienciasComponent } from './_components/usuarios/perfil/experiencias/experiencias.component';
import { EspecialidadesComponent } from './_components/usuarios/perfil/especialidades/especialidades.component';
import { ActivarUsuarioComponent } from './_components/usuarios/activar-usuario/activar-usuario.component';

// Preguntas
import { PreguntaBuscadorComponent } from './_components/preguntas/pregunta-buscador/pregunta-buscador.component';
import { PreguntaResultadosComponent } from './_components/preguntas/pregunta-resultados/pregunta-resultados.component';
import { PreguntaCrearComponent } from './_components/preguntas/pregunta-crear/pregunta-crear.component';
import { PreguntaSelectCategoriaComponent } from './_components/preguntas/pregunta-select-categoria/pregunta-select-categoria.component';
import { PreguntaDetalleComponent } from './_components/preguntas/pregunta-detalle/pregunta-detalle.component';
import { ListPreguntasComponent } from './_components/preguntas/list-preguntas/list-preguntas.component';
import { NuestrosServiciosComponent } from './_components/nuestros-servicios/nuestros-servicios.component';
import { ProfesionalesComponent } from './_components/profesionales/profesionales.component';
import { ProfesionalPerfilPublicoComponent } from './_components/profesional-perfil-publico/profesional-perfil-publico.component';
import { PatrocinadoresComponent } from './_components/patrocinadores/patrocinadores.component';
import { PreguntasRankingComponent } from './_components/preguntas/preguntas-ranking/preguntas-ranking.component';

// Admin
import { LogsComponent } from './_components/admin/logs/logs.component';
import { LogDialog } from './_components/_dialogs/logs/log.dialog';
import { UsuariosComponent } from './_components/admin/usuarios/usuarios.component';
import { ReportsComponent } from './_components/admin/reports/reports.component';
import { UsuarioDialogComponent } from './_components/admin/usuarios/usuarios.component';


import { TerminosDialog } from './_components/_dialogs/terminosYcondiciones/terminos.dialog';
import { ContactoComponent } from './_components/contacto/contacto.component';
import { PreguntarComponent } from './_components/preguntas/preguntar/preguntar.component';
import { OlvidoContrasenaComponent } from './_components/usuarios/olvido-contrasena/olvido-contrasena.component';
import { RestablecerContrasenaComponent } from './_components/usuarios/restablecer-contrasena/restablecer-contrasena.component';
import { ResultadoPagoComponent } from './_components/resultado-pago/resultado-pago.component';
import { AgendarCitaComponent } from './_components/agendar-cita/agendar-cita.component';
import { PlanesComponent } from './_components/planes/planes.component';


@NgModule({
  exports: [
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule
  ],
  declarations: []
})
export class MaterialModule { }

@NgModule({
  declarations: [
    AppComponent,
    TitleCaseFirstLetterPipe,
    HomeComponent,
    DatosUsuarioComponent,
    DatosPersonalesComponent,
    FooterComponent,
    LoginComponent,
    FieldErrorDisplayComponent,
    PaginationComponent,
    ProfesionalesComponent,
    ProfesionalPerfilPublicoComponent,
    PatrocinadoresComponent,
    ContactoComponent,
    // Usuarios
    IdiomasComponent,
    EstudiosComponent,
    ExperienciasComponent,
    EspecialidadesComponent,
    PerfilComponent,
    PerfilProfesionalComponent,
    PerfilDatosPersonalesComponent,
    ActivarUsuarioComponent,
    OlvidoContrasenaComponent,
    RestablecerContrasenaComponent,
    // Preguntas
    PreguntaBuscadorComponent,
    PreguntaResultadosComponent,
    PreguntaCrearComponent,
    PreguntaSelectCategoriaComponent,
    PreguntaDetalleComponent,
    ListPreguntasComponent,
    NuestrosServiciosComponent,
    PreguntasRankingComponent,
    PreguntarComponent,
    // ADMIN
    LogsComponent,
    UsuariosComponent,
    ReportsComponent,
    // DIALOGS
    LogDialog,
    UsuarioDialogComponent,
    TerminosDialog,
    // PAGO
    ResultadoPagoComponent,
    PlanesComponent,
    // AGENDAR
    AgendarCitaComponent
  ],
  imports: [
    AppRoutingModule,
    CommonModule,
    BrowserModule,
    RouterModule,
    MaterialModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    Ng2ImgMaxModule
  ],
  providers: [
    AutenticacionService,
    PaisService,
    DepartamentoService,
    ProfesionalPerfilService,
    PreguntaService,
    RespuestaService,
    CategoriaPreguntaService,
    TraceService,
    CorreoService,
    LoaderService
  ],
  bootstrap: [AppComponent],
  entryComponents: [LogDialog, UsuarioDialogComponent, TerminosDialog]
})
export class AppModule { }
