import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, Subject } from 'rxjs';
import { catchError, tap, timeout } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';

import { Config } from '../_configs/config';

import { HeadersUtil } from './_service-util/headers-util';
import { ParamsUtil } from './_service-util/params-util';
import { ServiceMessageUtil } from './_service-util/service-message-util';
import { ImageFormatUtil } from '../_helpers/_util/image-format-util';

import { AutenticacionService } from './autenticacion.service';

import { Usuario } from '../_entities/usuario';
import { Persona } from '../_entities/persona';
import { CambioContrasena } from '../_entities/CambioContrasena';

@Injectable({ providedIn: 'root' })
export class UsuarioService {

  private srcImgPerfilsubject = new Subject<any>();
  currentDate = new Date();
  routeFiles = environment.endpointBucketFiles;

  private urlApi = '/api/usuarios/usuarios';
  private endpoint = environment.endpointUsuarios + this.urlApi;
  private apiName = Config.apiUsuariosUsuarios;

  // Operaciones
  private metodoGetAll = 'getAll';
  private metodoGetById = 'getById';
  private metodoGetPersonaByUsuarioId = 'getPersonaByUsuarioId';
  private metodoCount = 'count';
  private metodoAdd = 'add';
  private metodoUpdate = 'update';
  private metodoUpdatePersona = 'updatePersona';
  private metodoDelete = 'delete';
  private metodoUploadProfileImage = 'uploadProfileImage';
  private metodoCambiarContrasena = 'cambiarContrasena';
  private metodoRestablecerContrasena = 'restablecerContrasena';
  private metodoIsValidUsername = 'isValidUsername';
  private metodoActivarUsuario = 'activarUsuario';

  constructor(private http: HttpClient, private router: Router, private autenticacionService: AutenticacionService) { }

  getAll(paramsMap: Map<string, string>): Observable<Response> {
    // Get currentUser
    const user = this.autenticacionService.getCurrentUser();

    // Headers
    const headers = HeadersUtil.getHeadersLoggedInUser(this.apiName, user);

    return this.http.get(this.endpoint, { headers: headers, params: ParamsUtil.mapToHttpParams(paramsMap), observe: 'response' })
      .pipe(
        timeout(30000),
        tap(_ => {
          this.autenticacionService.refreshToken();
        }),
        catchError(this.handleError<any>(`${this.apiName} - ${this.metodoGetAll}`))
      );
  }

  getById(id: string): Observable<Response> {
    // Get currentUser
    const user = this.autenticacionService.getCurrentUser();

    // Headers
    let headers = new HttpHeaders();
    // Headers
    if (user) {
      headers = HeadersUtil.getHeadersLoggedInUser(this.apiName, user);
    } else {
      headers = HeadersUtil.getHeadersUnknownUser(this.apiName);
    }

    const url = this.endpoint + '/' + id;
    return this.http.get(url, { headers: headers, observe: 'response' })
      .pipe(
        timeout(30000),
        tap(_ => {
          this.autenticacionService.refreshToken();
        }),
        catchError(this.handleError<any>(`${this.apiName} - ${this.metodoGetById}`))
      );
  }

  getPersonaByUsuarioId(id: string): Observable<Response> {
    // Get currentUser
    const user = this.autenticacionService.getCurrentUser();

    // Headers
    const headers = HeadersUtil.getHeadersLoggedInUser(this.apiName, user);

    const url = this.endpoint + '/getPersonaByUsuarioId/' + id;
    return this.http.get(url, { headers: headers, observe: 'response' })
      .pipe(
        timeout(30000),
        tap(_ => {
          this.autenticacionService.refreshToken();
        }),
        catchError(this.handleError<any>(`${this.apiName} - ${this.metodoGetPersonaByUsuarioId}`))
      );
  }

  count(paramsMap: Map<string, string>): Observable<number> {
    // Get currentUser
    const user = this.autenticacionService.getCurrentUser();

    // Headers
    const headers = HeadersUtil.getHeadersUnknownUser(this.apiName);

    const url = this.endpoint + '/count';
    return this.http.get<number>(url, { headers: headers, params: ParamsUtil.mapToHttpParams(paramsMap) })
      .pipe(
        timeout(30000),
        tap(_ => {
          this.autenticacionService.refreshToken();
        }),
        catchError(this.handleError<number>(`${this.apiName} - ${this.metodoCount}`))
      );
  }

  add(obj: Usuario): Observable<Response> {
    // Get currentUser
    const user = this.autenticacionService.getCurrentUser();

    // Headers
    let headers = new HttpHeaders();

    if (user) {
      headers = HeadersUtil.getHeadersLoggedInUser(this.apiName, user);
    } else {
      headers = HeadersUtil.getHeadersUnknownUser(this.apiName);
    }

    return this.http.post(this.endpoint, obj, { headers: headers, observe: 'response' })
      .pipe(
        timeout(30000),
        tap(_ => {
          this.autenticacionService.refreshToken();
        }),
        catchError(this.handleError<any>(`${this.apiName} - ${this.metodoAdd}`))
      );
  }

  update(id: string, obj: Usuario): Observable<Response> {
    // Get currentUser
    const user = this.autenticacionService.getCurrentUser();

    // Headers
    const headers = HeadersUtil.getHeadersLoggedInUser(this.apiName, user);

    const url = this.endpoint + '/' + id;
    return this.http.put(url, obj, { headers: headers, observe: 'response' })
      .pipe(
        timeout(30000),
        tap(_ => {
          this.autenticacionService.refreshToken();
        }),
        catchError(this.handleError<any>(`${this.apiName} - ${this.metodoUpdate}`))
      );
  }

  updatePersona(id: string, obj: Persona): Observable<Response> {
    // Get currentUser
    const user = this.autenticacionService.getCurrentUser();

    // Headers
    const headers = HeadersUtil.getHeadersLoggedInUser(this.apiName, user);

    const url = this.endpoint + '/updatePersona/' + id;
    return this.http.put(url, obj, { headers: headers, observe: 'response' })
      .pipe(
        timeout(30000),
        tap(_ => {
          this.autenticacionService.refreshToken();
        }),
        catchError(this.handleError<any>(`${this.apiName} - ${this.metodoUpdatePersona}`))
      );
  }

  delete(id: string): Observable<Response> {
    // Get currentUser
    const user = this.autenticacionService.getCurrentUser();

    // Headers
    const headers = HeadersUtil.getHeadersLoggedInUser(this.apiName, user);

    const url = this.endpoint + '/' + id;
    return this.http.delete(url, { headers: headers, observe: 'response' })
      .pipe(
        timeout(30000),
        tap(_ => {
          this.autenticacionService.refreshToken();
        }),
        catchError(this.handleError<any>(`${this.apiName} - ${this.metodoDelete}`))
      );
  }

  cambiarContrasena(obj: CambioContrasena): Observable<Response> {
    // Get currentUser
    const user = this.autenticacionService.getCurrentUser();

    // Headers
    let headers = null;
    if (user) {
      headers = HeadersUtil.getHeadersLoggedInUser(this.apiName, user);
    } else {
      headers = HeadersUtil.getHeadersUnknownUser(this.apiName);
    }

    const url = this.endpoint + '/cambiarContrasena';
    return this.http.patch(url, obj, { headers: headers, observe: 'response' })
      .pipe(
        timeout(30000),
        tap(_ => {
          this.autenticacionService.refreshToken();
        }),
        catchError(this.handleError<any>(`${this.apiName} - ${this.metodoCambiarContrasena}`))
      );
  }

  restablecerContrasena(id: string): Observable<Response> {
    // Get currentUser
    const user = this.autenticacionService.getCurrentUser();

    // Headers
    const headers = HeadersUtil.getHeadersLoggedInUser(this.apiName, user);

    const url = this.endpoint + '/restablecerContrasena/' + id;
    return this.http.patch(url, null, { headers: headers, observe: 'response' })
      .pipe(
        timeout(30000),
        tap(_ => {
          this.autenticacionService.refreshToken();
        }),
        catchError(this.handleError<any>(`${this.apiName} - ${this.metodoRestablecerContrasena}`))
      );
  }

  uploadProfileImage(obj: File, usuarioId: string): Observable<Response> {
    // Get currentUser
    const user = this.autenticacionService.getCurrentUser();

    // Headers
    let headers = null;
    if (user) {
      headers = HeadersUtil.getHeadersLoggedInUser(this.apiName, user);
    } else {
      headers = HeadersUtil.getHeadersUnknownUser(this.apiName);
    }

    const url = this.endpoint + '/subirImagen';
    const input = new FormData();
    // console.log('///////// ', obj.type, ImageFormatUtil.formatImage(obj.type));
    // Add your values in here
    input.append('uploadfile', obj, usuarioId + ImageFormatUtil.formatImage(obj.type));
    return this.http.post<any>(url, input, { headers: headers, observe: 'response' })
      .pipe(
        timeout(30000),
        tap(_ => {
          this.autenticacionService.refreshToken();
        }),
        catchError(this.handleError<any>(`${this.apiName} - ${this.metodoUploadProfileImage}`))
      );
  }

  isValidUsername(username: string): Observable<boolean> {

    // Headers
    const headers = HeadersUtil.getHeadersUnknownUser(this.apiName);

    const url = this.endpoint + '/isValidUsername/' + username;
    return this.http.get<boolean>(url, { headers: headers })
      .pipe(
        timeout(30000),
        tap(_ => {
          this.autenticacionService.refreshToken();
        }),
        catchError(this.handleError<boolean>(`${this.apiName} - ${this.metodoIsValidUsername}`))
      );
  }

  activarUsuario(id: string, activationCode: string): Observable<Response> {

    // Headers
    const headers = HeadersUtil.getHeadersUnknownUser(this.apiName);

    const url = this.endpoint + '/activarUsuario/' + id + '/' + activationCode;
    return this.http.patch(url, null, { headers: headers, observe: 'response' })
      .pipe(
        timeout(30000),
        tap(_ => {
          this.autenticacionService.refreshToken();
        }),
        catchError(this.handleError<any>(`${this.apiName} - ${this.metodoActivarUsuario}`))
      );
  }

  getOlvidoContrasena(username: string): Observable<Response> {
    // Headers
    const headers = HeadersUtil.getHeadersUnknownUser(this.apiName);

    const url = this.endpoint + '/olvidoContrasena/' + username;
    return this.http.get(url, { headers: headers, observe: 'response' })
      .pipe(
        timeout(30000),
        tap(_ => {
          this.autenticacionService.refreshToken();
        }),
        catchError(this.handleError<any>(`${this.apiName} - ${this.metodoGetById}`))
      );
  }

  getEstadoC(_id: string): Observable<Response> {
    // Headers
    const headers = HeadersUtil.getHeadersUnknownUser(this.apiName);

    const url = this.endpoint + '/estadoC/' + _id;
    return this.http.get(url, { headers: headers, observe: 'response' })
      .pipe(
        timeout(30000),
        tap(_ => {
          this.autenticacionService.refreshToken();
        }),
        catchError(this.handleError<any>(`${this.apiName} - ${this.metodoGetById}`))
      );
  }

  // METODOS PARA SUSCRIPCION DE IMAGEN DE PERFIL
  setSrcImgPerfil(id: string, minutes: number, seconds: number) {
    const src = this.routeFiles + id + '.jpg?' + minutes + seconds;
    this.srcImgPerfilsubject.next({ text: src });
  }

  getSrcImgPerfil(): Observable<any> {
    return this.srcImgPerfilsubject.asObservable();
  }
  // FIN METODOS PARA SUSCRIPCION DE IMAGEN DE PERFIL

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      switch (error.status) {
        case 401: {
          ServiceMessageUtil.showErrorUnauthorizedRefreshToken(error, ServiceMessageUtil.TYPE_NOTIFICATION_MODAL).then((modalResp) => {
            if (modalResp.value) {
              this.autenticacionService.logout();
              this.router.navigate(['/home']);
            }
          });
          break;
        }
        default: {
          ServiceMessageUtil.isErrorGenerico(error, ServiceMessageUtil.TYPE_NOTIFICATION_ALERT);
          break;
        }
      }

      // console.log('error:', JSON.stringify(error));
      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
