import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of} from 'rxjs';
import { catchError, tap, timeout } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { Router } from '@angular/router';

import { Config } from '../../_configs/config';

import { HeadersUtil } from './../_service-util/headers-util';
import { ParamsUtil } from './../_service-util/params-util';
import { ServiceMessageUtil } from './../_service-util/service-message-util';

import { AutenticacionService } from './../autenticacion.service';

@Injectable({ providedIn: 'root' })
export class TraceService {

  private urlApi = '/api/trace';
  private endpoint = environment.endpointTrace + this.urlApi;
  private apiName = Config.apiTrace;


  // Operaciones
  private metodoGetAll = 'getAll';
  private metodoCount = 'count';

  constructor(private http: HttpClient, private router: Router, private autenticacionService: AutenticacionService) { }

  getAll(trace: any, api: any, paramsMap: Map<string, string>): Observable<Response> {
    // Get currentUser
    const user = this.autenticacionService.getCurrentUser();

    // Headers
    let headers = null;
    if (user) {
      headers = HeadersUtil.getHeadersLoggedInUser(this.apiName, user);
    } else {
      headers = HeadersUtil.getHeadersUnknownUser(this.apiName);
    }

    const url = this.endpoint + '/' + trace + '/' + api;
    return this.http.get(url, { headers: headers, params: ParamsUtil.mapToHttpParams(paramsMap), observe: 'response' })
      .pipe(
        timeout(30000),
        tap(_ => {
          this.autenticacionService.refreshToken();
        }),
        catchError(this.handleError<any>(`${this.apiName} - ${this.metodoGetAll}`))
      );
  }

  count(trace: any, api: any, paramsMap: Map<string, string>): Observable<number> {
    // Get currentUser
    const user = this.autenticacionService.getCurrentUser();

    // Headers
    let headers = null;
    if (user) {
      headers = HeadersUtil.getHeadersLoggedInUser(this.apiName, user);
    } else {
      headers = HeadersUtil.getHeadersUnknownUser(this.apiName);
    }

    const url = this.endpoint + '/' + trace + '/' + api + '/count';
    return this.http.get<number>(url, { headers: headers, params: ParamsUtil.mapToHttpParams(paramsMap) })
      .pipe(
        timeout(30000),
        tap(_ => {
          this.autenticacionService.refreshToken();
        }),
        catchError(this.handleError<number>(`${this.apiName} - ${this.metodoCount}`))
      );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      switch (error.status) {
        case 401: {
          ServiceMessageUtil.showErrorUnauthorizedRefreshToken(error, ServiceMessageUtil.TYPE_NOTIFICATION_MODAL).then((modalResp) => {
            if (modalResp.value) {
              this.autenticacionService.logout();
              this.router.navigate(['/home']);
            }
          });
          break;
        }
        default: {
          ServiceMessageUtil.isErrorGenerico(error, ServiceMessageUtil.TYPE_NOTIFICATION_ALERT);
          break;
        }
      }

      // console.log('error:', JSON.stringify(error));
      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
