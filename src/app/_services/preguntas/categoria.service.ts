import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from '../../../environments/environment';
import { catchError, tap, timeout } from 'rxjs/operators';
import { Router } from '@angular/router';

import { Config } from '../../_configs/config';

import { HeadersUtil } from './../_service-util/headers-util';
import { ParamsUtil } from './../_service-util/params-util';
import { ServiceMessageUtil } from './../_service-util/service-message-util';

import { AutenticacionService } from './../autenticacion.service';

import { CategoriaPregunta } from '../../_entities/preguntas/categoriaPregunta';


@Injectable({ providedIn: 'root' })
export class CategoriaPreguntaService {

  private categorias: any[] = [];
  // private categorias: CategoriaPregunta[] = [];
  private categoriasMap: Map<string, CategoriaPregunta> = new Map();

  private urlApi = '/api/preguntas/categorias';
  private endpoint = environment.endpointPreguntas + this.urlApi;
  private apiName = Config.apiPreguntasCategorias;

  // Operaciones
  private metodoGetAll = 'getAll';
  private metodoGetById = 'getById';
  private metodoCount = 'count';
  private metodoAdd = 'add';
  private metodoUpdate = 'update';
  private metodoDelete = 'delete';

  categoriaOtro = {
    id: '0.0',
    descripcion: 'Otro',
    categoriaPadreId: ''
  };

  constructor(private http: HttpClient, private router: Router, private autenticacionService: AutenticacionService) { }

  async getCategoriasCache(): Promise<CategoriaPregunta[]> {
    if (this.categorias && this.categorias.length > 0) {
      return this.categorias;
    } else {
      // Params
      const params = new Map<string, any>();
      params.set('sort', 'descripcion');
      params.set('estado', true);

      const response: any = await this.getAll(params).toPromise();
      // console.log('response:', response);
      if (response && response.status === 200) {
        this.categorias = response.body;
        // console.log('categorias', this.categorias);
      }

      // Asignamos al map
      if (this.categorias) {
        this.categoriasMap = new Map(this.categorias.map(obj => [obj.id, obj] as [string, CategoriaPregunta]));
        // console.log('map cache', this.categoriasMap);
      }
      return this.categorias;
    }
  }

  async getCategoriasMapCache(): Promise<Map<string, CategoriaPregunta>> {
    if (this.categoriasMap && this.categoriasMap.size > 0) {
      return this.categoriasMap;
    } else {
      // Params
      const params = new Map<string, any>();
      params.set('sort', 'descripcion');
      params.set('estado', true);

      const response: any = await this.getAll(params).toPromise();
      if (response && response.status === 200) {
        this.categorias = response.body;
      }

      if (this.categorias) {
        this.categoriasMap = new Map(this.categorias.map(obj => [obj.id, obj] as [string, CategoriaPregunta]));
      }
      return this.categoriasMap;
    }
  }

  getAll(paramsMap: Map<string, string>): Observable<Response> {
    // Get currentUser
    const user = this.autenticacionService.getCurrentUser();

    // Headers
    const headers = HeadersUtil.getHeadersUnknownUser(this.apiName);

    return this.http.get(this.endpoint, { headers: headers, params: ParamsUtil.mapToHttpParams(paramsMap), observe: 'response' })
      .pipe(
        timeout(30000),
        tap(_ => {
          this.autenticacionService.refreshToken();
        }),
        catchError(this.handleError<any>(`${this.apiName} - ${this.metodoGetAll}`))
      );
  }

  getById(id: string): Observable<Response> {
    // Get currentUser
    const user = this.autenticacionService.getCurrentUser();

    // Headers
    const headers = HeadersUtil.getHeadersLoggedInUser(this.apiName, user);

    const url = this.endpoint + '/' + id;
    return this.http.get(url, { headers: headers, observe: 'response' })
      .pipe(
        timeout(30000),
        tap(_ => {
          this.autenticacionService.refreshToken();
        }),
        catchError(this.handleError<any>(`${this.apiName} - ${this.metodoGetById}`))
      );
  }

  count(paramsMap: Map<string, string>): Observable<number> {
    // Get currentUser
    const user = this.autenticacionService.getCurrentUser();

    // Headers
    let headers = null;
    if (user) {
      headers = HeadersUtil.getHeadersLoggedInUser(this.apiName, user);
    } else {
      headers = HeadersUtil.getHeadersUnknownUser(this.apiName);
    }

    const url = this.endpoint + '/count';
    return this.http.get<number>(url, { headers: headers, params: ParamsUtil.mapToHttpParams(paramsMap) })
      .pipe(
        timeout(30000),
        tap(_ => {
          this.autenticacionService.refreshToken();
        }),
        catchError(this.handleError<number>(`${this.apiName} - ${this.metodoCount}`))
      );
  }

  add(obj: CategoriaPregunta): Observable<Response> {
    // Headers
    const headers = HeadersUtil.getHeadersUnknownUser(this.apiName);

    return this.http.post(this.endpoint, obj, { headers: headers, observe: 'response' })
      .pipe(
        timeout(30000),
        tap(_ => {
          this.autenticacionService.refreshToken();
        }),
        catchError(this.handleError<any>(`${this.apiName} - ${this.metodoAdd}`))
      );
  }

  update(id: string, obj: CategoriaPregunta): Observable<Response> {
    // Get currentUser
    const user = this.autenticacionService.getCurrentUser();

    // Headers
    const headers = HeadersUtil.getHeadersLoggedInUser(this.apiName, user);

    const url = this.endpoint + '/' + id;
    return this.http.put(url, obj, { headers: headers, observe: 'response' })
      .pipe(
        timeout(30000),
        tap(_ => {
          this.autenticacionService.refreshToken();
        }),
        catchError(this.handleError<any>(`${this.apiName} - ${this.metodoUpdate}`))
      );
  }

  delete(id: string): Observable<Response> {
    // Get currentUser
    const user = this.autenticacionService.getCurrentUser();

    // Headers
    const headers = HeadersUtil.getHeadersLoggedInUser(this.apiName, user);

    const url = this.endpoint + '/' + id;
    return this.http.delete(url, { headers: headers, observe: 'response' })
      .pipe(
        timeout(30000),
        tap(_ => {
          this.autenticacionService.refreshToken();
        }),
        catchError(this.handleError<any>(`${this.apiName} - ${this.metodoDelete}`))
      );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      switch (error.status) {
        case 401: {
          ServiceMessageUtil.showErrorUnauthorizedRefreshToken(error, ServiceMessageUtil.TYPE_NOTIFICATION_MODAL).then((modalResp) => {
            if (modalResp.value) {
              this.autenticacionService.logout();
              this.router.navigate(['/home']);
            }
          });
          break;
        }
        default: {
          ServiceMessageUtil.isErrorGenerico(error, ServiceMessageUtil.TYPE_NOTIFICATION_ALERT);
          break;
        }
      }

      // console.log('error:', JSON.stringify(error));
      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
