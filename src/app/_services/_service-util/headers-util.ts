import { HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';

import { ServiceConstants } from '../_service-util/service-constants';
import { RequestUtil } from '../../_helpers/_util/request-util';

import { Token } from '../../_entities/token';

export class HeadersUtil {

    static location = null;

    static getHeadersBasic(apiName: string) {

        let headers = new HttpHeaders()
            .append(ServiceConstants.APP_ID, environment.APP_ID)
            .append(ServiceConstants.REQUEST_ID, RequestUtil.generateRequestId(apiName));

        // Obtenemos la localizacion
        if (!this.location) {
            this.location = RequestUtil.getLocation();
        }

        // Validamos que la localizacion no sea null
        if (this.location) {
            headers = headers.append(ServiceConstants.LOCATION, this.location);
        }
        return headers;
    }

    static getHeadersUnknownUser(apiName: string) {
        let headers = this.getHeadersBasic(apiName);
        headers = headers.append(ServiceConstants.AUTHORIZATION, environment.tokenApiUnknownUser);
        return headers;
    }

    static getHeadersLoggedInUser(apiName: string, user: Token) {
        let headers = this.getHeadersBasic(apiName);
        headers = headers.append(ServiceConstants.AUTHORIZATION, user ? user.tokenType + ' ' + user.accessToken : '');
        return headers;
    }

}


