export const ARTICULOS = /el|la|los|las|un|una|unos|unas/g;
export const PREPOSICIONES = /a|ante|bajo|con|contra|de|desde|en|entre|hacia|hasta|para|por|pro|segun|sin|sobre|tras/g;
export const CONJUNCIONES = /y|e|ni|o|u|bien|ya|no|otro|tanto|como|pero|mas|aunque|sin embargo|no obstante|antes|por lo demas|sino|excepto|es|decir|esto|porque|pues|puesto|si|tal|que|siempre|luego|modo|conque|mientras|cuando/g;
export const SIGNOS = /[.*+?^${}()|[\]\\]/g;

export const ARTICULOS3 = 'el|la|los|las|un|una|unos|unas';
export const PREPOSICIONES3 = 'a|ante|bajo|con|contra|de|desde|en|entre|hacia|hasta|para|por|pro|segun|sin|sobre|tras';
export const CONJUNCIONES3 = 'y|e|ni|o|u|bien|ya|no|otro|tanto|como|pero|mas|aunque|sin embargo|no obstante|antes|por lo demas|sino|excepto|es|decir|esto|porque|pues|puesto|si|tal|que|siempre|luego|modo|conque|mientras|cuando';
export const OTROS3 = 'se|le|poco|han|estan|muy|mi|yo|tu|ella|nosotros|ustedes|ellos|';
export const SIGNOS3 = '¡|!|¿|?|.|,|:|;|+|-|*|/|(|)|{|}|[|]|"';


const ARTICULOS2 = ['el', 'la', 'los', 'las', 'un', 'una', 'unos', 'unas'];
const PREPOSICIONES2 = ['a', 'ante', '', 'bajo', 'con', 'contra', 'de', 'desde', 'en', 'entre', 'hacia', 'hasta', 'para', 'por', 'pro', 'segun', 'sin', 'sobre', 'tras'];
const CONJUNCIONES2 = ['y', 'e', 'ni', 'o', 'u', 'bien', 'ya', 'otro', 'tanto', 'como', 'pero', 'mas', 'aunque', 'sin embargo', 'no obstante', 'antes', 'por lo demas',
    'sino', 'excepto', 'es', 'decir', 'esto', 'porque', 'pues', 'puesto', 'si', 'tal', 'que', 'siempre', 'luego', 'modo', 'conque', 'mientras', 'cuando'];
const SIGNOS2 = ['¡', '!', '¿', '?', '.', ',', ':', ';', '+', '-', '*', '/', '(', ')', '{', '}', '[', ' ] ', '"', '', '', '', '', '', '', ''];
const XXXX = ['XXXX', ''];
