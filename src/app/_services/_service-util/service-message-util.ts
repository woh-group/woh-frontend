import { ModalUtil } from '../../_helpers/_util/modal-util';
import { NotifylUtil } from '../../_helpers/_util/notify-util';

import { Config } from '../../_configs/config';

export class ServiceMessageUtil {

    // Types
    static TYPE_NOTIFICATION_MODAL = 'MODAL';
    static TYPE_NOTIFICATION_ALERT = 'ALERT';

    static isErrorGenerico(error: any, typeNotification: string) {

        if (!window.navigator.onLine) {
            this.showNoHayConexion();
            return true;
        }

        switch (error.status) {
            case 400: {
                this.showErrorBadRequest(error, typeNotification);
                break;
            }
            case 401: {
                this.showErrorUnauthorized(error, typeNotification);
                break;
            }
            case 403: {
                this.showErrorForbidden(error, typeNotification);
                break;
            }
            case 404: {
                this.showErrorNotFound(error, typeNotification);
                break;
            }
            case 409: {
                this.showErrorConflict(error, typeNotification);
                break;
            }
            case 500: {
                this.showErrorGenerico(error);
                break;
            }
            default: {
                this.showErrorGenerico(error);
                break;
            }
        }
    }

    static showNoHayConexion() {
        const message = 'Parece que tu conexión de internet ha fallado, por favor comprueba tu conexión e inténtalo nuevamente';
        console.error(message);
        ModalUtil.showModal(message, true, ModalUtil.TYPE_MODAL_ERROR);
    }

    static showErrorGenerico(error: any) {
        const message = 'Parece que ocurrió un error al procesar la solicitud, inténtalo de nuevo más tarde';
        let requestId = null;
        if (error.headers) {
            requestId = error.headers.get(Config.headerRequestId);
        }

        if (error.status) {
            console.error(`[ID: ${requestId}]`, `[${error.status} - ${error.statusText}]`, `[${JSON.stringify(error.error)}]`, `[${message}]`);
            // ModalUtil.showModal(message, true, ModalUtil.TYPE_MODAL_ERROR);
            NotifylUtil.showNotification(NotifylUtil.FROM_TOP, NotifylUtil.ALIGN_RIGHT, NotifylUtil.TYPE_NOTIFY_ERROR, message, null, requestId);
        } else {
            const message2 = 'Error de comunicación con el servicio';
            console.error(`[ID: ${requestId}]`, `[${error.name} - ${error.message}]`, `[${message2}]`, `[${message}]`);
            // ModalUtil.showModal(message, true, ModalUtil.TYPE_MODAL_ERROR);
            NotifylUtil.showNotification(NotifylUtil.FROM_TOP, NotifylUtil.ALIGN_RIGHT, NotifylUtil.TYPE_NOTIFY_ERROR, message, null, requestId);
        }
    }

    static showErrorBadRequest(error: any, typeNotification: string): Promise<any> {
        const message = 'Petición invalida';
        const requestId = error.headers.get(Config.headerRequestId);

        console.error(`[ID: ${requestId}]`, `[${error.status} - ${error.statusText}]`, `[${JSON.stringify(error.error)}]`, `[${message}]`);

        if (typeNotification === this.TYPE_NOTIFICATION_ALERT) {
            NotifylUtil.showNotification(NotifylUtil.FROM_TOP, NotifylUtil.ALIGN_RIGHT, NotifylUtil.TYPE_NOTIFY_ERROR, message, null, requestId);
        } else {
            return ModalUtil.showModal(message, true, ModalUtil.TYPE_MODAL_ERROR, null, requestId);
        }
    }

    static showErrorUnauthorizedLogin(error: any, typeNotification: string): Promise<any> {
        const message = 'Credenciales invalidas';
        const requestId = error.headers.get(Config.headerRequestId);

        console.error(`[ID: ${requestId}]`, `[${error.status} - ${error.statusText}]`, `[${JSON.stringify(error.error)}]`, `[${message}]`);

        if (typeNotification === this.TYPE_NOTIFICATION_ALERT) {
            NotifylUtil.showNotification(NotifylUtil.FROM_TOP, NotifylUtil.ALIGN_RIGHT, NotifylUtil.TYPE_NOTIFY_ERROR, message, null, requestId);
        } else {
            return ModalUtil.showModal(message, true, ModalUtil.TYPE_MODAL_ERROR, null, requestId);
        }
    }

    static showErrorUnauthorizedRefreshToken(error: any, typeNotification: string): Promise<any> {
        const message = 'Su sesión ha expirado por inactividad, vuelva a iniciar sesión para continuar disfrutando de los servicios';
        const requestId = error.headers.get(Config.headerRequestId);

        console.error(`[ID: ${requestId}]`, `[${error.status} - ${error.statusText}]`, `[${JSON.stringify(error.error)}]`, `[${message}]`);

        if (typeNotification === this.TYPE_NOTIFICATION_ALERT) {
            NotifylUtil.showNotification(NotifylUtil.FROM_TOP, NotifylUtil.ALIGN_RIGHT, NotifylUtil.TYPE_NOTIFY_ERROR, message, null, requestId);
        } else {
            return ModalUtil.showModal(message, false, ModalUtil.TYPE_MODAL_ERROR, 'SESIÓN EXPIRADA');
        }
    }

    static showErrorUnauthorized(error: any, typeNotification: string): Promise<any> {
        const message = 'Token invalido';
        const requestId = error.headers.get(Config.headerRequestId);

        console.error(`[ID: ${requestId}]`, `[${error.status} - ${error.statusText}]`, `[${JSON.stringify(error.error)}]`, `[${message}]`);

        if (typeNotification === this.TYPE_NOTIFICATION_ALERT) {
            NotifylUtil.showNotification(NotifylUtil.FROM_TOP, NotifylUtil.ALIGN_RIGHT, NotifylUtil.TYPE_NOTIFY_ERROR, message, null, requestId);
        } else {
            return ModalUtil.showModal(message, true, ModalUtil.TYPE_MODAL_ERROR, null, requestId);
        }
    }

    static showErrorForbidden(error: any, typeNotification: string): Promise<any> {
        const message = 'Permisos insuficientes';
        const requestId = error.headers.get(Config.headerRequestId);

        console.error(`[ID: ${requestId}]`, `[${error.status} - ${error.statusText}]`, `[${JSON.stringify(error.error)}]`, `[${message}]`);

        if (typeNotification === this.TYPE_NOTIFICATION_ALERT) {
            NotifylUtil.showNotification(NotifylUtil.FROM_TOP, NotifylUtil.ALIGN_RIGHT, NotifylUtil.TYPE_NOTIFY_ERROR, message, null, requestId);
        } else {
            return ModalUtil.showModal(message, true, ModalUtil.TYPE_MODAL_ERROR, null, requestId);
        }
    }

    static showErrorNotFound(error: any, typeNotification: string): Promise<any> {
        const message = 'Registro no encontrado';
        const requestId = error.headers.get(Config.headerRequestId);

        console.error(`[ID: ${requestId}]`, `[${error.status} - ${error.statusText}]`, `[${JSON.stringify(error.error)}]`, `[${message}]`);

        if (typeNotification === this.TYPE_NOTIFICATION_ALERT) {
            NotifylUtil.showNotification(NotifylUtil.FROM_TOP, NotifylUtil.ALIGN_RIGHT, NotifylUtil.TYPE_NOTIFY_ERROR, message, null, requestId);
        } else {
            return ModalUtil.showModal(message, true, ModalUtil.TYPE_MODAL_ERROR, null, requestId);
        }
    }

    static showErrorConflict(error: any, typeNotification: string) {
        const message = 'Ya existe un registro con ese id';
        const requestId = error.headers.get(Config.headerRequestId);

        console.error(`[ID: ${requestId}]`, `[${error.status} - ${error.statusText}]`, `[${JSON.stringify(error.error)}]`, `[${message}]`);

        if (typeNotification === this.TYPE_NOTIFICATION_ALERT) {
            NotifylUtil.showNotification(NotifylUtil.FROM_TOP, NotifylUtil.ALIGN_RIGHT, NotifylUtil.TYPE_NOTIFY_ERROR, message, null, requestId);
        } else {
            return ModalUtil.showModal(message, true, ModalUtil.TYPE_MODAL_ERROR, null, requestId);
        }
    }

}

