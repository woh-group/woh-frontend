import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { OnInit } from '@angular/core';
import { EpaycoTransaction } from 'src/app/_entities/epayco-transaction';

@Injectable({
  providedIn: 'root'
})

export class EpaycoService implements OnInit{
	configUrl = 'https://secure.epayco.co/validation/v1/reference/';
  	constructor(
  	private http: HttpClient,
  	) { }

	 ngOnInit() {
	   
	 }
  	getTransactionResponse(refPayco: string) {
	  return this.http.get<EpaycoTransaction>(this.configUrl+refPayco);
	}
}