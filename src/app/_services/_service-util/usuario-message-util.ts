import { ModalUtil } from '../../_helpers/_util/modal-util';
import { NotifylUtil } from '../../_helpers/_util/notify-util';
import { Config } from '../../_configs/config';

export class UsuarioMessageUtil {

    static showMessage(estado: number) {

        switch (estado) {
            case Config.estadoUsuarioPendiente: {
                this.showMessageEstadoDesconocido();
                break;
            }
            case Config.estadoUsuarioEnValidacion: {
                this.showMessageEstadoDesconocido();
                break;
            }
            case Config.estadoUsuarioBloqueado: {
                this.showMessageEstadoBloqueado();
                break;
            }
            case Config.estadoUsuarioInactivo: {
                this.showMessageEstadoDesconocido();
                break;
            }
            case Config.estadoUsuarioEliminado: {
                this.showMessageEstadoDesconocido();
                break;
            }
            default: {
                this.showMessageEstadoDesconocido();
                break;
            }
        }
    }

    static showMessageEstadoDesconocido() {
        const message = 'Estado del usuario desconocido';
        console.error(message);
        ModalUtil.showModal(message, true, ModalUtil.TYPE_MODAL_WARNING);
    }

    static showMessageEstadoBloqueado() {
        const message = 'Usuario bloqueado';
        console.error(message);
        ModalUtil.showModal(message, true, ModalUtil.TYPE_MODAL_WARNING);
    }

}

