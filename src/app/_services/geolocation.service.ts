import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, timeout } from 'rxjs/operators';

import { Config } from '../_configs/config';


@Injectable({ providedIn: 'root' })
export class GeolocationService {

  private apiName = Config.apiGeolocalizacion;

  // Operaciones
  private metodoGetReverseGeocoding = 'getReverseGeocoding';

  constructor(private http: HttpClient) { }

  getReverseGeocoding(latitude: number, longitude: number): any {

    const endpoint = 'https://nominatim.openstreetmap.org/reverse?format=jsonv2&accept-language=es&lat=' + latitude + '&lon=' + longitude;

    return this.http.get(endpoint)
      .pipe(
        timeout(30000),
        catchError(this.handleError<any>(`${this.apiName} - ${this.metodoGetReverseGeocoding}`))
      );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // console.log('error:', JSON.stringify(error));
      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
