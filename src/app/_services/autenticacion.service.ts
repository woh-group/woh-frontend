import { Injectable } from '@angular/core';
import { HttpClient, } from '@angular/common/http';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { environment } from '../../environments/environment';
import { catchError, map, timeout } from 'rxjs/operators';
import { Router } from '@angular/router';

import { Config } from '../_configs/config';

import { HeadersUtil } from './_service-util/headers-util';
import { ServiceMessageUtil } from './_service-util/service-message-util';

import { Token } from '../_entities/token';
import { Persona } from '../_entities/persona';

@Injectable({ providedIn: 'root' })
export class AutenticacionService {

  private urlApi = '/api/usuarios/autenticacion';
  private endpoint = environment.endpointUsuarios + this.urlApi;
  private apiName = Config.apiUsuariosAutenticacion;

  // Usuario
  private currentUserSubject: BehaviorSubject<Token>;
  public currentUser: Observable<Token>;

  // Persona
  private currentPersonSubject: BehaviorSubject<Persona>;
  public currentPerson: Observable<Persona>;

  // Operaciones
  private metodoLogin = 'login';
  private metodoRefreshToken = 'refreshToken';

  constructor(private http: HttpClient, private router: Router) {
    this.currentUserSubject = new BehaviorSubject<Token>(this.getCurrentUser());
    this.currentUser = this.currentUserSubject.asObservable();

    this.currentPersonSubject = new BehaviorSubject<Persona>(JSON.parse(localStorage.getItem('persona')));
    this.currentPerson = this.currentPersonSubject.asObservable();
  }

  login(request: any): Observable<Response> {
    // Headers
    const headers = HeadersUtil.getHeadersUnknownUser(this.apiName);

    const url = this.endpoint + '/login';
    return this.http.post(url, request, { headers: headers, observe: 'response' })
      .pipe(
        timeout(30000),
        map((response: any) => {
          if (response && response.status === 201) {
            localStorage.setItem('token', JSON.stringify(response.body));
            this.currentUserSubject.next(response.body);
          }

          return response;
        }),
        // tap(_ => console.log('')),
        catchError(this.handleError<any>(`${this.apiName} - ${this.metodoLogin}`))
      );
  }

  logout() {
    // localStorage.clear();
    localStorage.removeItem('token');
    localStorage.removeItem('persona');
    this.currentUserSubject.next(null);
  }

  getCurrentUser(): Token {
    try {
      const user = localStorage.getItem('token');
      return JSON.parse(user);
    } catch (error) {
      return null;
    }
  }

  getCurrentPersona(): Persona {
    const persona = localStorage.getItem('persona');
    return JSON.parse(persona);
  }

  refreshToken() {
    // Get currentUser
    const user = this.getCurrentUser();
    if (user) {
      // Headers
      const headers = HeadersUtil.getHeadersLoggedInUser(this.apiName, user);

      const url = this.endpoint + '/refreshToken' + '/' + user.refreshToken;
      this.http.post<Token>(url, null, { headers: headers })
        .pipe(
          timeout(30000),
          map(response => {
            if (response && response.accessToken && localStorage.getItem('token')) {
              localStorage.setItem('token', JSON.stringify(response));
              // this.currentUserSubject.next(response);
            }
            return response;
          }),
          // tap(_ => console.log('')),
          catchError(this.handleError<Token>(`${this.apiName} - ${this.metodoRefreshToken}`))
        ).subscribe(() => { });
    }
  }

  savePersonalocalStorage(obj: Persona) {
    const personaNew = new Persona();
    personaNew.nombres = obj.nombres;
    personaNew.apellidos = obj.apellidos;
    personaNew.sexo = obj.sexo;
    personaNew.fechaNacimiento = obj.fechaNacimiento;
    localStorage.setItem('persona', JSON.stringify(personaNew));
    this.currentPersonSubject.next(personaNew);
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      switch (error.status) {
        case 0: {
          this.logout();
          break;
        }
        case 401: {
          if (operation === `${this.apiName} - ${this.metodoRefreshToken}`) {
            ServiceMessageUtil.showErrorUnauthorizedRefreshToken(error, ServiceMessageUtil.TYPE_NOTIFICATION_MODAL).then((modalResp) => {
              if (modalResp.value) {
                this.logout();
                this.router.navigate(['/home']);
              }
            });
          } else {
            ServiceMessageUtil.showErrorUnauthorizedLogin(error, ServiceMessageUtil.TYPE_NOTIFICATION_MODAL);
          }
          break;
        }
        default: {
          ServiceMessageUtil.isErrorGenerico(error, ServiceMessageUtil.TYPE_NOTIFICATION_ALERT);
          break;
        }
      }

      // console.log('error:', JSON.stringify(error));
      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
