import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from '../../../environments/environment';
import { catchError, tap, timeout } from 'rxjs/operators';
import { Router } from '@angular/router';

import { Config } from '../../_configs/config';

import { ServiceMessageUtil } from '../_service-util/service-message-util';
import { HeadersUtil } from '../_service-util/headers-util';
import { ParamsUtil } from '../_service-util/params-util';

import { AutenticacionService } from '../autenticacion.service';

import { Departamento } from '../../_entities/departamento';


@Injectable({ providedIn: 'root' })
export class DepartamentoService {

  private urlApi = '/api/comunes/departamentos';
  private endpoint = environment.endpointComunes + this.urlApi;
  private apiName = Config.apiComunesDepartamentos;

  // Operaciones
  private metodoGetAll = 'getAll';
  private metodoGetById = 'getById';
  private metodoCount = 'count';
  private metodoAdd = 'add';
  private metodoUpdate = 'update';
  private metodoDelete = 'delete';
  private metodoGetCiudadesByDepartamentoId = 'getCiudadesByDepartamentoId';
  private metodoCountCiudadesByDepartamentoId = 'countCiudadesByDepartamentoId';

  constructor(private http: HttpClient, private router: Router, private autenticacionService: AutenticacionService) { }

  getAll(paramsMap: Map<string, string>): Observable<Response> {
    // Get currentUser
    const user = this.autenticacionService.getCurrentUser();

    // Headers
    let headers = null;
    if (user) {
      headers = HeadersUtil.getHeadersLoggedInUser(this.apiName, user);
    } else {
      headers = HeadersUtil.getHeadersUnknownUser(this.apiName);
    }

    return this.http.get(this.endpoint, { headers: headers, params: ParamsUtil.mapToHttpParams(paramsMap), observe: 'response' })
      .pipe(
        timeout(30000),
        tap(_ => {
          this.autenticacionService.refreshToken();
        }),
        catchError(this.handleError<any>(`${this.apiName} - ${this.metodoGetAll}`))
      );
  }

  getById(id: string): Observable<Response> {
    // Get currentUser
    const user = this.autenticacionService.getCurrentUser();

    // Headers
    const headers = HeadersUtil.getHeadersLoggedInUser(this.apiName, user);

    const url = this.endpoint + '/' + id;
    return this.http.get(url, { headers: headers, observe: 'response' })
      .pipe(
        timeout(30000),
        tap(_ => {
          this.autenticacionService.refreshToken();
        }),
        catchError(this.handleError<any>(`${this.apiName} - ${this.metodoGetById}`))
      );
  }

  count(paramsMap: Map<string, string>): Observable<number> {
    // Get currentUser
    const user = this.autenticacionService.getCurrentUser();

    // Headers
    const headers = HeadersUtil.getHeadersLoggedInUser(this.apiName, user);

    const url = this.endpoint + '/count';
    return this.http.get<number>(url, { headers: headers, params: ParamsUtil.mapToHttpParams(paramsMap) })
      .pipe(
        timeout(30000),
        tap(_ => {
          this.autenticacionService.refreshToken();
        }),
        catchError(this.handleError<number>(`${this.apiName} - ${this.metodoCount}`))
      );
  }

  add(obj: Departamento): Observable<Response> {
    // Headers
    const headers = HeadersUtil.getHeadersUnknownUser(this.apiName);

    return this.http.post(this.endpoint, obj, { headers: headers, observe: 'response' })
      .pipe(
        timeout(30000),
        tap(_ => {
          this.autenticacionService.refreshToken();
        }),
        catchError(this.handleError<any>(`${this.apiName} - ${this.metodoAdd}`))
      );
  }

  update(id: string, obj: Departamento): Observable<Response> {
    // Get currentUser
    const user = this.autenticacionService.getCurrentUser();

    // Headers
    const headers = HeadersUtil.getHeadersLoggedInUser(this.apiName, user);

    const url = this.endpoint + '/' + id;
    return this.http.put(url, obj, { headers: headers, observe: 'response' })
      .pipe(
        timeout(30000),
        tap(_ => {
          this.autenticacionService.refreshToken();
        }),
        catchError(this.handleError<any>(`${this.apiName} - ${this.metodoUpdate}`))
      );
  }

  delete(id: string): Observable<Response> {
    // Get currentUser
    const user = this.autenticacionService.getCurrentUser();

    // Headers
    const headers = HeadersUtil.getHeadersLoggedInUser(this.apiName, user);

    const url = this.endpoint + '/' + id;
    return this.http.delete(url, { headers: headers, observe: 'response' })
      .pipe(
        timeout(30000),
        tap(_ => {
          this.autenticacionService.refreshToken();
        }),
        catchError(this.handleError<any>(`${this.apiName} - ${this.metodoDelete}`))
      );
  }

  GetCiudadesByDepartamentoId(departamentoId: any, paramsMap: Map<string, string>): Observable<Response> {
    // Get currentUser
    const user = this.autenticacionService.getCurrentUser();

    // Headers
    let headers = null;
    if (user) {
      headers = HeadersUtil.getHeadersLoggedInUser(this.apiName, user);
    } else {
      headers = HeadersUtil.getHeadersUnknownUser(this.apiName);
    }

    const url = this.endpoint + '/' + departamentoId + '/ciudades';
    return this.http.get(url, { headers: headers, params: ParamsUtil.mapToHttpParams(paramsMap), observe: 'response' })
      .pipe(
        timeout(30000),
        tap(_ => {
          this.autenticacionService.refreshToken();
        }),
        catchError(this.handleError<any>(`${this.apiName} - ${this.metodoGetCiudadesByDepartamentoId}`))
      );
  }

  countCiudadesByDepartamentoId(departamentoId: any, paramsMap: Map<string, string>): Observable<number> {
    // Get currentUser
    const user = this.autenticacionService.getCurrentUser();

    // Headers
    const headers = HeadersUtil.getHeadersLoggedInUser(this.apiName, user);

    const url = this.endpoint + '/' + departamentoId + '/ciudades/count';
    return this.http.get<number>(url, { headers: headers, params: ParamsUtil.mapToHttpParams(paramsMap) })
      .pipe(
        timeout(30000),
        tap(_ => {
          this.autenticacionService.refreshToken();
        }),
        catchError(this.handleError<number>(`${this.apiName} - ${this.metodoCountCiudadesByDepartamentoId}`))
      );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      switch (error.status) {
        case 401: {
          ServiceMessageUtil.showErrorUnauthorizedRefreshToken(error, ServiceMessageUtil.TYPE_NOTIFICATION_MODAL).then((modalResp) => {
            if (modalResp.value) {
              this.autenticacionService.logout();
              this.router.navigate(['/home']);
            }
          });
          break;
        }
        default: {
          ServiceMessageUtil.isErrorGenerico(error, ServiceMessageUtil.TYPE_NOTIFICATION_ALERT);
          break;
        }
      }

      // console.log('error:', JSON.stringify(error));
      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
