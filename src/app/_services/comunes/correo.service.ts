import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from '../../../environments/environment';
import { catchError, tap, timeout } from 'rxjs/operators';
import { Router } from '@angular/router';

import { Config } from '../../_configs/config';

import { ServiceMessageUtil } from '../_service-util/service-message-util';
import { HeadersUtil } from '../_service-util/headers-util';

import { AutenticacionService } from '../autenticacion.service';

import { Correo } from '../../_entities/correo';


@Injectable({ providedIn: 'root' })
export class CorreoService {

  private urlApi = '/api/comunes/correos';
  private endpoint = environment.endpointComunes + this.urlApi;
  private apiName = Config.apiComunesCorreos;

  // Operaciones
  private metodoSend = 'send';

  constructor(private http: HttpClient, private router: Router, private autenticacionService: AutenticacionService) { }

  send(obj: Correo): Observable<Response> {
    // Headers
    const headers = HeadersUtil.getHeadersUnknownUser(this.apiName);

    const url = this.endpoint + '/send';
    return this.http.post(url, obj, { headers: headers, observe: 'response' })
      .pipe(
        timeout(30000),
        tap(_ => {
          this.autenticacionService.refreshToken();
        }),
        catchError(this.handleError<any>(`${this.apiName} - ${this.metodoSend}`))
      );
  }
  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      switch (error.status) {
        case 401: {
          ServiceMessageUtil.showErrorUnauthorizedRefreshToken(error, ServiceMessageUtil.TYPE_NOTIFICATION_MODAL).then((modalResp) => {
            if (modalResp.value) {
              this.autenticacionService.logout();
              this.router.navigate(['/home']);
            }
          });
          break;
        }
        default: {
          ServiceMessageUtil.isErrorGenerico(error, ServiceMessageUtil.TYPE_NOTIFICATION_ALERT);
          break;
        }
      }

      // console.log('error:', JSON.stringify(error));
      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
