import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';


@Component({
    selector: 'app-terminos-dialog',
    templateUrl: 'terminos.dialog.html',
    styleUrls: ['./terminos.css']
  })
  // tslint:disable-next-line:component-class-suffix
  export class TerminosDialog {

    constructor(
      public dialogRef: MatDialogRef<TerminosDialog>,
      @Inject(MAT_DIALOG_DATA) public data: any) {
      }

    onNoClick(): void {
      this.dialogRef.close();
    }

}
