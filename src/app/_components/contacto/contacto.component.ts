import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { Usuario } from '../../_entities/usuario';
import { Config } from '../../_configs/config';
import { AutenticacionService } from '../../_services/autenticacion.service';
import { Persona } from '../../_entities/persona';
import { JwtUtil } from '../../_helpers/_util/jwt-util';
import { ValidatorComponentUtil } from '../../_helpers/_util/validator-component-util';
import { CorreoService } from '../../_services/comunes/correo.service';
import { Correo } from '../../_entities/correo';
import { NotifylUtil } from '../../_helpers/_util/notify-util';
import { UsuarioService } from '../../_services/usuario.service';
import { LoaderService } from '../../_services/_service-util/loader.service';

@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.css']
})
export class ContactoComponent implements OnInit {

  loggedIn = false;
  form: FormGroup;
  persona: Persona;
  private usuarioId = '';
  usuario: Usuario = new Usuario();

  constructor(
    private formBuilder: FormBuilder,
    private correoService: CorreoService,
    private autenticacionService: AutenticacionService,
    private loaderService: LoaderService,
    private usuarioService: UsuarioService) {
    // Validamos si esta logueado
    this.autenticacionService.currentUser.subscribe(response => {
      if (response && response.accessToken) {
        this.loggedIn = true;

        // Obtenemos los datos del token
        const tokenInfo = JwtUtil.decodeToken(response.accessToken);
        this.usuarioId = tokenInfo.sub;
        // Obtenemos los datos de la persona
        this.autenticacionService.currentPerson.subscribe(responsePersona => {
          if (responsePersona) {
            this.persona = responsePersona;
          }
        });
      } else {
        this.loggedIn = false;
      }
    }, error => {
      this.loggedIn = false;
    });
  }

  ngOnInit() {
    if (this.usuarioId) {
      this.getUsuario();
    } else {
      this.initializeForm();
    }
  }

  initializeForm() {
    this.form = this.formBuilder.group({
      nombreCompleto: [this.persona ? this.persona.nombres + ' ' + this.persona.apellidos : null, Validators.required],
      email: [this.usuario.username ? this.usuario.username : null, Validators.compose([Validators.required, Validators.pattern(Config.emailValido)])],
      asunto: [null, Validators.required],
      descripcion: [null, Validators.compose([Validators.required, Validators.minLength(10)])]
    });
  }

  getUsuario() {
    this.usuarioService.getById(this.usuarioId).subscribe((response: any) => {
      if (response && response.status === 200) {
        this.usuario = response.body;
        this.initializeForm();
      }
    });
  }

  probarLoader() {
    this.loaderService.display(true);
  }

  enviarMensaje() {
    const correo = new Correo;
    correo.remitente = this.form.get('email').value;
    correo.destinatario = Config.correoContacto;
    correo.remitenteNombre = this.form.get('nombreCompleto').value;
    correo.asunto = this.form.get('asunto').value;
    correo.nombrePlantilla = Config.plantillaContacto;

    this.correoService.send(correo).subscribe((response: any) => {
      if (response && response.status === 201) {
        NotifylUtil.showNotification(NotifylUtil.FROM_TOP, NotifylUtil.ALIGN_RIGHT, NotifylUtil.TYPE_NOTIFY_SUCCESS, 'Su correo ha sido enviado');
      }
    });
  }

  isFieldInvalid(fieldControl: AbstractControl): boolean {
    return ValidatorComponentUtil.isFieldInvalid(fieldControl);
  }

  getFieldError(fieldControl: AbstractControl, fieldType?: string): string {
    return ValidatorComponentUtil.getFieldError(fieldControl, fieldType);
  }

  isRequired(fieldControl: AbstractControl): boolean {
    return ValidatorComponentUtil.isRequired(fieldControl);
  }

}
