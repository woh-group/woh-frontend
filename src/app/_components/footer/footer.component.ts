import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { TerminosDialog } from '../_dialogs/terminosYcondiciones/terminos.dialog';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Config } from 'src/app/_configs/config';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  window: any = window;
  formEpayco: FormGroup;

  @Input() loggedIn: boolean;

  constructor(public dialog: MatDialog, private formBuilder: FormBuilder) { }

  ngOnInit() {
  }

  openDialogTerminosYCondiciones() {
    const dialogRef = this.dialog.open(TerminosDialog, {
      width: '1080px',
      height: '900px'
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  epayco(){
    var handler = this.window.ePayco.checkout.configure({ key: 'c2b022ad3a811cd013e8202f14c244e1', test: false });
    var data = { 
     //Parametros compra (obligatorio) 
     currency: "cop",
     name: Config.nombreServicio,
     description: Config.nombreServicio,
     tax_base: Config.valorConsulta,
     tax: '0',
     amount: Config.valorConsulta,
     country: "co",
     lang: "es",
     external: "false",
     //Atributos opcionales
     method:'GET',
     response: "https://woh.com.co/pay/result",
     confirmation: "https://woh.com.co/pay/confirmation",
   } 
   handler.open(data)
 }

}
