import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, AbstractControl, NgControl } from '@angular/forms';

import { Config } from 'src/app/_configs/config';
import { NotifylUtil } from 'src/app/_helpers/_util/notify-util';

import { CorreoService } from 'src/app/_services/comunes/correo.service';
import { ProfesionalService } from 'src/app/_services/profesionales/profesional.service';

import { Correo } from 'src/app/_entities/correo';
import { Profesional } from 'src/app/_entities/profesional';

@Component({
  selector: 'app-agendar-cita',
  templateUrl: './agendar-cita.component.html',
  styleUrls: ['./agendar-cita.component.css']
})
export class AgendarCitaComponent implements OnInit {

  form: FormGroup;
  profesionales: Profesional[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private profesionalesService: ProfesionalService,
    private correoService: CorreoService,
  ) {
    this.initalizeForm();
  }

  ngOnInit() {
    this.getProfesionales();
  }

  initalizeForm() {
    this.form = this.formBuilder.group({
      nombre: [null, Validators.required],
      email: [null, Validators.compose([Validators.required, Validators.pattern(Config.emailValido)])],
      celular: [null, Validators.required],
      profesional: [null, Validators.required],
    });
  }

  getProfesionales() {
    // Params
    const params = new Map<string, any>();
    params.set('showPersona', true);
    params.set('estado', true);
    params.set('sort', 'persona.nombres');

    this.profesionalesService.getAll(params).subscribe((response: any) => {
      if (response && response.status === 200) {
        this.profesionales = response.body;
        this.profesionalesService.profesionales = this.profesionales;
      }
    });
  }

  agendar() {
    if (this.form.valid) {
      console.log(this.form.value);
      let message = `Hola, deseo agendar una cita con ${this.form.get('profesional').value.toUpperCase()}.\n\n`;
      message += 'Mis datos son\n';
      message += `Nombre: ${this.form.get('nombre').value}\n`;
      message += `Celular: ${this.form.get('celular').value}\n`;
      message += `Email: ${this.form.get('email').value}\n`;

      const correo = new Correo;
      correo.remitente = this.form.get('email').value;
      correo.destinatario = Config.correoContactoGmail;
      correo.remitenteNombre = this.form.get('nombre').value;
      correo.asunto = 'WOH - [SOLICITUD AGENDAMIENTO CITA]';
      correo.mensaje = message;

      this.correoService.send(correo).subscribe((response: any) => {
        if (response && response.status === 201) {
          NotifylUtil.showNotification(NotifylUtil.FROM_TOP, NotifylUtil.ALIGN_RIGHT, NotifylUtil.TYPE_NOTIFY_SUCCESS, 'Su solicitud ha sido enviada, en breve nos pondremos en contacto con usted');
        }
      });
    }
  }

}
