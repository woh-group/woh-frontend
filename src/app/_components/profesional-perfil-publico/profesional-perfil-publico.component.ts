import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Persona } from '../../_entities/persona';
import { ActivatedRoute } from '@angular/router';

import { environment } from '../../../environments/environment';
import { Config } from '../../_configs/config';

import { NivelAcademicoService } from 'src/app/_services/comunes/nivelAcademico.service';
import { ProfesionalService } from 'src/app/_services/profesionales/profesional.service';

import { NivelAcademico } from 'src/app/_entities/nivelAcademico';
import { Profesional } from 'src/app/_entities/profesional';
import { finalize } from 'rxjs/operators';
import { LoaderService } from 'src/app/_services/_service-util/loader.service';

@Component({
  selector: 'app-profesional-perfil-publico',
  templateUrl: './profesional-perfil-publico.component.html',
  styleUrls: ['./profesional-perfil-publico.component.css']
})
export class ProfesionalPerfilPublicoComponent implements OnInit {

  form: FormGroup;
  persona: Persona = new Persona();
  profesional: Profesional = new Profesional();
  nivelesAcademicos: NivelAcademico[] = [];

  routeFiles = environment.endpointBucketFiles;
  linkAgendarCitaProfesional;

  constructor(private route: ActivatedRoute,
    private nivelAcademicoService: NivelAcademicoService,
    private profesionalesService: ProfesionalService,
    private formBuilder: FormBuilder,
    private router: Router,
    private loaderService: LoaderService) { }

  ngOnInit() {
    const id = this.route.snapshot.params['id'];
    // console.log('id', id);

    if (id) {
      this.getProfesionalById(id);
      this.getNivelesAcademicos();
    }
    // this.initalizeForm();
  }

  initalizeForm() {
    this.form = this.formBuilder.group({
      nombres: [this.persona.nombres],
      apellidos: [this.persona.apellidos],
      sexo: [this.persona.sexo],
      fechaNacimiento: [this.persona.fechaNacimiento],
      pais: [null],
      departamento: [null],
      ciudad: [this.persona.ciudadId]
    });
  }

  getProfesionalById(usuarioId: string) {
    // Params
    const params = new Map<string, any>();
    params.set('showPersona', true);
    params.set('showEstudios', true);
    params.set('showExperiencias', true);

    this.loaderService.display(true);
    this.profesionalesService.getByUsuarioId(usuarioId, params).pipe(finalize(() => {
      this.loaderService.display(false);
    })).subscribe((response: any) => {
      // console.log('response', response);
      if (response && response.status === 200) {
        this.profesional = response.body;
        this.linkAgendarCitaProfesional = Config.agendarCitaProfesional.replace('###NOMBRE###', this.profesional.persona.nombres + ' ' + this.profesional.persona.apellidos);
      } else {
        this.router.navigate(['/home']);
      }
    });
  }

  getNivelesAcademicos() {
    // Params
    const params = new Map<string, any>();

    this.nivelAcademicoService.getAll(params).subscribe((response: any) => {
      // console.log('response nivelesAcademicos', response);
      if (response && response.status === 200) {
        this.nivelesAcademicos = response.body;
      }
    });
  }

  showNivelAcademico(id: number): string {
    if (this.nivelesAcademicos) {
      const nivel = this.nivelesAcademicos.filter(item => item.id === id);
      if (nivel.length > 0) {
        return nivel[0].descripcion.toLowerCase();
      }
    }
    return '';
  }

}
