import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfesionalPerfilPublicoComponent } from './profesional-perfil-publico.component';

describe('ProfesionalPerfilPublicoComponent', () => {
  let component: ProfesionalPerfilPublicoComponent;
  let fixture: ComponentFixture<ProfesionalPerfilPublicoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfesionalPerfilPublicoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfesionalPerfilPublicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
