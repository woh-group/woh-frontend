import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NotifylUtil } from '../../../_helpers/_util/notify-util';

import { UsuarioService } from '../../../_services/usuario.service';
import { LoaderService } from 'src/app/_services/_service-util/loader.service';
import { finalize } from 'rxjs/operators';


@Component({
  selector: 'app-activar-usuario',
  templateUrl: './activar-usuario.component.html',
  styleUrls: ['./activar-usuario.component.css']
})
export class ActivarUsuarioComponent implements OnInit {

  @Input() correo: string;
  @Output() eventTotalRegistros = new EventEmitter();

  urlValida = false;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private usuarioService: UsuarioService,
    private loaderService: LoaderService) { }

  ngOnInit() {
    // Obtenemos el parametro por url
    const usuarioId = this.route.snapshot.params['id'];
    const activationCode = this.route.snapshot.params['activationCode'];

    if (usuarioId && activationCode) {
      this.activarUsuario(usuarioId, activationCode);
    }

  }

  activarUsuario(usuarioId: string, activationCode: string) {
    this.loaderService.display(true);
    this.usuarioService.activarUsuario(usuarioId, activationCode).pipe(finalize(() => {
      this.loaderService.display(false);
    })).subscribe((response: any) => {
      if (response && response.status === 200) {
        NotifylUtil.showNotification(NotifylUtil.FROM_TOP, NotifylUtil.ALIGN_RIGHT, NotifylUtil.TYPE_NOTIFY_SUCCESS, 'Usuario activado con éxito');
        this.urlValida = true;
      }
    });
  }

}
