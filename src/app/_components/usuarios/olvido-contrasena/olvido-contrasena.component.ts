import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ValidatorComponentUtil } from 'src/app/_helpers/_util/validator-component-util';
import { Config } from 'src/app/_configs/config';
import { UsuarioService } from 'src/app/_services/usuario.service';
import { ModalUtil } from 'src/app/_helpers/_util/modal-util';
import { finalize } from 'rxjs/operators';
import { LoaderService } from 'src/app/_services/_service-util/loader.service';
import { NotifylUtil } from 'src/app/_helpers/_util/notify-util';

@Component({
  selector: 'app-olvido-contrasena',
  templateUrl: './olvido-contrasena.component.html',
  styleUrls: ['./olvido-contrasena.component.css']
})
export class OlvidoContrasenaComponent implements OnInit {

  form: FormGroup;

  constructor(private formBuilder: FormBuilder,
    private usuarioService: UsuarioService,
    private loaderService: LoaderService) { }

  ngOnInit() {
    this.initializeForm();
  }

  initializeForm() {
    this.form = this.formBuilder.group({
      email: [null , Validators.compose([Validators.required, Validators.pattern(Config.emailValido)])]
    });
  }

  validateUsername() {
    // Params
    const params = new Map<string, any>();
    params.set('username', this.form.get('email').value);

    this.usuarioService.count(params).subscribe((response: any) => {
      // console.log('response', response);
      if (response === 0) {
        // EL CORREO NO EXISTE
        ModalUtil.showModal('Este correo no está registrado en nuestra plataforma', false, ModalUtil.TYPE_MODAL_SUCCESS);
      } else {
        // EL CORREO SI EXISTE
        this.enviarInstrucciones();
      }
    });
  }

  enviarInstrucciones() {
    const username = this.form.get('email').value;
    this.loaderService.display(true);
    this.usuarioService.getOlvidoContrasena(username).pipe(finalize(() => {
      this.loaderService.display(false);
    })).subscribe((response: any) => {
      if (response && response.status === 200) {
        ModalUtil.showModal('El correo se ha enviado con exito', false, ModalUtil.TYPE_MODAL_SUCCESS, 'LISTO');
      } else {
        ModalUtil.showModal('Ha ocurrido un problema. Intente más tarde', false, ModalUtil.TYPE_MODAL_ERROR, 'ERROR');
      }
    });
  }

  isFieldInvalid(fieldControl: AbstractControl): boolean {
    return ValidatorComponentUtil.isFieldInvalid(fieldControl);
  }

  getFieldError(fieldControl: AbstractControl, fieldType?: string): string {
    return ValidatorComponentUtil.getFieldError(fieldControl, fieldType);
  }

  isRequired(fieldControl: AbstractControl): boolean {
    return ValidatorComponentUtil.isRequired(fieldControl);
  }

}
