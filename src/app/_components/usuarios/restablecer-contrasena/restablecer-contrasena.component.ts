import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { ValidatorComponentUtil } from 'src/app/_helpers/_util/validator-component-util';
import { MustMatch } from 'src/app/_helpers/must-match.validator';
import { UsuarioService } from 'src/app/_services/usuario.service';
import { CambioContrasena } from 'src/app/_entities/cambioContrasena';
import { Usuario } from 'src/app/_entities/usuario';
import { LoaderService } from 'src/app/_services/_service-util/loader.service';
import { finalize } from 'rxjs/operators';
import { NotifylUtil } from 'src/app/_helpers/_util/notify-util';
import { ActivatedRoute, Router } from '@angular/router';
import { AutenticacionService } from 'src/app/_services/autenticacion.service';

@Component({
  selector: 'app-restablecer-contrasena',
  templateUrl: './restablecer-contrasena.component.html',
  styleUrls: ['./restablecer-contrasena.component.css']
})
export class RestablecerContrasenaComponent implements OnInit {

  form: FormGroup;
  usuario: Usuario = new Usuario();
  usuarioID;
  loggedIn = false;

  constructor(private formBuilder: FormBuilder,
    private usuarioService: UsuarioService,
    private loaderService: LoaderService,
    private route: ActivatedRoute,
    private router: Router,
    private autenticacionService: AutenticacionService) { }

  ngOnInit() {
    this.logout();
    this.usuarioID = this.route.snapshot.params['id'];
    this.getEstadoC();
    this.initializeForm();
  }

  initializeForm() {
    this.form = this.formBuilder.group({
      passwordTemporal: [null , Validators.required],
      password: [null , Validators.required],
      confirmPassword: [null , Validators.required]
    }, {
      validator: MustMatch('password', 'confirmPassword')
    });
  }

  restablecerContrasena() {
        const cambioContrasena: CambioContrasena = new CambioContrasena();
        cambioContrasena.username = this.usuario.username;
        cambioContrasena.oldPassword = this.form.get('passwordTemporal').value;
        cambioContrasena.newPassword = this.form.get('password').value;
        this.loaderService.display(true);
        this.usuarioService.cambiarContrasena(cambioContrasena).pipe(finalize(() => {
          this.loaderService.display(false);
        })).subscribe((response: any) => {
          if (response && response.status === 200) {
            NotifylUtil.showNotification(NotifylUtil.FROM_TOP, NotifylUtil.ALIGN_RIGHT, NotifylUtil.TYPE_NOTIFY_SUCCESS, 'Se ha restablecido tu contraseña con éxito');
            this.router.navigate(['/home']);
          } else if (response && response.status === 204) {
            NotifylUtil.showNotification(NotifylUtil.FROM_TOP, NotifylUtil.ALIGN_RIGHT, NotifylUtil.TYPE_NOTIFY_ERROR, 'Clave temporal incorrecta');
          } else {
            NotifylUtil.showNotification(NotifylUtil.FROM_TOP, NotifylUtil.ALIGN_RIGHT, NotifylUtil.TYPE_NOTIFY_ERROR, 'Ha ocurrido un problema. Intente más tarde');
          }
        });
  }

  logout() {
    this.autenticacionService.logout();
    this.loggedIn = false;
  }

  iniciarRestablecer() {
    this.usuarioService.getById(this.usuarioID).subscribe((response: any) => {
      if (response && response.status === 200) {
        this.usuario = response.body;
        this.restablecerContrasena();
      }
    });
  }

  getEstadoC() {
    this.loaderService.display(true);
    this.usuarioService.getEstadoC(this.usuarioID).pipe(finalize(() => {
      this.loaderService.display(false);
    })).subscribe((response: any) => {
      if (response && response.status === 200) {
        if (response.body === false) {
          this.router.navigate(['/home']);
        }
      } else {
        this.router.navigate(['/home']);
      }
    });
  }

  isFieldInvalid(fieldControl: AbstractControl): boolean {
    return ValidatorComponentUtil.isFieldInvalid(fieldControl);
  }

  getFieldError(fieldControl: AbstractControl, fieldType?: string): string {
    return ValidatorComponentUtil.getFieldError(fieldControl, fieldType);
  }

  isRequired(fieldControl: AbstractControl): boolean {
    return ValidatorComponentUtil.isRequired(fieldControl);
  }

}
