import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Profesional } from '../../../../_entities/profesional';
import { ProfesionalService } from '../../../../_services/profesionales/profesional.service';
import { finalize } from 'rxjs/operators';
import { LoaderService } from 'src/app/_services/_service-util/loader.service';

@Component({
  selector: 'app-idiomas',
  templateUrl: './idiomas.component.html',
  styleUrls: ['./idiomas.component.css']
})
export class IdiomasComponent implements OnInit {

  form: FormGroup;
  @Input() profesional: Profesional;

  idiomas: string[] = ['Español', 'Ingles', 'Frances'];
  idiomasNoRegistrados: string[] = [];


  constructor(
    private formBuilder: FormBuilder,
    private profesionalService: ProfesionalService,
    private loaderService: LoaderService
  ) { }

  ngOnInit() {
    this.initalizeForm();
    this.getIdiomasNoRegistrados();
  }

  initalizeForm() {
    this.form = this.formBuilder.group({
      idiomas: []
    });
  }

  getIdiomasNoRegistrados() {
    if (this.idiomas) {
      this.idiomasNoRegistrados = [];

      if (this.profesional.idiomas) {
        this.idiomas.forEach((idioma: string) => {
          if (this.profesional.idiomas.indexOf(idioma) === -1) {
            this.idiomasNoRegistrados.push(idioma);
          }
        });
      } else {
        this.idiomasNoRegistrados = this.idiomasNoRegistrados.concat(this.idiomas);
      }
    }
  }

  addIdioma(event: any) {
    if (event.isUserInput) {
      let idiomas: string[] = [];
      if (this.profesional.idiomas) {
        idiomas = idiomas.concat(this.profesional.idiomas);
      }
      idiomas.push(event.source.value);
      this.updateIdioma(idiomas);
    }
  }

  deleteIdioma(idioma: string) {
    const idiomas = this.profesional.idiomas.filter(obj => obj !== idioma);
    this.updateIdioma(idiomas);
  }

  updateIdioma(idiomas: string[]) {
    this.profesional.idiomas = idiomas;
    this.loaderService.display(true);
    this.profesionalService.updateIdiomas(this.profesional._id, this.profesional).pipe(finalize(() => {
      this.loaderService.display(false);
    })).subscribe((response: any) => {
      if (response && response.status === 200) {
        //  NotifylUtil.showNotification(NotifylUtil.FROM_TOP, NotifylUtil.ALIGN_RIGHT, NotifylUtil.TYPE_NOTIFY_SUCCESS, 'Idioma actualizado con éxito');
        this.profesional.idiomas = idiomas;
        this.getIdiomasNoRegistrados();
      }
    });
  }

}
