import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators, AbstractControl } from '@angular/forms';

import swal from 'sweetalert2';
import { ModalUtil } from 'src/app/_helpers/_util/modal-util';
import { NotifylUtil } from 'src/app/_helpers/_util/notify-util';
import { ValidatorComponentUtil } from 'src/app/_helpers/_util/validator-component-util';

import { NivelAcademicoService } from 'src/app/_services/comunes/nivelAcademico.service';
import { ProfesionalPerfilEstudioService } from 'src/app/_services/profesionales/perfil-estudio.service';

import { NivelAcademico } from 'src/app/_entities/nivelAcademico';
import { Estudio } from 'src/app/_entities/estudio';
import { LoaderService } from 'src/app/_services/_service-util/loader.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-estudios',
  templateUrl: './estudios.component.html',
  styleUrls: ['./estudios.component.css']
})
export class EstudiosComponent implements OnInit {

  form: FormGroup;
  formChange = false;
  @Input() tipoEstudioId: number;
  @Input() perfilId: string;
  @Output() eventTotalRegistros = new EventEmitter();

  estudioActivo = false;
  nivelesAcademicos: NivelAcademico[] = [];
  estudio: Estudio;
  pregrado = false;

  // Form Arrays
  formArrayEstudios = 'estudios';

  // Accordion
  currentAccordion = -1;
  accordionState = false;

  constructor(
    private formBuilder: FormBuilder,
    private nivelAcademicoService: NivelAcademicoService,
    private profesionPerfilEstudioService: ProfesionalPerfilEstudioService,
    private loaderService: LoaderService
  ) { }

  ngOnInit() {
    this.initalizeForm();

    this.getNivelesAcademicos(this.tipoEstudioId);
    this.getEstudios(this.tipoEstudioId);
  }

  initalizeForm() {
    this.form = this.formBuilder.group({
      estudios: new FormArray([])
    });

    // Nos subscribimos para saber cuando el formulario cambie
    let cantidadRegistros = 0;
    this.form.valueChanges.subscribe(() => {
      this.formChange = true;
      if (this.form.get('estudios').value && cantidadRegistros !== this.form.get('estudios').value.length) {
        cantidadRegistros = this.form.get('estudios').value.length;
        this.eventTotalRegistros.emit({
          perfilId: this.perfilId,
          cantidadRegistros: cantidadRegistros
        });
      }
    });
  }

  isFieldInvalid(fieldControl: AbstractControl): boolean {
    return ValidatorComponentUtil.isFieldInvalid(fieldControl);
  }

  getFieldError(fieldControl: AbstractControl, fieldType?: string): string {
    return ValidatorComponentUtil.getFieldError(fieldControl, fieldType);
  }

  isRequired(fieldControl: AbstractControl): boolean {
    return ValidatorComponentUtil.isRequired(fieldControl);
  }

  getNivelesAcademicos(tipo: number) {
    // Params
    const params = new Map<string, any>();
    params.set('estado', true);
    params.set('tipo', tipo);

    this.nivelAcademicoService.getAll(params).subscribe((response: any) => {
      // console.log('response nivelesAcademicos', response);
      if (response && response.status === 200) {
        this.nivelesAcademicos = response.body;
      }
    });
  }

  getEstudios(tipoEstudioId: number) {
    this.profesionPerfilEstudioService.getByPerfilProfesionalId(this.perfilId).subscribe((response: any) => {
      // console.log('response getByPerfilProfesionalId', response);
      if (response && response.status === 200 && response.body) {
        response.body.forEach((estudio: Estudio) => {
          if (estudio.tipoEstudioId === tipoEstudioId) {
            this.addNewRow(this.formArrayEstudios, estudio);
          }
        });
      }
    });
  }

  getTipoNivelAcademico(nivelAcademicoID: any) {
    if (nivelAcademicoID === 4) {
      this.pregrado = true;
    } else {
      this.pregrado = false;
    }
  }

  addOrUpdateEstudio(indexRow: number) {
    const control = this.form.get(this.formArrayEstudios).value[indexRow];
    if ((<FormArray>this.form.controls.estudios).controls[indexRow].valid) {

      const estudio = new Estudio();
      estudio._id = control.id;
      estudio.perfilProfesionalId = this.perfilId;
      estudio.tipoEstudioId = this.tipoEstudioId;
      estudio.institucion = control.institucion;
      estudio.nivelAcademicoId = control.nivelAcademico;
      estudio.titulo = control.titulo;
      estudio.estado = control.estado;
      estudio.fechaInicio = control.fechaInicio;
      estudio.fechaFin = control.fechaFin;
      estudio.tarjetaProfesional = control.tarjetaProfesional;
      // console.log('estudio', estudio);

      // Validamos la  operacion
      if (estudio._id) {
        // console.log('UPDATE');
        this.updateEstudio(estudio);
      } else {
        // console.log('ADD');
        this.addEstudio(indexRow, estudio);
      }
    }
  }

  addEstudio(indexRow: number, estudio: Estudio) {
    this.loaderService.display(true);
    this.profesionPerfilEstudioService.add(estudio).pipe(finalize(() => {
      this.loaderService.display(false);
    })).subscribe((response: any) => {
      if (response && response.status === 201) {
        NotifylUtil.showNotification(NotifylUtil.FROM_TOP, NotifylUtil.ALIGN_RIGHT, NotifylUtil.TYPE_NOTIFY_SUCCESS, 'Estudio agregado con éxito');
        response.body.isNew = true;
        this.estudioActivo = false;
        this.addNewRow(this.formArrayEstudios, response.body);
        this.deleteRow(indexRow, this.formArrayEstudios);
      }
    });
  }

  updateEstudio(estudio: Estudio) {
    this.loaderService.display(true);
    this.profesionPerfilEstudioService.update(estudio._id, estudio).pipe(finalize(() => {
      this.loaderService.display(false);
    })).subscribe((response: any) => {
      if (response && response.status === 200) {
        NotifylUtil.showNotification(NotifylUtil.FROM_TOP, NotifylUtil.ALIGN_RIGHT, NotifylUtil.TYPE_NOTIFY_SUCCESS, 'Estudio actualizado con éxito');
        this.formChange = false;
      }
    });
  }

  deleteEstudio(indexRow: number) {
    const estudio = this.form.get(this.formArrayEstudios).value[indexRow];
    if (estudio) {
      let message: string;
      if (estudio.institucion && estudio.titulo) {
        message = `¿Está seguro que desea eliminar el estudio ${estudio.institucion} - ${estudio.titulo}?`;
      } else {
        message = `¿Está seguro que desea eliminar el estudio nuevo?`;
      }
      ModalUtil.showModalConfirm(message, true, ModalUtil.TYPE_MODAL_WARNING, ModalUtil.BTN_NAME_ELIMINAR).then((result) => {
        if (result.value) {
          if (estudio.id) {
            this.loaderService.display(true);
            this.profesionPerfilEstudioService.delete(estudio.id).pipe(finalize(() => {
              this.loaderService.display(false);
            })).subscribe((response: any) => {
              if (response && response.status === 200) {
                this.deleteRow(indexRow, this.formArrayEstudios);
                NotifylUtil.showNotification(NotifylUtil.FROM_TOP, NotifylUtil.ALIGN_RIGHT, NotifylUtil.TYPE_NOTIFY_SUCCESS, 'Estudio eliminado con éxito');
              }
            });
          } else {
            this.deleteRow(indexRow, this.formArrayEstudios);
            NotifylUtil.showNotification(NotifylUtil.FROM_TOP, NotifylUtil.ALIGN_RIGHT, NotifylUtil.TYPE_NOTIFY_SUCCESS, 'Estudio eliminado con éxito');
            this.estudioActivo = false;
          }
        }
      }).catch(swal.noop);
    }
  }

  addNewRow(formArrayName: string, data?: any) {
    if (this.estudioActivo) {
      ModalUtil.showModal('Aún tienes un estudio pendiente por terminar', true, ModalUtil.TYPE_MODAL_WARNING);
    } else {
      const control = <FormArray>this.form.get(formArrayName);
      // console.log('data', data);

      // add new formGroup
      if (formArrayName === this.formArrayEstudios) {
        control.push(this.formBuilder.group({
          id: data && data._id ? data._id : null,
          institucion: [data && data.institucion ? data.institucion : null, [Validators.required, Validators.minLength(3)]],
          nivelAcademico: [data && data.nivelAcademicoId ? data.nivelAcademicoId : null, Validators.required],
          titulo: [data && data.titulo ? data.titulo : null, [Validators.required, Validators.minLength(3)]],
          estado: [data && data.estado ? data.estado : null, Validators.required],
          fechaInicio: [data && data.fechaInicio ? data.fechaInicio : null, Validators.required],
          fechaFin: [data && data.fechaFin ? data.fechaFin : null],
          tarjetaProfesional: [data && data.tarjetaProfesional ? data.tarjetaProfesional : null]
        }));

        if (!data) {
          this.estudioActivo = true;
          // Habilitamos el accordion
          this.setCurrentAccordion(control.length - 1);
          this.accordionState = true;
        }
      }

    }
  }

  deleteRow(index: number, formArrayName: string) {
    const control = <FormArray>this.form.controls[formArrayName];
    control.removeAt(index);
  }

  nivelAcademicoChange(itemRowEstudio: any) {
    if (itemRowEstudio.value.nivelAcademico === 4 && itemRowEstudio.value.estado === 'CULMINADO') {
      itemRowEstudio.controls['tarjetaProfesional'].setValidators([Validators.required, Validators.minLength(4)]);
    } else {
      itemRowEstudio.controls['tarjetaProfesional'].clearValidators();
    }
    itemRowEstudio.controls['tarjetaProfesional'].updateValueAndValidity();
  }

  estadoChange(itemRowEstudio: any) {

    // Actualizamos la tarjetaProfesional
    this.nivelAcademicoChange(itemRowEstudio);

    if (itemRowEstudio.value.estado === 'CULMINADO') {
      itemRowEstudio.controls['fechaFin'].setValidators([Validators.required]);
    } else {
      itemRowEstudio.controls['fechaFin'].clearValidators();
    }
    itemRowEstudio.controls['fechaFin'].updateValueAndValidity();
  }

  setCurrentAccordion(index: number) {
    this.currentAccordion = index;
  }

}
