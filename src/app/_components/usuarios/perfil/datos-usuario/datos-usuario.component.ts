import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, Validators, FormGroup, AbstractControl } from '@angular/forms';

import { Ng2ImgMaxService } from 'ng2-img-max';
import { environment } from '../../../../../environments/environment';

import { ValidatorComponentUtil } from '../../../../_helpers/_util/validator-component-util';
import { MustMatch } from '../../../../_helpers/must-match.validator';
import { ModalUtil } from '../../../../_helpers/_util/modal-util';
import { NotifylUtil } from '../../../../_helpers/_util/notify-util';
import { Config } from '../../../../_configs/config';

import { UsuarioService } from '../../../../_services/usuario.service';

import { Usuario } from '../../../../_entities/usuario';
import { CambioContrasena } from '../../../../_entities/cambioContrasena';
import { TerminosDialog } from 'src/app/_components/_dialogs/terminosYcondiciones/terminos.dialog';
import { MatDialog } from '@angular/material';
import { finalize } from 'rxjs/operators';
import { LoaderService } from 'src/app/_services/_service-util/loader.service';


declare const $: any;

@Component({
  selector: 'app-datos-usuario',
  templateUrl: './datos-usuario.component.html',
  styleUrls: ['./datos-usuario.component.css']
})
export class DatosUsuarioComponent implements OnInit {

  @Input() usuarioId: string;
  @Output() eventFormValid = new EventEmitter();
  @Output() eventFormDatos = new EventEmitter();
  @Output() eventCurrentUsername = new EventEmitter();

  form: FormGroup;
  formPassword: FormGroup;
  usuario: Usuario = new Usuario();
  routeFiles = environment.endpointBucketFiles;

  srcImgPerfil: any;

  constructor(
    private ng2ImgMax: Ng2ImgMaxService,
    private formBuilder: FormBuilder,
    private usuarioService: UsuarioService,
    private loaderService: LoaderService,
    private cd: ChangeDetectorRef,
    public dialog: MatDialog) { }

  ngOnInit() {
    const currentDate = new Date();
    this.srcImgPerfil = this.routeFiles + this.usuarioId + '.jpg?' + currentDate.getMinutes() + currentDate.getSeconds();
    // Inicializamos el form
    if (this.usuarioId) {
      this.getUsuario();
    } else {
      this.initalizeForm();
    }
  }

  setSrcImgPerfil(id: string): void {
    const currentDate = new Date();
    this.usuarioService.setSrcImgPerfil(id, currentDate.getMinutes(), currentDate.getSeconds());
  }

  initalizeForm() {
    this.form = this.formBuilder.group({
      imagenPerfil: [null],
      email: [this.usuario.username ? this.usuario.username : null, Validators.compose([Validators.required, Validators.pattern(Config.emailValido)])],
      oldPassword: [null],
      password: [null, Validators.compose([Validators.required, Validators.minLength(6)])],
      confirmPassword: [null, Validators.required],
      aceptoRecibirPublicidad: [false],
      aceptoTerminosCondiciones: [false, Validators.required],
    }, {
        validator: MustMatch('password', 'confirmPassword')
      });

    // Validamos si esta logueado
    if (this.usuarioId) {
      this.form.get('oldPassword').setValidators([Validators.required]);
      this.form.get('oldPassword').setValidators([Validators.required]);
    }

    // Nos subscribimos para saber cuando el formulario sea valido
    this.form.valueChanges.subscribe(() => {
      if (this.form.get('aceptoTerminosCondiciones').value && this.form.valid) {
        this.eventFormValid.emit(true);
        this.eventFormDatos.emit(this.form.value);
      } else {
        this.eventFormValid.emit(false);
      }
    });
  }

  getUsuario() {
    this.usuarioService.getById(this.usuarioId).subscribe((response: any) => {
      if (response && response.status === 200) {
        this.usuario = response.body;
        this.eventCurrentUsername.emit(this.usuario.username);
        this.initalizeForm();
      }
    });
  }

  validateUsername() {
    // Params
    const params = new Map<string, any>();
    params.set('username', this.form.get('email').value);

    this.usuarioService.count(params).subscribe((response: any) => {
      // console.log('response', response);
      if (response === 0) {
        ModalUtil.showModal('El correo electrónico es valido', false, ModalUtil.TYPE_MODAL_SUCCESS);
      } else {
        ModalUtil.showModal('El correo electrónico ya existe', false, ModalUtil.TYPE_MODAL_ERROR);
      }
    });
  }

  cambiarPassword() {
    ModalUtil.showModalConfirm('¿Está seguro que desea cambiar su contraseña?', true, ModalUtil.TYPE_MODAL_WARNING, ModalUtil.BTN_NAME_ACEPTAR).then((modalResp) => {
      if (modalResp.value) {
        const cambioContrasena: CambioContrasena = new CambioContrasena();
        cambioContrasena.username = this.usuario.username;
        cambioContrasena.oldPassword = this.form.get('oldPassword').value;
        cambioContrasena.newPassword = this.form.get('password').value;
        this.loaderService.display(true);
        this.usuarioService.cambiarContrasena(cambioContrasena).pipe(finalize(() => {
          this.loaderService.display(false);
        })).subscribe((response: any) => {
          if (response && response.status === 200) {
            NotifylUtil.showNotification(NotifylUtil.FROM_TOP, NotifylUtil.ALIGN_RIGHT, NotifylUtil.TYPE_NOTIFY_SUCCESS, 'Contraseña cambiada con éxito');
          } else {
            NotifylUtil.showNotification(NotifylUtil.FROM_TOP, NotifylUtil.ALIGN_RIGHT, NotifylUtil.TYPE_NOTIFY_ERROR, 'Credenciales invalidas');
          }
        });
      }
    });
  }

  subirImagenPrimeraVez(uploadedImage) {
    this.form.patchValue({
      imagenPerfil: uploadedImage
   });
    // need to run CD since file load runs outside of zone
    this.cd.markForCheck();
  }

  selectProfileImage(event: any): void {
    let uploadedImage: File;
    const selectedFile = event.target.files[0];
    // console.log('$selectedFile', selectedFile);
    // console.log('info:', selectedFile.name, selectedFile.type, selectedFile.size);

    this.ng2ImgMax.resizeImage(selectedFile, 400, 400).subscribe((result2: any) => {
      uploadedImage = new File([result2], result2.name, {
        type: 'image/jpeg',
        lastModified: Date.now()
      });

      // Mostramos la imagen
      // console.log('$file new ', uploadedImage);
      // console.log('info2:', uploadedImage.name, uploadedImage.type, uploadedImage.size);

      const reader: FileReader = new FileReader();
      reader.readAsDataURL(uploadedImage);
      reader.onload = () => {
        if (this.usuarioId) {
          this.usuarioService.uploadProfileImage(uploadedImage, this.usuarioId).subscribe((response: any) => {
            this.setSrcImgPerfil(this.usuarioId);
            // console.log('Response selectProfileImage: ', response);
            if (response && response.status === 201) {
              NotifylUtil.showNotification(NotifylUtil.FROM_TOP, NotifylUtil.ALIGN_RIGHT, NotifylUtil.TYPE_NOTIFY_SUCCESS, 'Nueva imágen editada con éxito');
              $('#wizardPicturePreview').attr('src', reader.result).fadeIn('slow');
            } else {
              NotifylUtil.showNotification(NotifylUtil.FROM_TOP, NotifylUtil.ALIGN_RIGHT, NotifylUtil.TYPE_NOTIFY_ERROR, 'La nueva imágen no pudo ser cargada');
            }
          });
        } else {
          $('#wizardPicturePreview').attr('src', reader.result).fadeIn('slow');
          this.subirImagenPrimeraVez(uploadedImage);
        }
      };

    },
      error => {
        console.log('Ocurrio un error cargando la imagen', error);
      }
    );
  }

  openDialogTerminosYCondiciones() {
    const dialogRef = this.dialog.open(TerminosDialog, {
      width: '1080px',
      height: '900px'
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  isFieldInvalid(fieldControl: AbstractControl): boolean {
    return ValidatorComponentUtil.isFieldInvalid(fieldControl);
  }

  getFieldError(fieldControl: AbstractControl, fieldType?: string): string {
    return ValidatorComponentUtil.getFieldError(fieldControl, fieldType);
  }

  isRequired(fieldControl: AbstractControl): boolean {
    return ValidatorComponentUtil.isRequired(fieldControl);
  }

}
