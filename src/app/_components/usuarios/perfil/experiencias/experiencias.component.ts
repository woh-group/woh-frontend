import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators, AbstractControl } from '@angular/forms';

import swal from 'sweetalert2';
import { ModalUtil } from 'src/app/_helpers/_util/modal-util';
import { NotifylUtil } from 'src/app/_helpers/_util/notify-util';
import { ValidatorComponentUtil } from 'src/app/_helpers/_util/validator-component-util';

import { ProfesionalPerfilExperienciaService } from '../../../../_services/profesionales/perfil-experiencia.service';

import { Experiencia } from '../../../../_entities/experiencia';
import { LoaderService } from 'src/app/_services/_service-util/loader.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-experiencias',
  templateUrl: './experiencias.component.html',
  styleUrls: ['./experiencias.component.css']
})
export class ExperienciasComponent implements OnInit {

  form: FormGroup;
  formChange = false;
  @Input() perfilId: string;
  @Output() eventTotalRegistros = new EventEmitter();

  experienciaActiva = false;
  experiencia: Experiencia;

  // Form Arrays
  formArrayExperiencias = 'experiencias';

  // Accordion
  currentAccordion = -1;
  accordionState = false;

  constructor(
    private formBuilder: FormBuilder,
    private profesionalPerfilExperienciaService: ProfesionalPerfilExperienciaService,
    private loaderService: LoaderService) { }

  ngOnInit() {
    this.initalizeForm();
    this.getExperiencias();
  }

  initalizeForm() {
    this.form = this.formBuilder.group({
      experiencias: new FormArray([])
    });

    // Nos subscribimos para saber cuando el formulario cambie
    let cantidadRegistros = 0;
    this.form.valueChanges.subscribe(() => {
      if (this.form.get('experiencias').value && cantidadRegistros !== this.form.get('experiencias').value.length) {
        cantidadRegistros = this.form.get('experiencias').value.length;
        this.eventTotalRegistros.emit({
          perfilId: this.perfilId,
          cantidadRegistros: cantidadRegistros
        });
      }
    });
  }

  isFieldInvalid(fieldControl: AbstractControl): boolean {
    return ValidatorComponentUtil.isFieldInvalid(fieldControl);
  }

  getFieldError(fieldControl: AbstractControl, fieldType?: string): string {
    return ValidatorComponentUtil.getFieldError(fieldControl, fieldType);
  }

  isRequired(fieldControl: AbstractControl): boolean {
    return ValidatorComponentUtil.isRequired(fieldControl);
  }

  getExperiencias() {
    this.loaderService.display(true);
    this.profesionalPerfilExperienciaService.getByPerfilProfesionalId(this.perfilId).pipe(finalize(() => {
      this.loaderService.display(false);
    })).subscribe((response: any) => {
      if (response && response.status === 200 && response.body) {
        response.body.forEach((experiencia: Experiencia) => {
          this.addNewRow(this.formArrayExperiencias, experiencia);
        });
      }
    });
  }

  addOrUpdateExperiencia(indexRow: number) {
    const control = this.form.get(this.formArrayExperiencias).value[indexRow];
    if ((<FormArray>this.form.controls.experiencias).controls[indexRow].valid) {

      const experiencia = new Experiencia();
      experiencia._id = control.id;
      experiencia.perfilProfesionalId = this.perfilId;
      experiencia.empresa = control.empresa;
      experiencia.cargo = control.cargo;
      experiencia.fechaInicio = control.fechaInicio;
      experiencia.fechaFin = control.fechaFin;
      experiencia.funciones = control.funciones;
      experiencia.actualmente = control.actualmente;

      // Validamos la  operacion
      if (experiencia._id) {
        this.updateExperiencia(experiencia);
      } else {
        this.addExperiencia(indexRow, experiencia);
      }
    }
  }

  addExperiencia(indexRow: number, experiencia: Experiencia) {
    this.loaderService.display(true);
    this.profesionalPerfilExperienciaService.add(experiencia).pipe(finalize(() => {
      this.loaderService.display(false);
    })).subscribe((response: any) => {
      if (response && response.status === 201) {
        NotifylUtil.showNotification(NotifylUtil.FROM_TOP, NotifylUtil.ALIGN_RIGHT, NotifylUtil.TYPE_NOTIFY_SUCCESS, 'Experiencia agregada con éxito');
        response.body.isNew = true;
        this.experienciaActiva = false;
        this.addNewRow(this.formArrayExperiencias, response.body);
        this.deleteRow(indexRow, this.formArrayExperiencias);
      }
    });
  }

  updateExperiencia(experiencia: Experiencia) {
    this.loaderService.display(true);
    this.profesionalPerfilExperienciaService.update(experiencia._id, experiencia).pipe(finalize(() => {
      this.loaderService.display(false);
    })).subscribe((response: any) => {
      if (response && response.status === 200) {
        NotifylUtil.showNotification(NotifylUtil.FROM_TOP, NotifylUtil.ALIGN_RIGHT, NotifylUtil.TYPE_NOTIFY_SUCCESS, 'Experiencia actualizado con éxito');
        this.formChange = false;
      }
    });
  }

  deleteExperiencia(indexRow: number) {
    const experiencia = this.form.get(this.formArrayExperiencias).value[indexRow];
    if (experiencia) {
      let message: string;
      if (experiencia.empresa && experiencia.cargo) {
        message = `¿Está seguro que desea eliminar la experiencia ${experiencia.empresa} - ${experiencia.cargo}?`;
      } else {
        message = `¿Está seguro que desea eliminar la experiencia nueva?`;
      }
      ModalUtil.showModalConfirm(message, true, ModalUtil.TYPE_MODAL_WARNING, ModalUtil.BTN_NAME_ELIMINAR).then((result) => {
        if (result.value) {
          if (experiencia.id) {
            this.loaderService.display(true);
            this.profesionalPerfilExperienciaService.delete(experiencia.id).pipe(finalize(() => {
              this.loaderService.display(false);
            })).subscribe((response: any) => {
              if (response && response.status === 200) {
                this.deleteRow(indexRow, this.formArrayExperiencias);
                NotifylUtil.showNotification(NotifylUtil.FROM_TOP, NotifylUtil.ALIGN_RIGHT, NotifylUtil.TYPE_NOTIFY_SUCCESS, 'Experiencia eliminada con éxito');
              }
            });
          } else {
            this.deleteRow(indexRow, this.formArrayExperiencias);
            NotifylUtil.showNotification(NotifylUtil.FROM_TOP, NotifylUtil.ALIGN_RIGHT, NotifylUtil.TYPE_NOTIFY_SUCCESS, 'Experiencia eliminada con éxito');
            this.experienciaActiva = false;
          }
        }
      }).catch(swal.noop);
    }
  }

  addNewRow(formArrayName: string, data?: any) {
    if (this.experienciaActiva) {
      ModalUtil.showModal('Aún tienes una experiencia pendiente por terminar', true, ModalUtil.TYPE_MODAL_WARNING);
    } else {
      const control = <FormArray>this.form.get(formArrayName);

      // add new formGroup
      if (formArrayName === this.formArrayExperiencias) {
        control.push(this.formBuilder.group(
          {
            id: data && data._id ? data._id : null,
            empresa: [data && data.empresa ? data.empresa : null, Validators.required],
            cargo: [data && data.cargo ? data.cargo : null, [Validators.required, Validators.minLength(5)]],
            fechaInicio: [data && data.fechaInicio ? data.fechaInicio : null, Validators.required],
            fechaFin: [data && data.fechaFin ? data.fechaFin : null],
            funciones: [data && data.funciones ? data.funciones : null, [Validators.required, Validators.minLength(5)]],
            actualmente: [data && data.actualmente ? data.actualmente : null]
          }));

        if (!data) {
          this.experienciaActiva = true;
          // Habilitamos el accordion
          this.setCurrentAccordion(control.length - 1);
          this.accordionState = true;
        }
      }

    }
  }

  deleteRow(index: number, formArrayName: string) {
    const control = <FormArray>this.form.controls[formArrayName];
    control.removeAt(index);
  }

  setCurrentAccordion(index: number) {
    this.currentAccordion = index;
  }

}
