import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { Config } from '../../../../_configs/config';
import { ModalUtil } from '../../../../_helpers/_util/modal-util';
import { JwtUtil } from '../../../../_helpers/_util/jwt-util';

import { AutenticacionService } from '../../../../_services/autenticacion.service';
import { UsuarioService } from '../../../../_services/usuario.service';

import { Persona } from '../../../../_entities/persona';
import { Usuario } from '../../../../_entities/usuario';
import { Profesional } from 'src/app/_entities/profesional';
import { ProfesionalService } from '../../../../_services/profesionales/profesional.service';
import { NotifylUtil } from 'src/app/_helpers/_util/notify-util';
import { LoaderService } from 'src/app/_services/_service-util/loader.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  // Formulario
  form: FormGroup;
  isValidFormDatosUsuario = false;
  isValidFormDatosPersonales = false;
  registroBasico = true;
  title: string;

  private usuarioId = null;
  private loggedIn = false;
  private userRol = 0;
  private scope: '';

  currentTab = 1;
  currentUsername: string;
  persona: Persona = new Persona();
  usuario: Usuario = new Usuario();
  profesional: Profesional;
  password: string;
  showActivarUsuario = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private usuarioService: UsuarioService,
    private autenticacionService: AutenticacionService,
    private profesionalService: ProfesionalService,
    private loaderService: LoaderService
  ) {
    // Obtenemos el rol por url
    this.getRolByUrlParams();

    // Validamos si esta logueado
    this.autenticacionService.currentUser.subscribe(response => {
      if (response && response.accessToken) {
        this.loggedIn = true;

        // Obtenemos los datos del token
        const tokenInfo = JwtUtil.decodeToken(response.accessToken);
        this.usuarioId = tokenInfo.sub;
        this.userRol = tokenInfo.rol;
        this.registroBasico = false;

        // Obtenemos los datos de la persona
        this.autenticacionService.currentPerson.subscribe(responsePersona => {
          if (responsePersona) {
            this.persona = responsePersona;
          }
        });
      } else {
        this.loggedIn = false;
        if (this.userRol === Config.rolPaciente) {
          this.registroBasico = true;
        } else {
          this.registroBasico = false;
        }
      }
    }, error => {
      this.loggedIn = false;
    });
  }

  ngOnInit() {
    this.initalizeForm();

    // Obtenemos el scope
    this.scope = this.route.snapshot.data.scope;

    // Obtenemos el titulo del form
    this.getTitle();
  }

  getTitle() {
    if (this.userRol === Config.rolPaciente) {
      switch (this.scope) {
        case Config.scopeADD: {
          this.title = 'REGISTRO';
          break;
        }
        case Config.scopeUPDATE: {
          this.title = 'EDITAR PERFIL';
          break;
        }
      }
    } else if (this.userRol === Config.rolProfesional) {
      switch (this.scope) {
        case Config.scopeADD: {
          this.title = 'REGISTRO PROFESIONAL';
          break;
        }
        case Config.scopeUPDATE: {
          this.title = 'EDITAR PERFIL';
          break;
        }
      }
    }
  }

  getRolByUrlParams() {
    // Obtenemos el parametro por url
    const type = this.route.snapshot.params['type'];
    if (type === 'profesional') {
      this.userRol = Config.rolProfesional;
    } else {
      this.userRol = Config.rolPaciente;
    }
  }

  initalizeForm() {
    this.form = this.formBuilder.group({
      terminos: [null, Validators.required]
    });
  }

  isValidUsername() {
    if (this.persona.correo) {
      // Params
      const params = new Map<string, any>();
      params.set('username', this.persona.correo);

      this.usuarioService.isValidUsername(this.persona.correo).subscribe((response: any) => {
        if (response != null) {
          if (response) {
            this.currentTab = 2;
          } else {
            ModalUtil.showModal('El correo electrónico ya existe', false, ModalUtil.TYPE_MODAL_ERROR);
          }
        } else {
          NotifylUtil.showNotification(NotifylUtil.FROM_TOP, NotifylUtil.ALIGN_RIGHT, NotifylUtil.TYPE_NOTIFY_ERROR, NotifylUtil.MESSAGE_ERROR_GENERICO);
        }
      });
    }
  }

  addUsuario() {
    this.usuario.username = this.persona.correo;
    this.usuario.password = this.password;

    // Asignamos el rol
    if (this.userRol === Config.rolPaciente) { // Paciente
      this.usuario.estadoId = Config.estadoUsuarioInactivo;
      this.usuario.rol = Config.rolPaciente;
    } else if (this.userRol === Config.rolProfesional) { // Profesional
      this.usuario.estadoId = Config.estadoUsuarioInactivo;
      this.usuario.rol = Config.rolProfesional;
    }

    this.usuario.permisos = Config.permisos;
    this.usuario.persona = this.persona;

    // Agregamos el usuario
    this.loaderService.display(true);
    this.usuarioService.add(this.usuario).pipe(finalize(() => {
      this.loaderService.display(false);
    })).subscribe((response: any) => {
      if (response && response.status === 201) {
        // NotifylUtil.showNotification(NotifylUtil.FROM_TOP, NotifylUtil.ALIGN_RIGHT, NotifylUtil.TYPE_NOTIFY_SUCCESS, 'Usuario registrado con éxito');
        this.usuario._id = response.body._id;
        this.usuarioId = response.body._id;

        // Mostramos el mensaje de activar usuario
        this.showActivarUsuario = true;

        // Validamos si hay que cargar una imagen
        if (this.usuario.imagenPerfil) {
          this.cargarImagen(this.usuario.imagenPerfil, this.usuarioId);
        } else {
          this.cargarImagenPorDefecto(this, this.usuarioId);
        }
      }
    });
  }

  cargarImagenPorDefecto(context: any, usuarioId: string) {
    let blob = null;
    let file: File;
    const xhr = new XMLHttpRequest();
    xhr.open('GET', './assets/img/avatar-user-default.png');
    xhr.responseType = 'blob'; // force the HTTP response, response-type header to be blob
    xhr.onload = function () {
      blob = xhr.response; // xhr.response is now a blob object
      file = new File([blob], usuarioId + '.jpg', { type: 'image/jpeg', lastModified: Date.now() });
      context.cargarImagen(file, usuarioId);
    };
    xhr.send();
  }

  cargarImagen(uploadedImage: File, usuarioId: string) {
    this.usuarioService.uploadProfileImage(uploadedImage, usuarioId).subscribe((response: any) => {
      if (response && response.status === 201) {
        NotifylUtil.showNotification(NotifylUtil.FROM_TOP, NotifylUtil.ALIGN_RIGHT, NotifylUtil.TYPE_NOTIFY_SUCCESS, 'Imágen cargada con éxito');
      }
    });
  }

  // ******************** TABS ********************
  getFormDatosUsuarioValidEvent(val: boolean) {
    this.isValidFormDatosUsuario = val;
  }

  getFormValidEvent(val: boolean) {
    this.isValidFormDatosPersonales = val;
  }

  getFormDatosUsuarioDataEvent(data: any) {
    if (data) {
      this.persona.correo = data.email;
      this.password = data.password;
      this.usuario.imagenPerfil = data.imagenPerfil;
      this.usuario.aceptoRecibirPublicidad = data.aceptoRecibirPublicidad;

      // Version terminos y condiciones
      this.usuario.versionTerminosCondiciones = data.aceptoTerminosCondiciones ? Config.versionTerminosCondiciones : 0;
    }
  }

  getFormDataEvent(data: any) {
    // console.log('getFormDataEvent', JSON.stringify(data));
    if (data) {
      this.persona.tipoDocumentoId = data.tipoDocumentoId;
      this.persona.numeroDocumento = data.numeroDocumento;
      this.persona.nombres = data.nombres;
      this.persona.apellidos = data.apellidos;
      this.persona.sexo = data.sexo;
      this.persona.fechaNacimiento = data.fechaNacimiento;
      this.persona.estadoCivil = data.estadoCivil;
      this.persona.telefono = data.telefono;
      this.persona.celular = data.celular;
      this.persona.direccion = data.direccion;
      this.persona.codigoPostal = data.codigoPostal;
      this.persona.paisId = data.pais;
      this.persona.departamentoId = data.departamento;
      this.persona.ciudadId = data.ciudad;
    }
  }

  getCurrentUsernameEvent(data: string) {
    this.currentUsername = data;
  }

  getFormsValid(): boolean {
    if (this.isValidFormDatosUsuario && this.isValidFormDatosPersonales) {
      return true;
    } else {
      return false;
    }
  }

}
