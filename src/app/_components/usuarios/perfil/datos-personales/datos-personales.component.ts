import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, Validators, FormGroup, AbstractControl, NgControl } from '@angular/forms';
import { finalize } from 'rxjs/operators';

import { Config } from '../../../../_configs/config';
import { ValidatorComponentUtil } from '../../../../_helpers/_util/validator-component-util';
import { ModalUtil } from '../../../../_helpers/_util/modal-util';
import { NotifylUtil } from '../../../../_helpers/_util/notify-util';

import { TipoDocumentoService } from '../../../../_services/comunes/tipoDocumento.service';
import { PaisService } from '../../../../_services/comunes/pais.service';
import { DepartamentoService } from '../../../../_services/comunes/departamento.service';
import { AutenticacionService } from '../../../../_services/autenticacion.service';
import { UsuarioService } from '../../../../_services/usuario.service';
import { LoaderService } from 'src/app/_services/_service-util/loader.service';

import { Pais } from '../../../../_entities/pais';
import { Departamento } from '../../../../_entities/departamento';
import { Ciudad } from '../../../../_entities/ciudad';
import { TipoDocumento } from '../../../../_entities/tipoDocumento';
import { Persona } from '../../../../_entities/persona';


@Component({
  selector: 'app-datos-personales',
  templateUrl: './datos-personales.component.html',
  styleUrls: ['./datos-personales.component.css']
})
export class DatosPersonalesComponent implements OnInit {

  @Input() scope: string;
  @Input() usuarioId: string;
  @Input() currentEmail: string;
  @Input() userRol: number;
  @Input() registroBasico: boolean;

  @Output() eventFormValid = new EventEmitter();
  @Output() eventFormDatos = new EventEmitter();

  form: FormGroup;
  tiposDocumento: TipoDocumento[];
  paises: Pais[];
  departamentos: Departamento[];
  ciudades: Ciudad[];
  datosPersonaRegistrados: Persona;
  edadMinimaRegistro = Config.maxDateParaRegistro;
  persona: Persona = new Persona();

  constructor(
    private formBuilder: FormBuilder,
    private autenticacionService: AutenticacionService,
    private usuarioService: UsuarioService,
    private paisService: PaisService,
    private departamentoService: DepartamentoService,
    private tiposDocumentoService: TipoDocumentoService,
    private loaderService: LoaderService
  ) {
  }

  ngOnInit() {
    if (!this.registroBasico) {
      this.getTiposDocumento();
      this.getPaises();
    }

    // Inicializamos el form
    if (this.usuarioId) {
      this.getPersona();
    } else {
      this.initalizeForm();
    }
  }

  getRequieredFields() {

    if (this.registroBasico || this.userRol === Config.rolPaciente) {
      return {
        'tipoDocumentoId': false,
        'numeroDocumento': false,
        'nombres': true,
        'apellidos': true,
        'sexo': true,
        'fechaNacimiento': true,
        'estadoCivil': true,
        'telefono': false,
        'celular': false,
        'direccion': false,
        'codigoPostal': false,
        'pais': false,
        'departamento': false,
        'ciudad': false
      };
    } else {
      return {
        'tipoDocumentoId': true,
        'numeroDocumento': true,
        'nombres': true,
        'apellidos': true,
        'sexo': true,
        'fechaNacimiento': true,
        'estadoCivil': true,
        'telefono': false,
        'celular': false,
        'direccion': false,
        'codigoPostal': false,
        'pais': false,
        'departamento': false,
        'ciudad': false
      };
    }
  }

  initalizeForm() {
    // Validamos si esta logueado
    if (this.usuarioId && this.persona) {
      this.form = this.formBuilder.group({
        tipoDocumentoId: [this.persona.tipoDocumentoId, null],
        numeroDocumento: [this.persona.numeroDocumento, [Validators.minLength(3), Validators.maxLength(15)]],
        nombres: [this.persona.nombres, Validators.compose([Validators.minLength(2)])],
        apellidos: [this.persona.apellidos, [Validators.minLength(2)]],
        sexo: [this.persona.sexo],
        fechaNacimiento: [this.persona.fechaNacimiento],
        estadoCivil: [this.persona.estadoCivil],
        telefono: [this.persona.telefono, [Validators.minLength(7)]],
        celular: [this.persona.celular, [Validators.minLength(10)]],
        direccion: [this.persona.direccion, [Validators.minLength(10)]],
        codigoPostal: [this.persona.codigoPostal, [Validators.minLength(4)]],
        pais: [this.persona.paisId],
        departamento: [this.persona.departamentoId],
        ciudad: [this.persona.ciudadId]
      });
    } else {
      this.form = this.formBuilder.group({
        tipoDocumentoId: [null, null],
        numeroDocumento: [null, [Validators.minLength(3), Validators.maxLength(5)]],
        nombres: [null, Validators.compose([Validators.minLength(2)])],
        apellidos: [null, [Validators.minLength(2)]],
        sexo: [null],
        fechaNacimiento: [null],
        estadoCivil: [null],
        telefono: [null, [Validators.minLength(7)]],
        celular: [null, [Validators.minLength(10)]],
        direccion: [null, [Validators.minLength(10)]],
        codigoPostal: [null, [Validators.minLength(4)]],
        pais: [null],
        departamento: [null],
        ciudad: [null]
      });
    }

    // Asignamos el validador a los campos requeridos
    const fields = this.getRequieredFields();
    Object.keys(fields).map(key => {
      if (fields[key]) {
        if (key === 'nombres' || key === 'apellidos') {
          this.form.get(key).setValidators([Validators.required, Validators.pattern(Config.soloLetras)]);
        } else {
          this.form.get(key).setValidators([Validators.required]);
        }
      }
    });

    // Nos subscribimos para saber cuando el formulario sea valido
    this.form.valueChanges.subscribe(val => {
      if (this.form.valid) {
        this.eventFormValid.emit(true);
        this.eventFormDatos.emit(this.form.value);
      }
    });
    this.disabledFields();
  }

  disabledFields() {
    if (this.usuarioId && this.form.get('tipoDocumentoId').value != null) {
      this.form.get('tipoDocumentoId').disable();
    }
    if (this.usuarioId && this.form.get('numeroDocumento').value != null) {
      this.form.get('numeroDocumento').disable();
    }
    if (this.usuarioId && this.form.get('nombres').value != null) {
      this.form.get('nombres').disable();
    }
    if (this.usuarioId && this.form.get('apellidos').value != null) {
      this.form.get('apellidos').disable();
    }
    if (this.usuarioId && this.form.get('sexo').value != null) {
      this.form.get('sexo').disable();
    }
    if (this.usuarioId && this.form.get('fechaNacimiento').value != null) {
      this.form.get('fechaNacimiento').disable();
    }
  }

  isFieldInvalid(fieldControl: AbstractControl): boolean {
    return ValidatorComponentUtil.isFieldInvalid(fieldControl);
  }

  getFieldError(fieldControl: AbstractControl, fieldType?: string): string {
    return ValidatorComponentUtil.getFieldError(fieldControl, fieldType);
  }

  isRequired(fieldControl: AbstractControl): boolean {
    return ValidatorComponentUtil.isRequired(fieldControl);
  }

  getPersona() {
    this.usuarioService.getPersonaByUsuarioId(this.usuarioId).subscribe((response: any) => {
      if (response && response.status === 200) {
        this.persona = response.body;
        if (this.persona.paisId) {
          this.getDepartamentos(this.persona.paisId);
          this.getCiudades(this.persona.departamentoId);
        }
        this.initalizeForm();
      }
    });
  }

  getTiposDocumento() {
    // Params
    const params = new Map<string, any>();
    params.set('estado', true);

    this.tiposDocumentoService.getAll(params).subscribe((response: any) => {
      if (response && response.status === 200) {
        this.tiposDocumento = response.body;
      }
    });
  }

  getPaises() {
    // Params
    const params = new Map<string, any>();
    params.set('estado', true);

    this.paisService.getAll(params).subscribe((response: any) => {
      if (response && response.status === 200) {
        this.paises = response.body;
      }
    });
  }

  getDepartamentos(paisId: any) {
    if (paisId) {
      // Params
      const params = new Map<string, any>();
      params.set('estado', true);

      this.paisService.GetDepartamentosByPaisId(paisId, params).subscribe((response: any) => {
        if (response && response.status === 200) {
          this.departamentos = response.body;
        }
      });
    }
  }

  getCiudades(departamentoId: any) {
    if (departamentoId) {

      // Params
      const params = new Map<string, any>();
      params.set('estado', true);

      this.departamentoService.GetCiudadesByDepartamentoId(departamentoId, params).subscribe((response: any) => {
        if (response && response.status === 200) {
          this.ciudades = response.body;
        }
      });
    }
  }

  updatePersona() {
    ModalUtil.showModalConfirm('¿Está seguro que desea modificar sus datos personales?', true, ModalUtil.TYPE_MODAL_WARNING, ModalUtil.BTN_NAME_ACEPTAR).then((modalResp) => {
      if (modalResp.value) {
        const personaNew: Persona = this.form.getRawValue();
        personaNew.correo = this.currentEmail;
        personaNew.paisId = this.form.get('pais').value;
        personaNew.departamentoId = this.form.get('departamento').value;
        personaNew.ciudadId = this.form.get('ciudad').value;
        this.loaderService.display(true);
        this.usuarioService.updatePersona(this.usuarioId, personaNew).pipe(finalize(() => {
          this.loaderService.display(false);
          this.disabledFields();
        })).subscribe((response: any) => {
          if (response && response.status === 200) {
            this.persona = response.body;
            this.autenticacionService.savePersonalocalStorage(this.persona);
            NotifylUtil.showNotification(NotifylUtil.FROM_TOP, NotifylUtil.ALIGN_RIGHT, NotifylUtil.TYPE_NOTIFY_SUCCESS, 'Datos actualizados con éxito');
          }
        });
      }
    });
  }

}
