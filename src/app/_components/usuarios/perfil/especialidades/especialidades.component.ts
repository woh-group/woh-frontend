import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

import { NotifylUtil } from 'src/app/_helpers/_util/notify-util';

import { EspecialidadService } from '../../../../_services/especialidad.service';
import { ProfesionalPerfilService } from '../../../../_services/profesionales/perfil.service';

import { Especialidad } from '../../../../_entities/especialidad';
import { PerfilProfesional } from '../../../../_entities/perfilProfesional';
import { LoaderService } from 'src/app/_services/_service-util/loader.service';
import { finalize } from 'rxjs/operators';


@Component({
  selector: 'app-especialidades',
  templateUrl: './especialidades.component.html',
  styleUrls: ['./especialidades.component.css']
})
export class EspecialidadesComponent implements OnInit {

  form: FormGroup;
  @Input() perfil: PerfilProfesional;
  @Input() profesionalId: string;

  especialidades: Especialidad[] = [];
  especialidadesNoRegistradas: Especialidad[] = [];
  especialidad: Especialidad;

  constructor(
    private formBuilder: FormBuilder,
    private profesionalPerfilService: ProfesionalPerfilService,
    private especialidadService: EspecialidadService,
    private loaderService: LoaderService
  ) { }

  ngOnInit() {
    this.initalizeForm();

    // Inicializamos en caso de ser necesario
    if (!this.perfil.especialidades) {
      this.perfil.especialidades = [];
    }

    this.getEspecialidades();
  }

  initalizeForm() {
    this.form = this.formBuilder.group({
      especialidades: [null, null]
    });
  }

  getEspecialidades() {
    // Params
    const params = new Map<string, any>();
    params.set('estado', true);
    this.loaderService.display(true);
    this.especialidadService.getAll(params).pipe(finalize(() => {
      this.loaderService.display(false);
    })).subscribe((response: any) => {
      if (response && response.status === 200) {
        this.especialidades = response.body;
        this.getEspecialidadesNoRegistradas();
      }
    });
  }

  getEspecialidadesNoRegistradas() {
    if (this.especialidades) {
      this.especialidadesNoRegistradas = [];
      this.especialidades.forEach((especialidad: Especialidad) => {
        if (this.perfil.especialidades.indexOf(especialidad.descripcion) === -1) {
          this.especialidadesNoRegistradas.push(especialidad);
        }
      });
    }
  }

  addEspecialidad(event: any) {
    if (event.isUserInput) {
      let especialidades: string[] = [];
      especialidades = especialidades.concat(this.perfil.especialidades);
      especialidades.push(event.source.value);
      this.updateEspecialidad(especialidades);
    }
  }

  deleteEspecialidad(especialidad: string) {
    const especialidades = this.perfil.especialidades.filter(obj => obj !== especialidad);
    this.updateEspecialidad(especialidades);
  }

  updateEspecialidad(especialidades: string[]) {
    // console.log(especialidad, '->', JSON.stringify(especialidades));
    this.loaderService.display(true);
    this.profesionalPerfilService.updateEspecialidades(this.profesionalId, this.perfil._id, especialidades).pipe(finalize(() => {
      this.loaderService.display(false);
    })).subscribe((response: any) => {
      if (response && response.status === 200) {
        //  NotifylUtil.showNotification(NotifylUtil.FROM_TOP, NotifylUtil.ALIGN_RIGHT, NotifylUtil.TYPE_NOTIFY_SUCCESS, 'Experiencia actualizado con éxito');
        this.perfil.especialidades = especialidades;
        this.getEspecialidadesNoRegistradas();
      }
    });
  }

}
