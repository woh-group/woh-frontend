import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import { Router } from '@angular/router';

import swal from 'sweetalert2';
import { ModalUtil } from '../../../../_helpers/_util/modal-util';
import { NotifylUtil } from 'src/app/_helpers/_util/notify-util';
import { PageUtil } from '../../../../_helpers/_util/page-util';

import { ProfesionService } from '../../../../_services/comunes/profesion.service';
import { EspecialidadService } from '../../../../_services/especialidad.service';
import { ProfesionalService } from '../../../../_services/profesionales/profesional.service';
import { ProfesionalPerfilService } from '../../../../_services/profesionales/perfil.service';

import { Especialidad } from '../../../../_entities/especialidad';
import { Profesional } from '../../../../_entities/profesional';
import { Profesion } from '../../../../_entities/profesion';
import { PerfilProfesional } from '../../../../_entities/perfilProfesional';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { LoaderService } from 'src/app/_services/_service-util/loader.service';
import { finalize } from 'rxjs/operators';


@Component({
  selector: 'app-perfil-profesional',
  templateUrl: './perfil-profesional.component.html',
  styleUrls: ['./perfil-profesional.component.css']
})
export class PerfilProfesionalComponent implements OnInit {

  form: FormGroup;
  formChange = false;
  @Input() usuarioId: string;

  especialidades: Especialidad[] = [];
  idsProfesiones: number[] = [];
  profesiones: Profesion[] = [];
  profesionesNoRegistradas: Profesion[] = [];

  profesional: Profesional;

  // Form Arrays
  formArrayPerfiles = 'perfiles';

  // Tap
  tapActiveIndex = 0;

  // Accordion
  currentAccordion = -1;
  accordionState = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private especialidadService: EspecialidadService,
    private profesionService: ProfesionService,
    private profesionalService: ProfesionalService,
    private profesionalPerfilService: ProfesionalPerfilService,
    private loaderService: LoaderService
  ) { }

  ngOnInit() {
    this.initalizeForm();
    this.getEspecialidades();
    
    if (this.usuarioId) {
      this.getProfesiones();
    }
  }

  initalizeForm() {
    this.form = this.formBuilder.group({
      // especialidadPrincipal: [null],
      profesion: [null, Validators.required],
      perfiles: new FormArray([])
    });

    // Nos subscribimos para saber cuando el formulario cambie
    this.form.valueChanges.subscribe(() => {
      this.formChange = true;
    });
  }

  getEspecialidades() {
    // Params
    const params = new Map<string, any>();
    params.set('estado', true);

    this.especialidadService.getAll(params).subscribe((response: any) => {
      if (response && response.status === 200) {
        this.especialidades = response.body;
      }
    });
  }

  getProfesiones() {
    // Params
    const params = new Map<string, any>();
    params.set('estado', true);

    this.profesionService.getAll(params).subscribe((response: any) => {
      // console.log('response', response);
      if (response && response.status === 200) {
        this.profesiones = response.body;

        // Obtenemos el profesional
        this.getProfesional();
      }
    });
  }

  getProfesionesNoRegistradas() {
    if (this.profesiones) {
      this.profesionesNoRegistradas = [];
      this.profesiones.forEach((profesion: Profesion) => {
        if (this.idsProfesiones.indexOf(profesion.id) === -1) {
          this.profesionesNoRegistradas.push(profesion);
        }
      });
    }
  }

  getProfesional() {
    this.profesionalService.getByUsuarioId(this.usuarioId).subscribe((response: any) => {
      // console.log('response profesional', response);
      if (response && response.status === 200) {
        this.profesional = response.body;
        // this.form.get('especialidadPrincipal').setValue(this.profesional.especialidadPrincipal);

        if (response.body.perfiles) {
          response.body.perfiles.forEach((perfil: PerfilProfesional) => {
            this.addNewRow(this.formArrayPerfiles, perfil);

            // Actualizamos el combo de profesiones no registradas
            this.idsProfesiones.push(perfil.profesionId);
          });
        }
        this.getProfesionesNoRegistradas();
      }
    });
  }

  updateEspecialidadPrincipal() {
    if (this.form.get('especialidadPrincipal').value) {
      this.profesional.especialidadPrincipal = this.form.get('especialidadPrincipal').value;
      this.loaderService.display(true);
      this.profesionalService.update(this.profesional._id, this.profesional).pipe(finalize(() => {
        this.loaderService.display(false);
      })).subscribe((response: any) => {
        // console.log('response update especialidadPrincipal', response);
        if (response && response.status === 200) {
          NotifylUtil.showNotification(NotifylUtil.FROM_TOP, NotifylUtil.ALIGN_RIGHT, NotifylUtil.TYPE_NOTIFY_SUCCESS, 'Especialidad actualizada con éxito');
        }
      });
    }
  }

  addPerfilProfesional() {
    const perfil = new PerfilProfesional();
    perfil.profesionId = this.form.get('profesion').value;
    this.loaderService.display(true);
    this.profesionalPerfilService.add(this.profesional._id, perfil).pipe(finalize(() => {
      this.loaderService.display(false);
    })).subscribe((response: any) => {
      if (response && response.status === 201) {
        NotifylUtil.showNotification(NotifylUtil.FROM_TOP, NotifylUtil.ALIGN_RIGHT, NotifylUtil.TYPE_NOTIFY_SUCCESS, 'Profesión agregado con éxito');
        response.body.isNew = true;
        this.addNewRow(this.formArrayPerfiles, response.body);
        if (!this.profesional.perfiles) {
          this.profesional.perfiles = [];
        }
        this.profesional.perfiles.push(response.body);

        // Actualizamos el combo de profesiones no registradas
        this.idsProfesiones.push(response.body.profesionId);
        this.getProfesionesNoRegistradas();

        // Limpiamos el combo
        this.form.controls['profesion'].markAsUntouched();
        this.form.get('profesion').setValue(null);
      }
    });
  }

  updatePerfilProfesional(indexRow: number) {
    const perfil = this.form.get(this.formArrayPerfiles).value[indexRow];
    if (perfil) {
      const perfilProfesional = new PerfilProfesional();
      perfilProfesional.profesionId = perfil.profesionId;
      perfilProfesional.perfil = perfil.descripcion;
      this.loaderService.display(true);
      this.profesionalPerfilService.update(this.profesional._id, perfil.id, perfilProfesional).pipe(finalize(() => {
        this.loaderService.display(false);
      })).subscribe((response: any) => {
        if (response && response.status === 200) {
          NotifylUtil.showNotification(NotifylUtil.FROM_TOP, NotifylUtil.ALIGN_RIGHT, NotifylUtil.TYPE_NOTIFY_SUCCESS, 'Perfil actualizado con éxito');
          this.formChange = false;
        }
      });
    }
  }

  deletePerfilProfesional(indexRow: number) {
    const perfil = this.form.get(this.formArrayPerfiles).value[indexRow];
    if (perfil) {
      if (perfil.data.estudios || perfil.data.estudiosComplementarios || perfil.data.experiencias) {
        let message = 'Debe eliminar la información asociada antes de eliminar el perfil<br>';
        if (perfil.data.estudios) {
          message += '<br>Estudios: ' + perfil.data.estudios;
        }
        if (perfil.data.estudiosComplementarios) {
          message += '<br>Estudios Complementarios: ' + perfil.data.estudiosComplementarios;
        }
        if (perfil.data.experiencias) {
          message += '<br>Experiencias: ' + perfil.data.experiencias;
        }
        ModalUtil.showModal(message, true, ModalUtil.TYPE_MODAL_ERROR);
      } else {
        ModalUtil.showModalConfirm(`¿Está seguro que desea eliminar el perfil ${this.getProfesionNameById(perfil.profesionId)}?`, true, ModalUtil.TYPE_MODAL_WARNING, ModalUtil.BTN_NAME_ELIMINAR).then((result) => {
          if (result.value) {
            this.loaderService.display(true);
            this.profesionalPerfilService.delete(this.profesional._id, perfil.id).pipe(finalize(() => {
              this.loaderService.display(false);
            })).subscribe((response: any) => {
              if (response && response.status === 200) {
                this.deleteRow(indexRow, this.formArrayPerfiles);
                NotifylUtil.showNotification(NotifylUtil.FROM_TOP, NotifylUtil.ALIGN_RIGHT, NotifylUtil.TYPE_NOTIFY_SUCCESS, 'Perfil eliminado con éxito');
                this.profesional.perfiles.splice(indexRow, 1);

                // Actualizamos el combo de profesiones no registradas
                this.idsProfesiones = this.idsProfesiones.filter(obj => obj !== perfil.profesionId);
                this.getProfesionesNoRegistradas();
              }
            });
          }
        }).catch(swal.noop);
      }
    }
  }

  getProfesionNameById(id: number): string {
    let name = '--';
    const profesionesTemp = this.profesiones.filter(item => item.id === id);
    if (profesionesTemp.length > 0) {
      name = profesionesTemp[0].nombre.toLowerCase();
    }
    return name;
  }

  addNewRow(formArrayName: string, data?: any) {
    const control = <FormArray>this.form.get(formArrayName);

    // console.log('data', data);

    // add new formGroup
    if (formArrayName === this.formArrayPerfiles) {
      control.push(new FormGroup({
        id: new FormControl(data && data._id ? data._id : null),
        profesionId: new FormControl(data && data.profesionId ? data.profesionId : null),
        descripcion: new FormControl(data && data.perfil ? data.perfil : ''),
        data: new FormControl({})
      }));

      if (data && data.isNew) {
        // Habilitamos el accordion
        this.setCurrentAccordion(control.length - 1);
        this.accordionState = true;
      }
    }
  }

  deleteRow(index: number, formArrayName: string) {
    const control = <FormArray>this.form.controls[formArrayName];
    control.removeAt(index);
  }

  setCurrentAccordion(index: number) {
    this.currentAccordion = index;
  }

  tabChanged(tabChangeEvent: MatTabChangeEvent): void {
    this.tapActiveIndex = tabChangeEvent.index;
  }

  getEstudiosTotalRegistrosEvent(data: any) {
    this.form.value.perfiles.forEach((perfil: any) => {
      if (perfil.id === data.perfilId) {
        perfil.data.estudios = data.cantidadRegistros;
      }
    });
  }

  getEstudiosComplementariosTotalRegistrosEvent(data: any) {
    this.form.value.perfiles.forEach((perfil: any) => {
      if (perfil.id === data.perfilId) {
        perfil.data.estudiosComplementarios = data.cantidadRegistros;
      }
    });
  }

  getExperienciasTotalRegistrosEvent(data: any) {
    this.form.value.perfiles.forEach((perfil: any) => {
      if (perfil.id === data.perfilId) {
        perfil.data.experiencias = data.cantidadRegistros;
      }
    });
  }

}
