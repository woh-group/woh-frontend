import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, Validators, FormGroup, AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';

import { Config } from '../../_configs/config';
import { ValidatorComponentUtil } from '../../_helpers/_util/validator-component-util';
import { JwtUtil } from '../../_helpers/_util/jwt-util';
import { UsuarioMessageUtil } from '../../_services/_service-util/usuario-message-util';

import { AutenticacionService } from '../../_services/autenticacion.service';
import { UsuarioService } from '../../_services/usuario.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  @ViewChild('btnCloseModal') btnCloseModal: ElementRef;

  constructor(private formBuilder: FormBuilder,
    private element: ElementRef,
    private router: Router,
    private usuarioService: UsuarioService,
    private autenticacionService: AutenticacionService) {
  }

  ngOnInit() {
    this.initalizeForm();
  }

  initalizeForm() {
    this.form = this.formBuilder.group({
      username: ['', [Validators.required, Validators.minLength(2), Validators.pattern(Config.emailValido)]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  setSrcImgPerfil(id: string): void {
    const currentDate = new Date();
    this.usuarioService.setSrcImgPerfil(id, currentDate.getMinutes(), currentDate.getSeconds());
  }

  login() {
    if (this.form.valid) {
      this.autenticacionService.login(this.form.value).subscribe((response: any) => {
        // console.log('response:', JSON.stringify(response));
        if (response && response.status === 201) {
          this.closeModal();

          // Obtenemos los datos del token
          const tokenInfo = JwtUtil.decodeToken(response.body.accessToken);

          // Obtenemos los datos del usuario
          this.usuarioService.getById(tokenInfo.sub).subscribe((responseUsuario: any) => {
            // console.log('responseUsuario:', JSON.stringify(responseUsuario));
            if (responseUsuario && responseUsuario.status === 200) {
              // Validamos los estados del usuario
              switch (responseUsuario.body.estadoId) {
                case Config.estadoUsuarioActivo: { // Usuario activo
                  // cargamos la imagen del usuario al menú
                  this.setSrcImgPerfil(responseUsuario.body._id);
                  // validamos si el usuario tiene un cambio de contrasena activo
                  if (responseUsuario.body.cambiarContrasena) {
                    this.autenticacionService.logout();
                    this.router.navigate(['/usuario/restablecerContrasena', responseUsuario.body._id]);
                  }

                  // validamos si el usuario es un rol de profesional
                  if (responseUsuario.body.rol === Config.rolProfesional) {
                    this.router.navigate(['/preguntas/search']);
                  }

                  // Validamos que no este en una ruta de registro
                  if (this.router.url.includes('/registro')) {
                    this.router.navigate(['/home']);
                  }

                  break;
                }
                case Config.estadoUsuarioBloqueado: { // Usuario bloqueado
                  UsuarioMessageUtil.showMessage(responseUsuario.body.estadoId);
                  this.autenticacionService.logout();
                  // this.router.navigate(['/login']);
                  break;
                }
                default: { // Usuario activo
                  // this.router.navigate(['/home']);
                  this.autenticacionService.logout();
                  break;
                }
              }
            }
          });
        }
      });
    }
  }

  isFieldInvalid(fieldControl: AbstractControl): boolean {
    return ValidatorComponentUtil.isFieldInvalid(fieldControl);
  }

  getFieldError(fieldControl: AbstractControl, fieldType?: string): string {
    return ValidatorComponentUtil.getFieldError(fieldControl, fieldType);
  }

  isRequired(fieldControl: AbstractControl): boolean {
    return ValidatorComponentUtil.isRequired(fieldControl);
  }

  closeModal() {
    this.btnCloseModal.nativeElement.click();
  }

}
