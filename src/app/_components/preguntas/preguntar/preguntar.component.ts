import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-preguntar',
  templateUrl: './preguntar.component.html',
  styleUrls: ['./preguntar.component.css']
})
export class PreguntarComponent implements OnInit {

  searchBox: string;
  @ViewChild('btnCloseModal') btnCloseModal: ElementRef;
  @ViewChild('inputPreguntarModal') inputPreguntarModal: ElementRef;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  preguntar(pregunta: string) {
    if (pregunta) {
      this.router.navigate(['/preguntas/search', pregunta]);
      this.closeModal();
      this.inputPreguntarModal.nativeElement.value = '';
    }
  }
  closeModal() {
    this.btnCloseModal.nativeElement.click();
  }

}
