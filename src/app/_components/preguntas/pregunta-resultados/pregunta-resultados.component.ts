import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { WordsUtil } from '../../../_helpers/_util/words-util';
import { JwtUtil } from '../../../_helpers/_util/jwt-util';

import { AutenticacionService } from '../../../_services/autenticacion.service';

@Component({
  selector: 'app-pregunta-resultados',
  templateUrl: './pregunta-resultados.component.html',
  styleUrls: ['./pregunta-resultados.component.css']
})
export class PreguntaResultadosComponent implements OnInit {

  // Paginador
  totalPreguntas = 0;
  words = '';
  pregunta = '';
  sort = '';
  preguntasConRespuesta = false;
  preguntasSinRespuesta = false;
  usuarioId = '';
  categoriaId = '';
  isHome = false;
  @Output() eventIsOk = new EventEmitter();

  // types
  @Input() type = '';
  type1 = 'TYPE-1'; // Preguntas sin respuesta
  type2 = 'TYPE-2'; // Preguntas busqueda
  type3 = 'TYPE-3'; // Preguntas busqueda por categoria
  type4 = 'TYPE-4'; // Preguntas x usuario

  constructor(private route: ActivatedRoute,
    private router: Router,
    private autenticacionService: AutenticacionService) { }

  ngOnInit() {
    if (this.router.url.includes('/home')) {
      this.isHome = true;
    } else {
      window.scrollTo(0, 0);
    }

    // Obtenemos el scope
    if (!this.type) {
      this.type = this.route.snapshot.data.scope;
    }

    switch (this.type) {
      case this.type1: {
        this.preguntasSinRespuesta = true;
        break;
      }
      case this.type2: {
        this.sort = '-visitas';
        this.route.params.subscribe(params => {
          this.pregunta = params['pregunta'];
          if (this.pregunta) {
            this.pregunta = this.pregunta.trim();
            this.words = WordsUtil.getPalabrasPrincipales(this.pregunta);
            this.preguntasConRespuesta = true;
          }
        });
        break;
      }
      case this.type3: {
        this.sort = '-visitas';
        this.preguntasConRespuesta = true;
        const idParam = this.route.snapshot.params['id'];
        if (idParam && idParam !== undefined) {
          this.categoriaId = idParam;
        }
        break;
      }
      case this.type4: {
        // Validamos si esta logueado
        this.autenticacionService.currentUser.subscribe(response => {
          if (response && response.accessToken) {

            // Obtenemos los datos del token
            const tokenInfo = JwtUtil.decodeToken(response.accessToken);
            this.usuarioId = tokenInfo.sub;
          }
        });
        break;
      }
    }
  }

  getEventGetCategoriaId(val: any) {
    if (val) {
      this.categoriaId = val.id;
      this.router.navigate(['/preguntas/search/category', this.categoriaId]);
    } else {
      this.categoriaId = '';
    }
  }

  getEventGetCategoriasIsOk(isOk: boolean) {
    this.eventIsOk.emit(isOk);
  }

  getEventCantidadPreguntas(cantidadPreguntas: number) {
    this.totalPreguntas = cantidadPreguntas;
  }

}
