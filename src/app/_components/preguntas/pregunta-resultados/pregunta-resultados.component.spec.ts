import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreguntaResultadosComponent } from './pregunta-resultados.component';

describe('PreguntaResultadosComponent', () => {
  let component: PreguntaResultadosComponent;
  let fixture: ComponentFixture<PreguntaResultadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreguntaResultadosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreguntaResultadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
