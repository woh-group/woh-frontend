import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreguntasRankingComponent } from './preguntas-ranking.component';

describe('PreguntasRankingComponent', () => {
  let component: PreguntasRankingComponent;
  let fixture: ComponentFixture<PreguntasRankingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreguntasRankingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreguntasRankingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
