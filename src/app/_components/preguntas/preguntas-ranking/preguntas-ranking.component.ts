import { Component, OnInit } from '@angular/core';

import { StringUtil } from '../../../_helpers/_util/string-util';

import { PreguntaService } from '../../../_services/preguntas/pregunta.service';

import { Pregunta } from '../../../_entities/preguntas/pregunta';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-preguntas-ranking',
  templateUrl: './preguntas-ranking.component.html',
  styleUrls: ['./preguntas-ranking.component.css']
})
export class PreguntasRankingComponent implements OnInit {

  preguntasMasVistas: Pregunta[] = [];
  preguntasUltimasRespuestas: Pregunta[] = [];

  pageSize = 5;
  currentPage = 1;

  filterField = '-visitas';
  lengthTextoPregunta = 100;
  lengthTextoRespuesta = 100;
  fechaActual = new Date();
  currentDate = new Date();
  routeFiles = environment.endpointBucketFiles;

  constructor(private preguntaService: PreguntaService) { }

  ngOnInit() {
    this.getMasVistas(this.pageSize, this.currentPage, '-visitas');
    this.getMasUltimasRespuestas(this.pageSize, this.currentPage, '-fechaUltimaRespuesta');
  }

  getMasVistas(pageSize: number, currentPage: number, sort: string) {
    // Params
    const params = new Map<string, any>();
    params.set('sort', sort);
    params.set('page[number]', currentPage);
    params.set('page[size]', pageSize);
    params.set('fechaUltimaRespuesta[ne]', null);

    this.preguntaService.getAll(params).subscribe((response: any) => {
      // console.log('response', response);
      if (response && response.status === 200) {
        this.preguntasMasVistas = response.body;
      }
    });
  }

  getMasUltimasRespuestas(pageSize: number, currentPage: number, sort: string) {
    // Params
    const params = new Map<string, any>();
    params.set('sort', sort);
    params.set('page[number]', currentPage);
    params.set('page[size]', pageSize);
    params.set('fechaUltimaRespuesta[ne]', null);

    this.preguntaService.getAll(params).subscribe((response: any) => {
      // console.log('response', response);
      if (response && response.status === 200) {
        this.preguntasUltimasRespuestas = response.body;
      }
    });
  }

  cortarTexto(texto: string, longitud: number) {
    return StringUtil.cortarTexto(texto, longitud);
  }

  calcularTiempo(fechaInicio: Date, fechaFin: Date) {
    if (fechaInicio) {
      const segundos = Math.floor((fechaFin.getTime() - new Date(fechaInicio).getTime()) / (1000));

      const semanas = Math.floor(segundos / 604800);
      const dias = Math.floor(segundos / 86400);
      const horas = Math.floor(segundos / 3600);
      const minutos = Math.floor(segundos / 60);
      // return semanas + ' semana ' + dias + ' dia ' + horas + ' hora ' + minutos + ' minutos ';

      if (semanas > 0) {
        return semanas + ' semanas';
      } else if (dias > 0) {
        return dias + ' días';
      } else if (horas > 0) {
        return horas + ' horas';
      } else if (minutos > 0) {
        return minutos + ' minutos';
      }
      return segundos + ' segundos';
    }
    return 'NaN';
  }

}
