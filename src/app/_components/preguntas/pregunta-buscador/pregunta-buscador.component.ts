import { Component, OnInit, Input } from '@angular/core';
import { Router} from '@angular/router';

@Component({
  selector: 'app-pregunta-buscador',
  templateUrl: './pregunta-buscador.component.html',
  styleUrls: ['./pregunta-buscador.component.css']
})
export class PreguntaBuscadorComponent implements OnInit {

  @Input() isHome: boolean;
  @Input() pregunta: string;
  searchBox: string;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  preguntar(pregunta: string) {
    if (this.isValidQuestion(pregunta)) {
      this.router.navigate(['/preguntas/search', pregunta]);
    }
  }

  isValidQuestion(pregunta: string) {
    if (pregunta && pregunta.trim().length > 2) {
      return true;
    }
    return false;
  }

}
