import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreguntaBuscadorComponent } from './pregunta-buscador.component';

describe('PreguntaBuscadorComponent', () => {
  let component: PreguntaBuscadorComponent;
  let fixture: ComponentFixture<PreguntaBuscadorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreguntaBuscadorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreguntaBuscadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
