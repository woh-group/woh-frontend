import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { CategoriaPreguntaService } from '../../../_services/preguntas/categoria.service';

import { CategoriaPregunta } from '../../../_entities/preguntas/categoriaPregunta';

@Component({
  selector: 'app-pregunta-select-categoria',
  templateUrl: './pregunta-select-categoria.component.html',
  styleUrls: ['./pregunta-select-categoria.component.css']
})
export class PreguntaSelectCategoriaComponent implements OnInit {

  selected: any;
  categorias: CategoriaPregunta[] = [];
  categoriasSelect: CategoriaPregunta[] = [];

  categoriaTodas = {
    id: 'allCategories',
    descripcion: 'Todas las categorías',
    categoriaPadreId: ''
  };

  categoriaOtro = {
    id: '0.0',
    descripcion: 'Otro',
    categoriaPadreId: ''
  };

  @Input() showBreadcrumb = false;
  @Input() showCategoriaOtro = false;
  @Input() categoriaPadreId: string;
  @Output() eventCategoria = new EventEmitter();
  @Output() eventIsOk = new EventEmitter();

  constructor(private categoriaPreguntaService: CategoriaPreguntaService) { }

  ngOnInit() {
    this.getCategorias(this.categoriaPadreId);
  }

  getCategorias(categoriaPadreId?: string) {
    this.categoriaPreguntaService.getCategoriasCache().then((categoriasCache: CategoriaPregunta[]) => {
      // console.log('categoriasCache array:', categoriasCache);

      if (categoriasCache && categoriasCache.length > 0) {
        // Validamos si estamos forzando una categoria por defecto
        if (this.categorias.length === 0 && categoriaPadreId) {
          const categoriasTemp = categoriasCache.filter(item => item.id === categoriaPadreId);
          if (categoriasTemp.length > 0) {
            const categoriaTemp = categoriasTemp[0];
            // console.log('categoriasTemp', categoriaTemp);
            // Agregamos los padres
            if (categoriaTemp.padres) {
              categoriaTemp.padres.reverse().forEach(item => {
                this.categoriasSelect.push(item);
              });
            }

            this.selected = categoriaTemp;
            this.sendEventCategoria();
            this.categoriasSelect.push(categoriaTemp);
          } else {
            categoriaPadreId = '';
          }
        }

        // console.log('categoriasCache', categoriasCache);
        // this.categorias = categoriasCache.filter(item => item.categoriaPadreId === '');

        if (categoriaPadreId) {
          this.categorias = categoriasCache.filter(item => item.categoriaPadreId === categoriaPadreId);
          const index = this.categoriasSelect.map(e => e.id).indexOf(categoriaPadreId);
          if (index !== -1) {
            this.categoriasSelect = this.categoriasSelect.slice(0, index + 1);
          }
          this.sendEventCategoria();
        } else {
          this.categorias = categoriasCache.filter(item => item.categoriaPadreId === '');
          // Limpiamos el array
          this.categoriasSelect = [];
          this.sendEventCategoria();
        }
        this.eventIsOk.emit(true);
      } else {
        this.eventIsOk.emit(false);
      }
    });
  }

  onSelectCategoria(obj: CategoriaPregunta): void {
    if (obj) {
      this.selected = obj;
      this.getCategorias(obj.id);
      this.categoriasSelect.push(obj);
    }
  }

  sendEventCategoria() {
    // console.log('this.selected', this.selected);
    if (this.selected) {
      // Validamos si es la ultima categoria
      if (this.categorias.length === 0) {
        this.eventCategoria.emit({
          final: true,
          id: this.selected.id,
          descripcion: this.selected.descripcion
        });
      } else {
        this.eventCategoria.emit({
          final: false,
          id: this.selected.id,
          descripcion: this.selected.descripcion
        });
      }
    } else {
      this.eventCategoria.emit('');
    }
  }

}
