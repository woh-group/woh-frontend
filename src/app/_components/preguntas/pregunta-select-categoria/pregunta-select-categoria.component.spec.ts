import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreguntaSelectCategoriaComponent } from './pregunta-select-categoria.component';

describe('PreguntaSelectCategoriaComponent', () => {
  let component: PreguntaSelectCategoriaComponent;
  let fixture: ComponentFixture<PreguntaSelectCategoriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreguntaSelectCategoriaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreguntaSelectCategoriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
