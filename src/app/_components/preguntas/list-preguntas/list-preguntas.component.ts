import { Component, OnInit, OnChanges, SimpleChanges, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { StringUtil } from '../../../_helpers/_util/string-util';

import { PreguntaService } from '../../../_services/preguntas/pregunta.service';
import { CategoriaPreguntaService } from '../../../_services/preguntas/categoria.service';

import { Pregunta } from '../../../_entities/preguntas/pregunta';
import { CategoriaPregunta } from '../../../_entities/preguntas/categoriaPregunta';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-list-preguntas',
  templateUrl: './list-preguntas.component.html',
  styleUrls: ['./list-preguntas.component.css']
})
export class ListPreguntasComponent implements OnInit, OnChanges {

  // Paginador
  pageSize = 5;
  totalPages = 1;
  totalPreguntas = 0;
  currentPage = 1;
  init = true;
  showButtonActualizar = false;
  routeFiles = environment.endpointBucketFiles;

  // Otros parametros
  @Input() words = '';
  @Input() preguntasConRespuesta = false;
  @Input() preguntasSinRespuesta = false;
  @Input() categoriaId = '';
  @Input() usuarioId = null;
  @Input() sort = null;
  @Output() eventCantidadPreguntas = new EventEmitter();

  preguntas: Pregunta[] = [];
  lengthTexto = 150;

  constructor(private route: ActivatedRoute,
    private preguntaService: PreguntaService,
    private categoriaPreguntaService: CategoriaPreguntaService) { }

  ngOnInit() {
    this.showButtonActualizar = this.preguntasSinRespuesta;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (!this.init) {
      this.getAll(this.currentPage);
      this.count();
    }
  }

  actualizarPreguntas() {
    this.currentPage = 1;
    this.getAll(this.currentPage);
    this.count();
  }

  getAll(currentPage: number) {
    // Params
    const params = new Map<string, any>();
    if (this.preguntasConRespuesta) {
      params.set('fechaUltimaRespuesta[ne]', null);
    }
    if (this.preguntasSinRespuesta) {
      params.set('fechaUltimaRespuesta', null);
    }
    if (this.words) {
      params.set('pregunta[contains]', this.words);
    }
    if (this.categoriaId && this.categoriaId !== 'allCategories') {
      params.set('categoriaId', this.categoriaId);
    }
    if (this.usuarioId) {
      params.set('usuarioId', this.usuarioId);
    }
    if (this.sort) {
      params.set('sort', this.sort);
    }
    params.set('page[number]', currentPage);
    params.set('page[size]', this.pageSize);

    this.preguntaService.getAll(params).subscribe((response: any) => {
      if (response && response.status === 200) {
        this.preguntas = response.body;

        // Asignamos la categoria a la pregunta
        if (this.preguntas) {
          this.categoriaPreguntaService.getCategoriasMapCache().then((categoriasCache: Map<string, CategoriaPregunta>) => {

            this.preguntas.map(obj => {
              const categoriaTemp = categoriasCache.get(obj.categoriaId);
              if (categoriaTemp) {
                obj.categoriaNombre = categoriaTemp.descripcion;
              }
            });
          });
        }

      }
    });
  }

  count() {
    // Params
    const params = new Map<string, any>();
    if (this.preguntasConRespuesta) {
      params.set('fechaUltimaRespuesta[ne]', null);
    }
    if (this.preguntasSinRespuesta) {
      params.set('fechaUltimaRespuesta', null);
    }
    if (this.words) {
      params.set('pregunta[contains]', this.words);
    }
    if (this.categoriaId && this.categoriaId !== 'allCategories') {
      params.set('categoriaId', this.categoriaId);
    }
    if (this.usuarioId) {
      params.set('usuarioId', this.usuarioId);
    }

    this.preguntaService.count(params).subscribe((response: any) => {
      if (response) {
        this.totalPreguntas = response;
        // Mapeamos la paginacion
        this.totalPages = Math.ceil(this.totalPreguntas / this.pageSize);
      } else {
        this.totalPreguntas = 0;
        this.totalPages = 1;
      }
      this.eventCantidadPreguntas.emit(this.totalPreguntas);
    });
  }

  cortarTexto(texto: string) {
    return StringUtil.cortarTexto(texto, this.lengthTexto);
  }

  getCurrentPage(currentPage: number) {
    this.currentPage = currentPage;
    this.getAll(this.currentPage);
    this.count();
    this.init = false;
  }

}
