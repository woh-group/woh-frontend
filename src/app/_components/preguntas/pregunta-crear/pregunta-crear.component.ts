import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormBuilder, Validators, FormGroup, AbstractControl } from '@angular/forms';

import { JwtUtil } from '../../../_helpers/_util/jwt-util';
import { DateUtil } from '../../../_helpers/_util/date-util';
import { StringUtil } from '../../../_helpers/_util/string-util';
import { PersonaUtil } from '../../../_helpers/_util/persona-util';
import { ValidatorComponentUtil } from '../../../_helpers/_util/validator-component-util';
import { NotifylUtil } from '../../../_helpers/_util/notify-util';

import { AutenticacionService } from '../../../_services/autenticacion.service';
import { PreguntaService } from '../../../_services/preguntas/pregunta.service';

import { CategoriaPregunta } from '../../../_entities/preguntas/categoriaPregunta';
import { Persona } from '../../../_entities/persona';
import { Pregunta } from '../../../_entities/preguntas/pregunta';
import { Config } from '../../../_configs/config';
import { finalize } from 'rxjs/operators';
import { LoaderService } from '../../../_services/_service-util/loader.service';

@Component({
  selector: 'app-pregunta-crear',
  templateUrl: './pregunta-crear.component.html',
  styleUrls: ['./pregunta-crear.component.css']
})
export class PreguntaCrearComponent implements OnInit {

  loggedIn = false;
  private usuarioId = '';
  form: FormGroup;

  preguntaTemp = '';
  persona: Persona;
  pregunta: Pregunta;

  isFinalCategoria = false;
  categoriaSelected: CategoriaPregunta;

  fechaMinimaPregunta = Config.maxDateParaPregunta;

  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private autenticacionService: AutenticacionService,
    private loaderService: LoaderService,
    private preguntaService: PreguntaService) {

    // Validamos si esta logueado
    this.autenticacionService.currentUser.subscribe(response => {
      // console.log('response', response);
      if (response && response.accessToken) {
        this.loggedIn = true;

        // Obtenemos los datos del token
        const tokenInfo = JwtUtil.decodeToken(response.accessToken);
        this.usuarioId = tokenInfo.sub;

        // Obtenemos los datos de la persona
        this.autenticacionService.currentPerson.subscribe(responsePersona => {
          // console.log('responsePersona', responsePersona);
          if (responsePersona) {
            this.persona = responsePersona;
            // Asignamos el validador a los campos requeridos
            if (this.form) {
              this.addFiledsValidators();
            }
          }
        });
      } else {
        this.loggedIn = false;
      }
    }, error => {
      this.loggedIn = false;
    });
  }

  ngOnInit() {
    this.preguntaTemp = this.route.snapshot.params['pregunta'];
    this.initalizeForm();
  }

  getRequieredFields(loggedIn: boolean) {

    if (loggedIn) {
      return {
        'pregunta': true,
        'informacionAdicional': false,
        'email': false,
        'sexo': false,
        'fechaNacimiento': false
      };
    } else {
      return {
        'pregunta': true,
        'informacionAdicional': false,
        'email': true,
        'sexo': true,
        'fechaNacimiento': true
      };
    }
  }

  initalizeForm() {
    this.form = this.formBuilder.group({
      pregunta: this.preguntaTemp,
      informacionAdicional: '',
      email: null,
      sexo: null,
      fechaNacimiento: null
    });

    // Asignamos el validador a los campos requeridos
    this.addFiledsValidators();
  }

  addFiledsValidators() {
    const fields = this.getRequieredFields(this.loggedIn);
    Object.keys(fields).map(key => {
      if (fields[key]) {
        if (key === 'email') {
          this.form.get(key).setValidators([Validators.compose([Validators.required, Validators.pattern(Config.emailValido)])]);
        } else {
          this.form.get(key).setValidators([Validators.compose([Validators.required])]);
        }
      } else {
        this.form.controls[key].clearValidators();
      }
      this.form.controls[key].updateValueAndValidity();
    });
  }

  isFieldInvalid(fieldControl: AbstractControl): boolean {
    return ValidatorComponentUtil.isFieldInvalid(fieldControl);
  }

  getFieldError(fieldControl: AbstractControl, fieldType?: string): string {
    return ValidatorComponentUtil.getFieldError(fieldControl, fieldType);
  }

  isRequired(fieldControl: AbstractControl): boolean {
    return ValidatorComponentUtil.isRequired(fieldControl);
  }

  preguntar() {
    this.pregunta = new Pregunta();
    if (this.loggedIn) {
      if (this.persona) {
        const edad = DateUtil.calculateAge(this.persona.fechaNacimiento);
        this.pregunta.usuarioId = this.usuarioId;
        this.pregunta.genero = PersonaUtil.getGeneroFromSexoAndAge(this.persona.sexo, edad);
        this.pregunta.edad = edad;
      } else {
        const message = 'Ocurrió un error al realizar la pregunta';
        NotifylUtil.showNotification(NotifylUtil.FROM_TOP, NotifylUtil.ALIGN_RIGHT, NotifylUtil.TYPE_NOTIFY_ERROR, message);
        console.error(`[${message}]`, `[persona is null]`);
      }
    } else {
      const edad = DateUtil.calculateAge(this.form.get('fechaNacimiento').value);
      this.pregunta.usuarioId = this.form.get('email').value;
      this.pregunta.genero = PersonaUtil.getGeneroFromSexoAndAge(this.form.get('sexo').value, edad);
      this.pregunta.edad = edad;
    }
    this.pregunta.pregunta = this.form.get('pregunta').value.trim();
    // this.pregunta.pregunta = StringUtil.colocarPuntoAlFinal(this.pregunta.pregunta);
    this.pregunta.informacionAdicional = this.form.get('informacionAdicional').value;
    if (this.pregunta.usuarioId) {
      this.loaderService.display(true);
      this.preguntaService.add(this.pregunta).pipe(finalize(() => {
        this.loaderService.display(false);
      })).subscribe((response: any) => {
        // console.log('response', response);
        if (response && response.status === 201) {
          this.router.navigate(['/preguntas/detail/' + response.body._id]);
        }
      });
    }

    // // Confirmamos la pregunta
    // ModalUtil.showModalConfirm('¿Está seguro que desea realizar esta pregunta?', ModalUtil.TYPE_MODAL_WARNING, ModalUtil.BTN_NAME_ACEPTAR).then((modalResp) => {
    //   if (modalResp.value) {

    //   }
    // });
  }

  getEventIsFinalCategoria(val: boolean) {
    this.isFinalCategoria = val;
  }

  getEventCategoriaSelected(val: CategoriaPregunta) {
    this.categoriaSelected = val;
  }

  goBack() {
    this.router.navigate(['/preguntas/search/' + this.preguntaTemp]);
    // this.location.back();
  }

  isValidQuestion(pregunta: string) {
    if (pregunta && pregunta.trim().length > 2) {
      return true;
    }
    return false;
  }

}
