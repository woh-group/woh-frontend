import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

import { Config } from '../../../_configs/config';

import { JwtUtil } from '../../../_helpers/_util/jwt-util';
import { NotifylUtil } from '../../../_helpers/_util/notify-util';
import { ModalUtil } from '../../../_helpers/_util/modal-util';

import { AutenticacionService } from '../../../_services/autenticacion.service';
import { PreguntaService } from '../../../_services/preguntas/pregunta.service';
import { RespuestaService } from '../../../_services/preguntas/respuesta.service';
import { CategoriaPreguntaService } from 'src/app/_services/preguntas/categoria.service';

import { CategoriaPregunta } from '../../../_entities/preguntas/categoriaPregunta';
import { Pregunta } from '../../../_entities/preguntas/pregunta';
import { Respuesta } from '../../../_entities/preguntas/respuesta';
import { Persona } from '../../../_entities/persona';
import { LoaderService } from 'src/app/_services/_service-util/loader.service';
import { finalize } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-pregunta-detalle',
  templateUrl: './pregunta-detalle.component.html',
  styleUrls: ['./pregunta-detalle.component.css']
})
export class PreguntaDetalleComponent implements OnInit {

  // Constantes
  private messageNotFound = 'La pregunta que está buscando no existe.';
  private constVisitas = 'visitas';
  private constLikes = 'likes';
  private constDislikes = 'dislikes';

  private loggedIn = false;
  private rol = 0;
  private preguntaId = '';
  private usuarioId = '';

  pregunta: Pregunta;
  respuesta: Respuesta;
  categoria: CategoriaPregunta;
  persona: Persona;
  form: FormGroup;
  currentDate = new Date();
  routeFiles = environment.endpointBucketFiles;
  respuestaDefault = Config.respuestaDefault;

  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private autenticacionService: AutenticacionService,
    private preguntaService: PreguntaService,
    private respuestaService: RespuestaService,
    private categoriaPreguntaService: CategoriaPreguntaService,
    private loaderService: LoaderService) {

    // Validamos si esta logueado
    this.autenticacionService.currentUser.subscribe(response => {
      if (response && response.accessToken) {
        this.loggedIn = true;

        // Obtenemos los datos del token
        const tokenInfo = JwtUtil.decodeToken(response.accessToken);
        this.usuarioId = tokenInfo.sub;
        this.rol = tokenInfo.rol;

        // Obtenemos los datos de la persona
        this.autenticacionService.currentPerson.subscribe(responsePersona => {
          // console.log('responsePersona', responsePersona);
          if (responsePersona) {
            this.persona = responsePersona;
          }
        });

      } else {
        this.loggedIn = false;
      }
    }, error => {
      this.loggedIn = false;
    });
  }

  ngOnInit() {
    this.preguntaId = this.route.snapshot.params['id'];

    // Obtenemos la pregunta
    this.loaderService.display(true);
    this.preguntaService.getById(this.preguntaId).pipe(finalize(() => {
      this.loaderService.display(false);
    })).subscribe((response: any) => {
      if (response && response.status === 200) {
        this.pregunta = response.body;

        // Obtenemos la categoria
        if (this.pregunta.categoriaId) {
          // console.log('categoria pregunta', this.pregunta.categoriaId);
          this.categoriaPreguntaService.getCategoriasCache().then((categoriasCache: CategoriaPregunta[]) => {
            // console.log('categoriasCache', categoriasCache);
            this.categoria = categoriasCache.filter(item => item.id === this.pregunta.categoriaId)[0];
            // console.log('categoria obj', this.categoria);
          });
        }

        if (this.pregunta.informacionAdicional) {
          this.pregunta.informacionAdicionalArray = this.pregunta.informacionAdicional.split('\n');
        }

        if (this.pregunta.respuestas) {
          this.pregunta.respuestas.map(respuesta => {
            respuesta.respuestaArray = respuesta.respuesta.split('\n');
          });
        } else {
          // Inicializamos las preguntas para evitar errores de nullpointer
          this.pregunta.respuestas = [];
        }

        // Actualizamos las visitas
        this.increaseVisitas();
      } else {
        ModalUtil.showModal(this.messageNotFound, false, ModalUtil.TYPE_MODAL_ERROR).then((modalResp: any) => {
          if (modalResp.value) {
            this.location.back();
          }
        });
      }
    });

    this.initalizeForm();
  }

  initalizeForm() {
    this.form = this.formBuilder.group({
      respuesta: ['', Validators.required]
    });
  }

  increaseVisitas() {
    this.preguntaService.increase(this.preguntaId, this.constVisitas, 1).subscribe((response: any) => {
      // console.log('response increaseVisitas', response);
      if (response && response.status !== 200) {
        const message = 'Parece que ocurrió un error al actualizar las visitas';
        console.error(`[ID: ${response.headers.get(Config.headerRequestId)}]`, `[${response.status} - ${response.statusText}]`, `[${response.body}]`, `[${message}]`);
      }
    });
  }

  responder() {
    this.respuesta = new Respuesta();
    this.respuesta.usuarioId = this.usuarioId;
    this.respuesta.respuesta = this.form.get('respuesta').value;

    if (this.pregunta.categoriaId) {

      // Actualizamos la categoria de la pregunta
      this.loaderService.display(true);
      this.preguntaService.update(this.preguntaId, this.pregunta).pipe(finalize(() => {
        this.loaderService.display(false);
      })).subscribe((responseUpdatePregunta: any) => {
        // console.log('responseUpdatePregunta', responseUpdatePregunta);
        if (responseUpdatePregunta && responseUpdatePregunta.status === 200) {

          // Agregamos la respuesta
          this.loaderService.display(true);
          this.respuestaService.add(this.preguntaId, this.respuesta).pipe(finalize(() => {
            this.loaderService.display(false);
          })).subscribe((responseAddRespuesta: any) => {
            if (responseAddRespuesta && responseAddRespuesta.status === 201) {
              responseAddRespuesta.body.respuestaArray = responseAddRespuesta.body.respuesta.split('\n');

              // Guardamos la respuesta en el array de respuestas para no llamar nuevamente el servicio
              const respuestaNew = responseAddRespuesta.body;
              respuestaNew.persona = this.persona;
              this.pregunta.respuestas.push(respuestaNew);
            }
          });
        }
      });
    }
  }

  like(respuesta: Respuesta) {
    this.respuestaService.increase(this.preguntaId, respuesta._id, this.constLikes, 1).subscribe((response: any) => {
      if (response && response.status === 200) {
        respuesta.likes++;
        respuesta.checkUtil = true;
      } else {
        NotifylUtil.showNotification(NotifylUtil.FROM_TOP, NotifylUtil.ALIGN_RIGHT, NotifylUtil.TYPE_NOTIFY_ERROR, NotifylUtil.MESSAGE_ERROR_GENERICO, null, response.headers.get(Config.headerRequestId));
      }
    });
  }

  dislike(respuesta: Respuesta) {
    this.respuestaService.increase(this.preguntaId, respuesta._id, this.constDislikes, 1).subscribe((response: any) => {
      if (response && response.status === 200) {
        respuesta.dislikes++;
        respuesta.checkUtil = true;
      } else {
        NotifylUtil.showNotification(NotifylUtil.FROM_TOP, NotifylUtil.ALIGN_RIGHT, NotifylUtil.TYPE_NOTIFY_ERROR, NotifylUtil.MESSAGE_ERROR_GENERICO, null, response.headers.get(Config.headerRequestId));
      }
    });
  }

  getEventGetCategoria(val: any) {
    if (val && val.final) {
      this.pregunta.categoriaId = val.id;

      // Mapeamos la categoria
      if (!this.categoria) {
        this.categoria = new CategoriaPregunta();
        this.categoria.id = val.id;
        this.categoria.descripcion = val.descripcion;
      }
      // this.categoria = this.categoriaPreguntaService.categoriasMap.get(val.id);
    } else {
      this.pregunta.categoriaId = '';
    }
  }

  deletePregunta() {
    if (this.pregunta._id) {
      ModalUtil.showModalConfirm(ModalUtil.MSJ_CONFIRM_ELIMINAR, true, ModalUtil.TYPE_MODAL_WARNING, ModalUtil.BTN_NAME_ELIMINAR).then((result) => {
        if (result.value) {
          console.log('Eliminado', this.pregunta._id);
          this.preguntaService.delete(this.preguntaId).pipe(finalize(() => {
            this.loaderService.display(false);
          })).subscribe((response: any) => {
            if (response && response.status === 200) {
              NotifylUtil.showNotification(NotifylUtil.FROM_TOP, NotifylUtil.ALIGN_RIGHT, NotifylUtil.TYPE_NOTIFY_SUCCESS, 'Pregunta eliminada con éxito');
              this.router.navigate(['/home']);
            }
          });
        }
      });
    }
  }

}
