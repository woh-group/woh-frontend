import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { finalize } from 'rxjs/operators';

import { Config } from '../../../_configs/config';

import { LoaderService } from 'src/app/_services/_service-util/loader.service';
import { UsuarioService } from 'src/app/_services/usuario.service';
import { PreguntaService } from '../../../_services/preguntas/pregunta.service';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {

  totalRegistros = 0;
  totalUsuariosPacientesActivos = 0;
  totalUsuariosPacientesInactivos = 0;
  totalUsuariosProfesionales = 0;

  totalPreguntas = 0;
  preguntasConRespuesta = 0;
  preguntasSinRespuesta = 0;

  perguntasAndRespuestasByYear: any[] = [];
  totalPerguntasByYear = 0;
  totalRespuestasByYear = 0;
  years: number[] = [];
  yearSelected = 2019;
  setValue() { this.yearSelected = 2019; }

  constructor(
    private loaderService: LoaderService,
    private usuarioService: UsuarioService,
    private preguntaService: PreguntaService,
  ) { }

  ngOnInit() {
    this.getPacientesActivos();
    this.getPacientesInactivos();
    this.getProfesionales();
    this.getPreguntasConRespuesta();
    this.getPreguntasSinRespuesta();
    this.getYears();
  }

  getYears() {
    const initYear = 2019;
    const currentYear = new Date().getFullYear();
    for (let index = initYear; index <= currentYear; index++) {
      this.years.push(index);
    }
    console.log(this.years);
  }

  getPacientesActivos() {
    // Params
    const params = new Map<string, any>();
    params.set('rol', Config.rolPaciente);
    params.set('estadoId', Config.estadoUsuarioActivo);

    this.usuarioService.count(params).subscribe((response: any) => {
      // console.log('response', response);
      if (response) {
        this.totalUsuariosPacientesActivos = response;
        this.totalRegistros += this.totalUsuariosPacientesActivos;
      }
    });
  }

  getPacientesInactivos() {
    // Params
    const params = new Map<string, any>();
    params.set('rol', Config.rolPaciente);
    params.set('estadoId', Config.estadoUsuarioInactivo);

    this.usuarioService.count(params).subscribe((response: any) => {
      // console.log('response', response);
      if (response) {
        this.totalUsuariosPacientesInactivos = response;
        this.totalRegistros += this.totalUsuariosPacientesInactivos;
      }
    });
  }

  getProfesionales() {
    // Params
    const params = new Map<string, any>();
    params.set('rol', Config.rolProfesional);
    params.set('estadoId', Config.estadoUsuarioActivo);

    this.usuarioService.count(params).subscribe((response: any) => {
      // console.log('response', response);
      if (response) {
        this.totalUsuariosProfesionales = response;
        this.totalRegistros += this.totalUsuariosProfesionales;
      }
    });
  }

  getPreguntasConRespuesta() {
    // Params
    const params = new Map<string, any>();
    params.set('fechaUltimaRespuesta[ne]', null);

    this.preguntaService.count(params).subscribe((response: any) => {
      console.log('getPreguntasConRespuesta', response);
      if (response) {
        this.preguntasConRespuesta = response;
        this.totalPreguntas += this.preguntasConRespuesta;
      }
    });
  }

  getPreguntasSinRespuesta() {
    // Params
    const params = new Map<string, any>();
    params.set('fechaUltimaRespuesta', null);

    this.preguntaService.count(params).subscribe((response: any) => {
      console.log('getPreguntasSinRespuesta', response);
      if (response) {
        this.preguntasSinRespuesta = response;
        this.totalPreguntas += this.preguntasSinRespuesta;
      }
    });
  }

  getPreguntasAndRespuestasByYear() {
    this.loaderService.display(true);
    this.totalPerguntasByYear = 0;
    this.totalRespuestasByYear = 0;
    this.preguntaService.getPreguntasAndRespuestasByYear(this.yearSelected).pipe(finalize(() => {
      this.loaderService.display(false);
    })).subscribe((response: any) => {
      if (response && response.status === 200) {
        this.perguntasAndRespuestasByYear = response.body;
        console.log(this.perguntasAndRespuestasByYear);
        this.perguntasAndRespuestasByYear.forEach((obj: any) => {
          this.totalPerguntasByYear += obj.preguntas;
          this.totalRespuestasByYear += obj.respuestas;
        });
      }
    });
  }

}
