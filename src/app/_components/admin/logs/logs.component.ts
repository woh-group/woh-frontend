import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Config } from 'src/app/_configs/config';
import { MatTableDataSource, MatSort, Sort } from '@angular/material';
import { TraceService } from '../../../_services/trace/trace.service';
import { Logs } from '../../../_entities/logs';
import { MatDialog} from '@angular/material/dialog';
import { LogDialog } from '../../_dialogs/logs/log.dialog';
import * as moment from 'moment';

@Component({
  selector: 'app-logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.css']
})
export class LogsComponent implements OnInit {

  form: FormGroup;
  apis = Config.apis.sort((a, b) => a.localeCompare(b));

  logs: Logs[] = [];
  // Paginador
  totalRegistros = 0;
  pageSize = 10;
  currentPage = 1;
  totalPages = 1;

  log;
  orderField = '-fecha';

  displayedColumns: string[] = ['fecha', 'tipo', 'level', 'requestId', 'username', 'metodo', 'actionDetalle'];
  dataSource = new MatTableDataSource();
  trace = 'logs';

  @ViewChild(MatSort) sort: MatSort;

  constructor(private formBuilder: FormBuilder, private traceService: TraceService, public dialog: MatDialog) { }

  ngOnInit() {
    this.initalizeForm();
    this.dataSource.sort = this.sort;
  }

  encabezados() {
    this.trace = this.form.get('trace').value;
    if (this.trace === 'logs') {
      this.displayedColumns = ['fecha', 'tipo', 'level', 'requestId', 'username', 'metodo', 'actionDetalle'];
    } else {
      this.displayedColumns = ['fecha', 'usuario', 'accion', 'requestId', 'appId', 'idRegistro', 'actionDetalle'];
    }
  }

  initalizeForm() {
    this.form = this.formBuilder.group({
      trace: [null, Validators.required],
      api: [null, Validators.required],
      fechaInicio: [new Date(), Validators.required],
      fechaFin: [new Date(), Validators.required],
      horaInicio: [],
      horaFin: [],
      requestId: [],
      username: [],
      statusCode: [],
      appId: [],
      ipCliente: [],
      ipServidor: [],
      level: [],
      metodo: [],
      accion: []
    });

  }

  getLogs(trace: any, api: any, pageSize: number, sort: string, currentPage: number,
    fechas?: string, requestId?: string, username?: string, statusCode?: string,
    appId?: string, ipCliente?: string, ipServidor?: string, level?: string, metodo?: string, accion?: string) {
    // Params
    const params = new Map<string, any>();
    params.set('page[size]', pageSize);
    params.set('page[number]', currentPage);
    params.set('sort', sort);
    if (fechas) {
      params.set('fecha[between]', fechas);
    }
    if (requestId) {
      params.set('requestId', requestId);
    }
    if (username) {
      params.set('username', username);
    }
    if (statusCode) {
      params.set('statusCode', statusCode);
    }

    if (appId) {
      params.set('appId', appId);
    }

    if (ipCliente) {
      params.set('ipCliente', ipCliente);
    }

    if (ipServidor) {
      params.set('ipServidor', ipServidor);
    }

    if (level) {
      params.set('level', level);
    }

    if (metodo) {
      params.set('metodo', metodo);
    }

    if (accion) {
      params.set('accion', accion);
    }

    this.traceService.getAll(trace, api, params).subscribe((response: any) => {
      if (response && response.status === 200) {
        this.logs = response.body;
        this.dataSource = new MatTableDataSource(this.logs);
      }
    });
  }

  count(trace: any, api: any, requestId?: string, username?: string, statusCode?: string, fechas?: string,
    appId?: string, ipCliente?: string, ipServidor?: string, level?: string, metodo?: string, accion?: string) {
    // Params
    const params = new Map<string, any>();
      if (fechas) {
        params.set('fecha[between]', fechas);
      }
      if (requestId) {
        params.set('requestId', requestId);
      }
      if (username) {
        params.set('username', username);
      }
      if (statusCode) {
        params.set('statusCode', statusCode);
      }

      if (appId) {
        params.set('appId', appId);
      }

      if (ipCliente) {
        params.set('ipCliente', ipCliente);
      }

      if (ipServidor) {
        params.set('ipServidor', ipServidor);
      }

      if (level) {
        params.set('level', level);
      }

      if (metodo) {
        params.set('metodo', metodo);
      }

      if (accion) {
        params.set('accion', accion);
      }

    this.traceService.count(trace, api, params).subscribe((response: any) => {
      if (response) {
        this.totalRegistros = response;
        // Mapeamos la paginacion
        this.totalPages = Math.ceil(this.totalRegistros / this.pageSize);
      } else {
        this.totalRegistros = 0;
        this.totalPages = 1;
      }
    });
  }

  getCurrentPage(currentPage: number) {
    if(this.form.valid) {
    this.currentPage = currentPage;
    this.getLogs(this.form.get('trace').value, this.form.get('api').value, this.pageSize, this.orderField, this.currentPage);
    }
  }

  getFilterLogs() {
    const trace = this.form.get('trace').value;
    const api = this.form.get('api').value;
    const requestId = this.form.get('requestId').value;
    const username = this.form.get('username').value;
    const statusCode = this.form.get('statusCode').value;
    const appId = this.form.get('appId').value;
    const ipCliente = this.form.get('ipCliente').value;
    const ipServidor = this.form.get('ipServidor').value;
    const level = this.form.get('level').value;
    const metodo = this.form.get('metodo').value;
    const accion = this.form.get('accion').value;

    let fechaFiltro = null;
    const fechaInicio = moment(this.form.get('fechaInicio').value).format('YYYY-MM-DD');
    const fechaFin = moment(this.form.get('fechaFin').value).format('YYYY-MM-DD');
    if (fechaInicio && fechaFin) {
      const horaInicio = this.form.get('horaInicio').value ? this.form.get('horaInicio').value + ':00-05' : '00:00:00-05';
      const horaFin = this.form.get('horaFin').value ? this.form.get('horaFin').value + ':59-05' : '23:59:59-05';
      fechaFiltro = fechaInicio + 'T' + horaInicio + ',' + fechaFin + 'T' + horaFin;
    }
    this.encabezados();
    this.getLogs(trace, api, this.pageSize, this.orderField, this.currentPage, fechaFiltro, requestId, username, statusCode, appId, ipCliente, ipServidor, level, metodo, accion);
    this.count(trace, api, requestId, username, statusCode, fechaFiltro, appId, ipCliente, ipServidor, level, metodo, accion);
  }

  verDetalle(log) {
    const dialogRef = this.dialog.open(LogDialog, {
      width: '90%',
      height: '90%',
      data: log
    });

    dialogRef.afterClosed().subscribe(result => {
      this.log = log;
    });
  }

  ordenarTabla(sort: Sort) {
    let direction = '';
    if (sort.direction === 'desc') {
      direction = '+';
    } else if (sort.direction === 'asc') {
      direction = '-';
    } else {
      direction = '';
    }
    const orderField_ = direction + sort.active;
    this.orderField = orderField_;
    this.getFilterLogs();
  }

}
