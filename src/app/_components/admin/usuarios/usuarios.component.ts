import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { finalize } from 'rxjs/operators';

import { MatTableDataSource, MatSort } from '@angular/material';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { Config } from 'src/app/_configs/config';

import { ModalUtil } from '../../../_helpers/_util/modal-util';
import { NotifylUtil } from '../../../_helpers/_util/notify-util';

import { UsuarioService } from '../../../_services/usuario.service';
import { LoaderService } from 'src/app/_services/_service-util/loader.service';

@Component({
  selector: 'app-usuario-dialog',
  templateUrl: 'usuario-dialog.html',
})
export class UsuarioDialogComponent {

  constructor(
    private usuarioService: UsuarioService,
    private loaderService: LoaderService,
    public dialogRef: MatDialogRef<UsuarioDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  restablecerContrasena(id: string): void {
    const message = '¿Está seguro que desea restablecer la contraseña?';
    ModalUtil.showModalConfirm(message, true, ModalUtil.TYPE_MODAL_QUESTION, ModalUtil.BTN_NAME_ACEPTAR).then((modalResp) => {
      if (modalResp.value) {
        this.loaderService.display(true);
        this.usuarioService.restablecerContrasena(id).pipe(finalize(() => {
          this.loaderService.display(false);
        })).subscribe((response: any) => {
          if (response && response.status === 200) {
            NotifylUtil.showNotification(NotifylUtil.FROM_TOP, NotifylUtil.ALIGN_RIGHT, NotifylUtil.TYPE_NOTIFY_SUCCESS, 'Contraseña restablecida con éxito');
          }
        });
      }
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit {

  @ViewChild(MatSort) sort: MatSort;
  displayedColumns: string[] = ['id', 'username', 'nombre', 'estado', 'rol', 'fechaUltimaSession', 'actionDetalle'];
  dataSource = new MatTableDataSource();

  form: FormGroup;

  // Paginador
  totalRegistros = 0;
  pageSize = 30;
  currentPage = 1;
  totalPages = 1;

  constructor(private formBuilder: FormBuilder,
    private usuarioService: UsuarioService,
    public dialog: MatDialog) { }

  ngOnInit() {
    this.dataSource.sort = this.sort;
    this.initalizeForm();

    this.getUsuarios();
    this.count();
  }

  initalizeForm() {
    this.form = this.formBuilder.group({
      id: [],
      username: [],
      rol: [],
      estado: [],
    });

  }

  getUsuarios() {
    // Params
    const params = new Map<string, any>();
    params.set('page[size]', this.pageSize);
    params.set('page[number]', this.currentPage);
    // params.set('sort', sort);

    if (this.form.get('id').value) {
      params.set('_id', this.form.get('id').value);
    }

    if (this.form.get('username').value) {
      params.set('username', this.form.get('username').value);
    }

    if (this.form.get('rol').value) {
      params.set('rol', this.form.get('rol').value);
    }

    if (this.form.get('estado').value) {
      params.set('estadoId', this.form.get('estado').value);
    }

    this.usuarioService.getAll(params).subscribe((response: any) => {
      if (response && response.status === 200) {
        this.dataSource = new MatTableDataSource(response.body);
      }
    });
  }

  count() {
    // Params
    const params = new Map<string, any>();

    if (this.form.get('id').value) {
      params.set('_id', this.form.get('id').value);
    }

    if (this.form.get('username').value) {
      params.set('username', this.form.get('username').value);
    }

    if (this.form.get('rol').value) {
      params.set('rol', this.form.get('rol').value);
    }

    if (this.form.get('estado').value) {
      params.set('estadoId', this.form.get('estado').value);
    }

    this.usuarioService.count(params).subscribe((response: any) => {
      if (response) {
        this.totalRegistros = response;
        // Mapeamos la paginacion
        this.totalPages = Math.ceil(this.totalRegistros / this.pageSize);
      } else {
        this.totalRegistros = 0;
        this.totalPages = 1;
      }
    });
  }

  buscar() {
    this.getUsuarios();
    this.count();
  }

  getCurrentPage(currentPage: number) {
    this.currentPage = currentPage;
    this.getUsuarios();
  }

  verDetalle(elemnt: any) {
    elemnt.estadoDescripcion = this.getEstado(elemnt.estadoId);
    elemnt.rolDescripcion = this.getRol(elemnt.rol);

    this.dialog.open(UsuarioDialogComponent, {
      width: '90%',
      height: '90%',
      data: elemnt
    });
  }

  getEstado(id: number): string {
    switch (id) {
      case Config.estadoUsuarioInactivo: {
        return 'Inactivo';
      }
      case Config.estadoUsuarioPendiente: {
        return 'Pendiente';
      }
      case Config.estadoUsuarioEnValidacion: {
        return 'EnValidacion';
      }
      case Config.estadoUsuarioActivo: {
        return 'Activo';
      }
      case Config.estadoUsuarioBloqueado: {
        return 'Bloqueado';
      }
      case Config.estadoUsuarioEliminado: {
        return 'Eliminado';
      }
      default: {
        return 'NaN';
      }
    }
  }

  getRol(id: number): string {
    switch (id) {
      case Config.rolSuperAdministrador: {
        return 'SuperAdministrador';
      }
      case Config.rolAdministrador: {
        return 'Administrador';
      }
      case Config.rolProfesional: {
        return 'Profesional';
      }
      case Config.rolPaciente: {
        return 'Paciente';
      }
      default: {
        return 'NaN';
      }
    }
  }

  getColorEstado(id: number) {
    switch (id) {
      case Config.estadoUsuarioInactivo: {
        return { 'color': '#3f51b5' };
      }
      case Config.estadoUsuarioPendiente: {
        return null;
      }
      case Config.estadoUsuarioEnValidacion: {
        return null;
      }
      case Config.estadoUsuarioActivo: {
        return { 'color': '#01DF01' };
      }
      case Config.estadoUsuarioBloqueado: {
        return { 'color': 'red' };
      }
      case Config.estadoUsuarioEliminado: {
        return { 'color': 'red' };
      }
      default: {
        return null;
      }
    }
  }

  getColorRol(id: number) {
    switch (id) {
      case Config.rolSuperAdministrador: {
        return { 'color': '#DF0101' };
      }
      case Config.rolAdministrador: {
        return { 'color': '#DF0101' };
      }
      default: {
        return null;
      }
    }
  }

}



