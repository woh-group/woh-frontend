import { Component, OnInit } from '@angular/core';

import { AutenticacionService } from '../../_services/autenticacion.service';

import { Usuario } from '../../_entities/usuario';
import { Persona } from '../../_entities/persona';

import { Config } from '../../_configs/config';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [AutenticacionService]
})
export class HomeComponent implements OnInit {

  loggedIn = false;
  usuario: Usuario;
  persona: Persona;
  profesionIdPsicologo = Config.profesionIdPsicologo;

  showSectionProfesionales = true;
  showSectionCategorias = true;

  constructor(
    private autenticacionService: AutenticacionService
  ) {
    // Validamos si esta logueado
    this.autenticacionService.currentUser.subscribe(response => {
      if (response && response.accessToken) {
        this.loggedIn = true;
      } else {
        this.loggedIn = false;
      }
    }, error => {
      this.loggedIn = false;
    });
  }

  ngOnInit() {
  }

  getEventShowCategoriasIsOk(isOk: boolean) {
    // console.log('isOk', isOk);
    this.showSectionCategorias = isOk;
  }

  getEventShowProfesionalesIsOk(isOk: boolean) {
    // console.log('isOk', isOk);
    this.showSectionProfesionales = isOk;
  }

}
