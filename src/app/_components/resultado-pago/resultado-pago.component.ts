import { Component, OnInit } from '@angular/core';
import { EpaycoService } from 'src/app/_services/_service-util/epayco.service';
import { ActivatedRoute } from '@angular/router';
import { EpaycoTransaction } from 'src/app/_entities/epayco-transaction';

@Component({
  selector: 'app-resultado-pago',
  templateUrl: './resultado-pago.component.html',
  styleUrls: ['./resultado-pago.component.css']
})
export class ResultadoPagoComponent implements OnInit {

  refPayco: string = ''
	transactionResponse:any ;
  constructor(
  private epaycoService: EpaycoService,
  private activatedRoute: ActivatedRoute

  ) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      this.refPayco= params['ref_payco'] || params['x_ref_payco'];
  });
   this.epaycoService.getTransactionResponse(this.refPayco)
   .subscribe((data: EpaycoTransaction) =>{
       this.transactionResponse = data.data
   });
  }

}
