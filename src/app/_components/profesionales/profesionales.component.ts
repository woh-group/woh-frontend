import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from '../../../environments/environment';

import { ArrayUtil } from 'src/app/_helpers/_util/array-util';

import { ProfesionalService } from 'src/app/_services/profesionales/profesional.service';
import { UsuarioService } from 'src/app/_services/usuario.service';

import { Profesional } from 'src/app/_entities/profesional';
import { LoaderService } from 'src/app/_services/_service-util/loader.service';
import { finalize } from 'rxjs/operators';


@Component({
  selector: 'app-profesionales',
  templateUrl: './profesionales.component.html',
  styleUrls: ['./profesionales.component.css']
})
export class ProfesionalesComponent implements OnInit {

  private isHome = false;
  @Output() eventIsOk = new EventEmitter();

  totalProfesionales = 0;
  pageSize = 9;
  currentPage = 1;
  totalPages = 1;

  profesionales: Profesional[] = [];
  letterParam: string;
  letters: string[] = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
  routeFiles = environment.endpointBucketFiles;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private profesionalesService: ProfesionalService,
    private usuarioService: UsuarioService,
    private loaderService: LoaderService) { }

  ngOnInit() {
    if (this.router.url.includes('/home')) {
      this.isHome = true;
      this.getProfesionales();
    } else {
      window.scrollTo(0, 0);
    }

    this.letterParam = this.route.snapshot.params['letter'];
    this.count();
  }

  getIsHome(): boolean {
    return this.isHome;
  }

  getProfesionales() {
    // Params
    const params = new Map<string, any>();
    params.set('showPersona', true);
    params.set('estado', true);
    if (this.letterParam) {
      params.set('firstLetter', this.letterParam);
    }

    if (this.isHome) {
      params.set('page[size]', 3);
      params.set('page[number]', 1);
    } else {
      params.set('page[size]', this.pageSize);
      params.set('page[number]', this.currentPage);
    }

    setTimeout(() => {
      if (this.isHome && this.profesionalesService.profesionales) {
        this.profesionales = ArrayUtil.shuffle(this.profesionalesService.profesionales).slice(0, 3);
        this.eventIsOk.emit(true);
      } else {
        this.loaderService.display(true);
        this.profesionalesService.getAll(params).pipe(finalize(() => {
          this.loaderService.display(false);
        })).subscribe((response: any) => {
          if (response && response.status === 200) {
            this.profesionales = ArrayUtil.shuffle(response.body);
            this.eventIsOk.emit(true);
          } else {
            this.eventIsOk.emit(false);
          }
        });
      }
    }, 2000);
  }

  count() {
    // Params
    const params = new Map<string, any>();
    if (this.letterParam) {
      params.set('persona.nombres[startswith]', this.letterParam);
    }
    params.set('rol', 3);
    params.set('estadoId', 4);

    this.usuarioService.count(params).subscribe((response: any) => {
      // console.log('response', response);
      if (response) {
        this.totalProfesionales = response;
        // Mapeamos la paginacion
        this.totalPages = Math.ceil(this.totalProfesionales / this.pageSize);
      } else {
        this.totalProfesionales = 0;
        this.totalPages = 1;
      }
    });
  }

  getProfesionalesByLetter(letter: string) {
    this.letterParam = letter;
    this.count();
    this.getProfesionales();
  }

  getCurrentPage(currentPage: number) {
    this.currentPage = currentPage;
    this.getProfesionales();
  }

}
