export const environment = {
  production: false,
  APP_ID: 'WOH-WEB',

  endpointComunes: 'http://localhost:3000',
  endpointUsuarios: 'http://localhost:3001',
  endpointProfesionales: 'http://localhost:3002',
  endpointPreguntas: 'http://localhost:3003',
  endpointTrace: 'http://localhost:3004',


  endpointBucketFiles: 'https://s3.amazonaws.com/woh-files-dev/perfil/',

  // tslint:disable-next-line:max-line-length
  tokenApiUnknownUser: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2wiOjEsInBlcm1pc29zIjp7IkFERCI6dHJ1ZSwiREVMRVRFIjp0cnVlLCJSRUFEIjp0cnVlLCJVUERBVEUiOnRydWV9LCJhdWQiOiJBTEwiLCJpYXQiOjE1MzU5NTA2ODYsImlzcyI6IlRFU1QiLCJzdWIiOiJhZG1pbiJ9.Ewt7rbpqeUFwH1pDItgFeFNyOEFDJAc0iWEt0mFuhXM',


  // *** Producction ***
  // endpointComunes : 'https://9mxli1mzcc.execute-api.us-east-2.amazonaws.com/prod',
  // endpointUsuarios: 'https://hjnyyntmk6.execute-api.us-east-2.amazonaws.com/prod',
  // endpointProfesionales: 'https://14podhfzk0.execute-api.us-east-2.amazonaws.com/prod',
  // endpointPreguntas: 'https://g12radp626.execute-api.us-east-2.amazonaws.com/prod',
  // endpointTrace: 'https://9s06jn7pc7.execute-api.us-east-2.amazonaws.com/prod',

  // endpointBucketFiles: 'https://s3.amazonaws.com/woh-files/perfil/',

  // tokenApiUnknownUser: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2wiOjUsInBlcm1pc29zIjp7IkFERCI6dHJ1ZSwiREVMRVRFIjp0cnVlLCJSRUFEIjp0cnVlLCJVUERBVEUiOnRydWV9LCJhdWQiOiJBTEwiLCJpYXQiOjE1NjE5NDA4NjksInN1YiI6IkNPTVVORVMifQ.tC5ikSGFRUv9HnpuuqFfcUO5Wl_tPlhsGSoTIKYQg1w',
};
