export const environment = {
  production: true,
  APP_ID: 'WOH-WEB',

  endpointComunes : 'https://9mxli1mzcc.execute-api.us-east-2.amazonaws.com/prod',
  endpointUsuarios: 'https://hjnyyntmk6.execute-api.us-east-2.amazonaws.com/prod',
  endpointProfesionales: 'https://14podhfzk0.execute-api.us-east-2.amazonaws.com/prod',
  endpointPreguntas: 'https://g12radp626.execute-api.us-east-2.amazonaws.com/prod',
  endpointTrace: 'https://9s06jn7pc7.execute-api.us-east-2.amazonaws.com/prod',


  endpointBucketFiles: 'https://s3.amazonaws.com/woh-files/perfil/',

  // tslint:disable-next-line:max-line-length
  tokenApiUnknownUser: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2wiOjUsInBlcm1pc29zIjp7IkFERCI6dHJ1ZSwiREVMRVRFIjp0cnVlLCJSRUFEIjp0cnVlLCJVUERBVEUiOnRydWV9LCJhdWQiOiJBTEwiLCJpYXQiOjE1NjE5NDA4NjksInN1YiI6IkNPTVVORVMifQ.tC5ikSGFRUv9HnpuuqFfcUO5Wl_tPlhsGSoTIKYQg1w',
};
