/// <reference types="Cypress" />
 
it('should visit home', () => {

    cy.visit('http://localhost:4200');

    cy.get('li.nav-item > a.login').click();
    
    // Login
    cy.get('input.username').type('lesolano');
    cy.get('input.password').type('123');
    cy.get('.card-footer > a.btn-ingresar').click();

    // Cerrar sesion
    cy.get('#navbarDropdownMenuLink').click();
    cy.get('a.logout').click();
});