/// <reference types="Cypress" />
 
it('should visit home', () => {

    cy.visit('http://localhost:4200');
    cy.get('li.nav-item > a.registro-paciente').click();

    const formDatosPersonales = cy.get('form.form-datos-persona')
    formDatosPersonales.get('#nombres').type('Pepito Andres');    
    formDatosPersonales.get('#apellidos').type('Perez Jimenez');
    formDatosPersonales.get('#email').type('pepito27@gmail.com');
    formDatosPersonales.get('#sexo').click().get('mat-option').contains('M').click();
    formDatosPersonales.get('#fechaNacimiento').type('3/7/1990');    
    formDatosPersonales.get('#estadoCivil').click().get('mat-option').contains('Soltero/a').click();
    
    
    const formRegistroUsuario = cy.get('form.form-datos-persona')
    formRegistroUsuario.get('button.btn-registrar').click();

    
    

   
});